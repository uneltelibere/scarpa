package eu.uneltelibere.scarpa.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.dao.StaticPageDao;
import eu.uneltelibere.scarpa.model.StaticPage;

public class DatabaseStaticPageServiceTest {

	private static final int TEST_PAGE_ID = 11;
	private static final int TEST_PAGE_VERSION_ID = 22;
	private static final int TEST_PAGE_VERSION_NUMBER = 7;

	private static final int LATEST_PAGE_VERSION = 0;

	@Captor
	private ArgumentCaptor<StaticPage.Version> captor;

	@InjectMocks
	private DatabaseStaticPageService staticPageService;

	@Mock
	private StaticPageDao staticPageDao;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetLatestVersionOfPage() {
		StaticPage.Version versionInDb = new StaticPage.Version.Builder().id(TEST_PAGE_VERSION_ID).build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.of(versionInDb));

		Optional<StaticPage.Version> maybeVersion = staticPageService.getLatestVersionOfPage(TEST_PAGE_ID);

		assertTrue(maybeVersion.isPresent());
		assertEquals(versionInDb, maybeVersion.get());
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetLatestVersionOfPageNotFound() {
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.empty());

		Optional<StaticPage.Version> maybeVersion = staticPageService.getLatestVersionOfPage(TEST_PAGE_ID);

		assertFalse(maybeVersion.isPresent());
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetVersionOfPage() {
		StaticPage.Version versionInDb = new StaticPage.Version.Builder().id(TEST_PAGE_VERSION_ID).build();
		when(staticPageDao.getVersionOfPage(TEST_PAGE_ID, TEST_PAGE_VERSION_NUMBER))
				.thenReturn(Optional.of(versionInDb));

		Optional<StaticPage.Version> maybeVersion = staticPageService.getVersionOfPage(TEST_PAGE_ID,
				TEST_PAGE_VERSION_NUMBER);

		assertTrue(maybeVersion.isPresent());
		assertEquals(versionInDb, maybeVersion.get());
		verify(staticPageDao).getVersionOfPage(TEST_PAGE_ID, TEST_PAGE_VERSION_NUMBER);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetVersionOfPageNotFound() {
		when(staticPageDao.getVersionOfPage(TEST_PAGE_ID, TEST_PAGE_VERSION_NUMBER)).thenReturn(Optional.empty());

		Optional<StaticPage.Version> maybeVersion = staticPageService.getVersionOfPage(TEST_PAGE_ID,
				TEST_PAGE_VERSION_NUMBER);

		assertFalse(maybeVersion.isPresent());
		verify(staticPageDao).getVersionOfPage(TEST_PAGE_ID, TEST_PAGE_VERSION_NUMBER);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetVersionOfPageLatest() {
		StaticPage.Version versionInDb = new StaticPage.Version.Builder().id(TEST_PAGE_VERSION_ID).build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.of(versionInDb));

		Optional<StaticPage.Version> maybeVersion = staticPageService.getVersionOfPage(TEST_PAGE_ID,
				LATEST_PAGE_VERSION);

		assertTrue(maybeVersion.isPresent());
		assertEquals(versionInDb, maybeVersion.get());
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetVersionOfPageLatestNotFound() {
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.empty());

		Optional<StaticPage.Version> maybeVersion = staticPageService.getVersionOfPage(TEST_PAGE_ID,
				LATEST_PAGE_VERSION);

		assertFalse(maybeVersion.isPresent());
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testGetVersionsOfPage() {
		StaticPage.Version versionInDb = new StaticPage.Version.Builder().id(TEST_PAGE_VERSION_ID).build();
		List<StaticPage.Version> listInDb = Collections.singletonList(versionInDb);
		when(staticPageDao.getVersionsOfPage(TEST_PAGE_ID)).thenReturn(listInDb);

		List<StaticPage.Version> versions = staticPageService.getVersionsOfPage(TEST_PAGE_ID);

		assertThat(versions, is(listInDb));
		verify(staticPageDao).getVersionsOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testSaveFirstVersion() {
		StaticPage staticPage = new StaticPage.Builder().id(TEST_PAGE_ID).build();
		StaticPage.Version firstVersion = new StaticPage.Version.Builder().id(TEST_PAGE_VERSION_ID).page(staticPage)
				.build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.empty());

		staticPageService.save(firstVersion);

		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verify(staticPageDao).create(captor.capture());
		assertEquals(1, captor.getValue().getVersionNumber());
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testSaveNotFirstVersion() {
		StaticPage staticPage = new StaticPage.Builder().id(TEST_PAGE_ID).build();
		StaticPage.Version existingVersion = new StaticPage.Version.Builder().page(staticPage)
				.versionNumber(TEST_PAGE_VERSION_NUMBER).build();
		StaticPage.Version newVersion = new StaticPage.Version.Builder().page(staticPage).build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.of(existingVersion));

		staticPageService.save(newVersion);

		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verify(staticPageDao).create(captor.capture());
		assertEquals(TEST_PAGE_VERSION_NUMBER + 1, captor.getValue().getVersionNumber());
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testCurrentVersionNumber() {
		StaticPage staticPage = new StaticPage.Builder().id(TEST_PAGE_ID).build();
		StaticPage.Version existingVersion = new StaticPage.Version.Builder().page(staticPage)
				.versionNumber(TEST_PAGE_VERSION_NUMBER).build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.of(existingVersion));

		int versionNumber = staticPageService.currentVersionNumber(staticPage);

		assertEquals(TEST_PAGE_VERSION_NUMBER, versionNumber);
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}

	@Test
	public void testCurrentVersionNumberNotFound() {
		StaticPage staticPage = new StaticPage.Builder().id(TEST_PAGE_ID).build();
		when(staticPageDao.getLatestVersionOfPage(TEST_PAGE_ID)).thenReturn(Optional.empty());

		int versionNumber = staticPageService.currentVersionNumber(staticPage);

		assertEquals(0, versionNumber);
		verify(staticPageDao).getLatestVersionOfPage(TEST_PAGE_ID);
		verifyNoMoreInteractions(staticPageDao);
	}
}
