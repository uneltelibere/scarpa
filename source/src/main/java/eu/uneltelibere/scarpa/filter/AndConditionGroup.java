package eu.uneltelibere.scarpa.filter;

import java.util.Collection;

public class AndConditionGroup<T> extends ConditionGroup<T> {

	@SafeVarargs
	public AndConditionGroup(Condition<T>... conditions) {
		super(conditions);
	}

	public AndConditionGroup(Collection<Condition<T>> conditions) {
		super(conditions);
	}
}
