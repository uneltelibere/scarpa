package eu.uneltelibere.scarpa.service.lister;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.dao.SortCriteria;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.PageNavigator;

public interface FilteredLister<T> {

	List<T> getList(ConditionGroup<T> conditions, int pageNumber);

	List<T> getList(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria, int pageNumber);

	Optional<PageNavigator> getNavigator(ConditionGroup<T> conditions, String basePageUrl, int pageNumber);
}
