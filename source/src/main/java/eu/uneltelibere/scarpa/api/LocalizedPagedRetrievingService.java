package eu.uneltelibere.scarpa.api;

import java.util.List;
import java.util.Locale;

public interface LocalizedPagedRetrievingService<T> {

	List<T> retrieve(Page page, Locale locale);

	int count();
}
