(function() {
	var MAX_RETRY_COUNT = 60;
	var RETRY_INTERVAL = 1000;
	var PROGRESS_DONE = 100;

	var retryCount = 0;
	var retryTimer;

	var resetStatus = $('#resetProgressText');
	var resetButton = $('#resetPasswordButton');
	var resetForm = $("#resetPasswordForm");

	resetButton.click(function() {
		triggerResetPassword();
	});

	var triggerResetPassword = function() {
		resetButton.attr("disabled", "disabled");
		resetForm.ajaxSubmit({
			dataType : "json",
			success : function(data) {
				resetStatus.html('Password reset was requested.');

				retryCount = 0;
				retryTimer = setInterval(checkStatus, RETRY_INTERVAL);
			},
			error : function(jqXHR, errorType, exception) {
				resetStatus.html('Failed to request password reset.');
				endResetPassword();
			}
		});
	};

	var endResetPassword = function() {
		clearInterval(retryTimer);
		resetButton.removeAttr("disabled");
	}

	var checkStatus = function() {
		retryCount++;
		if (retryCount > MAX_RETRY_COUNT) {
			resetStatus.html('Password reset failed.');
			endResetPassword();
		} else {
			resetStatus.html('Checking status (' + retryCount + ')...');
			$.ajax({
				url : contextPath + 'users/' + userId + '/resetPasswordStatus',
				success : function(data) {
					resetStatus.html('Password request was requested.');
				},
				complete : function(jqXHR) {
					var json = jqXHR.responseJSON;
					resetStatus.html(json.message);
					if (!json.success || json.progress === PROGRESS_DONE) {
						endResetPassword();
					}
				},
				error : function(jqXHR, errorType, exception) {
					resetStatus
							.html('Failed to check password request status.');
					endResetPassword();
				}
			});
		}
	};
})();
