(function() {

	/**
	 * When the `email` input changes, auto-fill the `username` field.
	 */
	$('#email').on('input', function(e) {
		forceLowerCase($(this))

		var username = $(this).val()
		var atPos = $(this).val().indexOf('@')
		if (atPos !== -1) {
			username = $(this).val().substring(0, atPos)
		}
		$('#username').val(username)
	});

	$('#username').on('input', function(e) {
		forceLowerCase($(this))
	});

	var forceLowerCase = function(input) {
		input.val(input.val().toLowerCase())
	}

})();
