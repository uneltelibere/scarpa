package eu.uneltelibere.scarpa.dao;

public class SqlSort {

	private final StringBuilder query = new StringBuilder();
	private final String defaultSort;

	private SqlSort(String defaultSort) {
		this.defaultSort = defaultSort;
	}

	public void addSort(String sql, SortCriteria.Order order) {
		query.append(sql);
		if (SortCriteria.Order.DESC.equals(order)) {
			query.append(" DESC");
		}
		query.append(", ");
	}

	public String query() {
		return query.toString() + defaultSort;
	}

	public static SqlSort sortByDefault(String defaultSort) {
		SqlSort sort = new SqlSort(defaultSort);
		return sort;
	}
}
