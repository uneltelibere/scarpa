package eu.uneltelibere.scarpa.service.choose.api;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

public interface DependencyChooser<T> {

	String choose(DependencyChooserParameters<T> params, Model model, HttpSession session);
}
