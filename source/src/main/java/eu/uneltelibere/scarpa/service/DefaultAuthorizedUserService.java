package eu.uneltelibere.scarpa.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

@Service
public class DefaultAuthorizedUserService implements AuthorizedUserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private RoleGroupDao roleGroupDao;

	@Override
	public Optional<User> getUserDetails(String username) {
		return userDao.getUserDetails(username).map(user -> userWithRoles(user));
	}

	@Override
	public User userWithRoles(User user) {
		Set<Role> roles = new LinkedHashSet<>();
		roles.addAll(roleDao.getRolesOfUser(user));

		List<RoleGroup> groups = roleGroupDao.getRoleGroupsOfUser(user);
		for (RoleGroup group : groups) {
			roles.addAll(roleDao.getRolesInGroup(group));
		}

		user.setRoles(new ArrayList<>(roles));
		return user;
	}
}
