package eu.uneltelibere.scarpa;

import eu.uneltelibere.scarpa.api.AbstractRole;

public class InstallAdminRole extends AbstractRole {

	public InstallAdminRole() {
		super("INSTALL_ADMIN");
	}
}
