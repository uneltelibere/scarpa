package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.User;

public interface UserDao extends PagedRetriever<User> {

	Optional<User> getUser(long id);

	Optional<User> getUserDetails(String username);

	Optional<User> getUser(String username);

	Optional<User> getUserByEmail(String email);

	long create(User user);

	void update(User user);

	void updateWithoutPassword(User user);

	void removeRolesFromUser(User user, List<Long> roleIDsToRemove);

	void addRolesToUser(User user, List<Long> roleIDsToAdd);

	void removeGroupsFromUser(User user, List<Long> groupIDsToRemove);

	void addGroupsToUser(User user, List<Long> groupIDsToAdd);

	void setPassword(User user);

	void delete(long id);
}
