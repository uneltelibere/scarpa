package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.BulkRetriever;
import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcUserDao extends JdbcMasterDao implements UserDao, BulkRetriever<User> {

	// @formatter:off
	private static final String COUNT_USERS = "SELECT COUNT(*) FROM users";

	private static final String ALL_USERS_PAGED = "SELECT "
			+ "u.id, u.username, u.credentials_non_expired, u.email "
			+ "FROM users u "
			+ "ORDER BY username "
			+ "OFFSET :offset LIMIT :limit";

	private static final String GET_USERS_BY_IDS = "SELECT "
			+ "u.id, u.username, u.credentials_non_expired, u.email "
			+ "FROM users u "
			+ "WHERE u.id IN (:ids) ";

	// TODO Stop getting roles here. Use role-groups, read them from the service.
	private static final String USER_WITH_ROLES = "SELECT "
			+ "u.id AS user_id, u.username, u.email, u.enabled, "
			+ "a.id AS role_id, a.label "
			+ "FROM users u "
			+ "LEFT JOIN user_authority ua ON ua.user_id = u.id "
			+ "LEFT JOIN authorities a ON a.id = ua.authority_id ";

	private static final String USER_WITH_ROLES_BY_ID = USER_WITH_ROLES
			+ "WHERE u.id = :id";

	private static final String USER_DETAILS = "SELECT "
			+ "u.id, u.username, u.password, u.enabled, u.credentials_non_expired "
			+ "FROM users u "
			+ "WHERE u.username = :username";

	private static final String USER_WITH_ROLES_BY_NAME = USER_WITH_ROLES
			+ "WHERE u.username = :username";

	private static final String USER_WITH_ROLES_BY_EMAIL = USER_WITH_ROLES
			+ "WHERE u.email = :email";

	private static final String NEW_USER = "INSERT INTO users ("
			+ "username, email, password, enabled, credentials_non_expired"
			+ ") VALUES ("
			+ ":username, :email, :password, true, :credentialsNonExpired)";

	private static final String UPDATE_USER = "UPDATE users SET "
			+ "email = :email, "
			+ "password = :password, "
			+ "enabled = :enabled "
			+ "WHERE id = :id";

	private static final String UPDATE_USER_NO_PASS = "UPDATE users SET "
			+ "email = :email, "
			+ "enabled = :enabled "
			+ "WHERE id = :id";

	private static final String UPDATE_PASS = "UPDATE users SET "
			+ "password = :password, "
			+ "credentials_non_expired = true "
			+ "WHERE id = :id";

	private static final String REMOVE_ROLES = "DELETE FROM user_authority ua "
			+ "WHERE ua.user_id = :user_id "
			+ "AND ua.authority_id IN (:role_ids)";

	private static final String ADD_ROLES = "INSERT INTO user_authority ("
			+ "user_id, authority_id"
			+ ") VALUES ("
			+ ":user_id, :role_id)";

	private static final String REMOVE_GROUPS = "DELETE FROM user_role_group "
			+ "WHERE user_id = :userId "
			+ "AND role_group_id IN (:roleIds)";

	private static final String ADD_GROUPS = "INSERT INTO user_role_group ("
			+ "user_id, role_group_id"
			+ ") VALUES ("
			+ ":userId, :roleId)";

	private static final String DELETE_USER = "DELETE FROM users u "
			+ "WHERE u.id = :id";

	private static final String DELETE_USER_AUTHORITY = "DELETE FROM user_authority ua "
			+ "WHERE ua.user_id = :id";
	// @formatter:on

	private RowMapper<User> userRowMapper = new RowMapper<User>() {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setUsername(rs.getString("username"));
			user.setCredentialsNonExpired(rs.getBoolean("credentials_non_expired"));
			user.setEmail(rs.getString("email"));
			return user;
		}
	};

	@Override
	public int count() {
		MapSqlParameterSource params = new MapSqlParameterSource();
		return jdbcTemplate.queryForObject(COUNT_USERS, params, Integer.class);
	}

	@Override
	public List<User> retrieve(int offset, int limit) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("offset", offset);
		params.addValue("limit", limit);
		return jdbcTemplate.query(ALL_USERS_PAGED, params, userRowMapper);
	}

	@Override
	public List<User> retrieveByIds(List<Long> ids) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("ids", ids);
		return jdbcTemplate.query(GET_USERS_BY_IDS, params, userRowMapper);
	}

	@Override
	public Optional<User> getUser(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		return getUser(USER_WITH_ROLES_BY_ID, params);
	}

	@Override
	public Optional<User> getUserDetails(String username) {
		SqlParameterSource params = new MapSqlParameterSource("username", username);
		return jdbcTemplate.query(USER_DETAILS, params, new ResultSetExtractor<Optional<User>>() {

			@Override
			public Optional<User> extractData(ResultSet rs) throws SQLException, DataAccessException {
				User user = new User();
				Optional<User> maybeUser = Optional.empty();
				if (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setUsername(rs.getString("username"));
					user.setPassword(rs.getString("password"));
					user.setEnabled(rs.getBoolean("enabled"));
					user.setCredentialsNonExpired(rs.getBoolean("credentials_non_expired"));
					maybeUser = Optional.of(user);
				}
				return maybeUser;
			}
		});
	}

	@Override
	public Optional<User> getUser(String username) {
		SqlParameterSource params = new MapSqlParameterSource("username", username);
		return getUser(USER_WITH_ROLES_BY_NAME, params);
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		SqlParameterSource params = new MapSqlParameterSource("email", email);
		return getUser(USER_WITH_ROLES_BY_EMAIL, params);
	}

	private Optional<User> getUser(String query, SqlParameterSource params) {
		return jdbcTemplate.query(query, params, new ResultSetExtractor<Optional<User>>() {

			@Override
			public Optional<User> extractData(ResultSet rs) throws SQLException, DataAccessException {
				User user = null;
				while (rs.next()) {
					if (rs.isFirst()) {
						user = new User();
						user.setId(rs.getInt("user_id"));
						user.setUsername(rs.getString("username"));
						user.setEmail(rs.getString("email"));
						user.setEnabled(rs.getBoolean("enabled"));
					}
					String label = rs.getString("label");
					if (label != null) {
						Role role = new Role();
						role.setId(rs.getInt("role_id"));
						role.setName(label);
						user.getRoles().add(role);
					}
				}
				return Optional.ofNullable(user);
			}
		});
	}

	@Override
	public long create(User user) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", user.getUsername());
		params.addValue("email", user.getEmail());
		params.addValue("password", user.getPassword());
		params.addValue("credentialsNonExpired", user.isCredentialsNonExpired());
		jdbcTemplate.update(NEW_USER, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public void update(User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", user.getId());
		params.addValue("email", user.getEmail());
		params.addValue("password", user.getPassword());
		params.addValue("enabled", user.isEnabled());
		jdbcTemplate.update(UPDATE_USER, params);
	}

	@Override
	public void updateWithoutPassword(User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", user.getId());
		params.addValue("email", user.getEmail());
		params.addValue("enabled", user.isEnabled());
		jdbcTemplate.update(UPDATE_USER_NO_PASS, params);
	}

	@Override
	public void removeRolesFromUser(User user, List<Long> roleIDsToRemove) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("user_id", user.getId());
		params.addValue("role_ids", roleIDsToRemove);
		jdbcTemplate.update(REMOVE_ROLES, params);
	}

	@Override
	public void addRolesToUser(User user, List<Long> roleIDsToAdd) {
		MapSqlParameterSource[] batchArgs = new MapSqlParameterSource[roleIDsToAdd.size()];
		for (int i = 0; i < roleIDsToAdd.size(); i++) {
			batchArgs[i] = new MapSqlParameterSource();
			batchArgs[i].addValue("user_id", user.getId());
			batchArgs[i].addValue("role_id", roleIDsToAdd.get(i));
		}
		jdbcTemplate.batchUpdate(ADD_ROLES, batchArgs);
	}

	@Override
	public void removeGroupsFromUser(User user, List<Long> groupIDsToRemove) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", user.getId());
		params.addValue("roleIds", groupIDsToRemove);
		jdbcTemplate.update(REMOVE_GROUPS, params);
	}

	@Override
	public void addGroupsToUser(User user, List<Long> groupIDsToAdd) {
		MapSqlParameterSource[] batchArgs = new MapSqlParameterSource[groupIDsToAdd.size()];
		for (int i = 0; i < groupIDsToAdd.size(); i++) {
			batchArgs[i] = new MapSqlParameterSource();
			batchArgs[i].addValue("userId", user.getId());
			batchArgs[i].addValue("roleId", groupIDsToAdd.get(i));
		}
		jdbcTemplate.batchUpdate(ADD_GROUPS, batchArgs);
	}

	@Override
	public void setPassword(User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", user.getId());
		params.addValue("password", user.getPassword());
		jdbcTemplate.update(UPDATE_PASS, params);
	}

	@Override
	public void delete(long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		jdbcTemplate.update(DELETE_USER_AUTHORITY, params);
		jdbcTemplate.update(DELETE_USER, params);
	}
}
