package eu.uneltelibere.scarpa.spring;

public interface PasswordRemover {

	String removePassword(String uri);
}
