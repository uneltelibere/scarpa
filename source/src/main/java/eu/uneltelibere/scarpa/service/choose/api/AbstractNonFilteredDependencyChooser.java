package eu.uneltelibere.scarpa.service.choose.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.model.PageNavigator;
import eu.uneltelibere.scarpa.service.lister.PagedLister;

public abstract class AbstractNonFilteredDependencyChooser<T> extends AbstractDependencyChooser<T> {

	@Autowired
	private PagedLister<T> lister;

	@Override
	protected List<T> getElements(DependencyChooserParameters<T> params) {
		int pageNumber = params.getPageNumber();
		return lister.getList(pageNumber);
	}

	@Override
	protected Optional<PageNavigator> getNavigator(DependencyChooserParameters<T> params) {
		int pageNumber = params.getPageNumber();
		String basePageUrl = params.getChooserUrl();
		return lister.getNavigator(basePageUrl, pageNumber);
	}
}
