package eu.uneltelibere.scarpa.service;

import java.net.URISyntaxException;

public interface UploadConfigurationService {

	String installUploadDirectory() throws URISyntaxException;

	String attachmentUploadDirectory() throws URISyntaxException;
}
