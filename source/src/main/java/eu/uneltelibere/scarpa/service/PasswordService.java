package eu.uneltelibere.scarpa.service;

public interface PasswordService {

	String generatePassword();

	String encodePassword(String clearText);

	boolean matches(CharSequence rawPassword, String encodedPassword);

	boolean isValidPassword(CharSequence clearText);

	int minPasswordLength();

	void validatePassword(CharSequence password, CharSequence passwordAgain);
}
