package eu.uneltelibere.scarpa.web.controller;

@SuppressWarnings("serial")
public class FormException extends RuntimeException {

	public FormException(String message) {
		super(message);
	}
}
