package eu.uneltelibere.scarpa.web.controller;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.NotFoundException;
import eu.uneltelibere.scarpa.service.PasswordService;
import eu.uneltelibere.scarpa.service.TokenService;
import eu.uneltelibere.scarpa.service.UserService;

@Controller
@RequestMapping("/password")
public class PasswordResetController {

	private static final Logger LOG = LoggerFactory.getLogger(PasswordResetController.class);

	private static final String ATTR_STATUS = "status";

	private static final String STATUS_TOKEN_OK = "tokenOk";
	private static final String STATUS_TOKEN_EXPIRED = "tokenExpired";
	private static final String STATUS_TOKEN_NOT_FOUND = "tokenNotFound";

	private static final String STATUS_PASSWORD_CHANGED = "passwordChanged";

	@Autowired
	private TokenService tokenService;

	@Autowired
	private PasswordService passwordService;

	@Autowired
	private UserService userService;

	@GetMapping
	public String showPasswordStatus(Model model) {
		LOG.info("Showing the password status page");
		return "passwordStatus";
	}

	@GetMapping(path = "/expired")
	public String showPasswordIsExpired(Model model) {
		LOG.info("Showing the expired password page");
		model.addAttribute("passwordMinLen", passwordService.minPasswordLength());
		return "passwordExpired";
	}

	@PostMapping(path = "/expired")
	public String changePasswordWithUsername(@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "oldPassword", defaultValue = "") String oldPassword,
			@RequestParam(value = "password", defaultValue = "") String password,
			@RequestParam(value = "passwordAgain", defaultValue = "") String passwordAgain,
			RedirectAttributes redirectAttributes) {
		LOG.info("User {} wanted to change their expired password", username);
		try {
			User user = userService.getUserDetails(username).get();
			boolean oldPasswordMatches = passwordService.matches(oldPassword, user.getPassword());
			if (!oldPasswordMatches) {
				LOG.info("Tried to change expired password for user {}, but the old password was wrong.", username);
				return "redirect:/password/expired?retry=nomatch";
			}
			if (!passwordService.isValidPassword(password)) {
				LOG.info("Tried to change expired password for user {}, but the new password was not acceptable.",
						username);
				return "redirect:/password/expired?retry=invalid";
			}
			if (!password.equals(passwordAgain)) {
				LOG.info("Tried to change expired password for user {}, but the new passwords didn't match.", username);
				return "redirect:/password/expired?retry=nomatch";
			}
			String encodedPassword = passwordService.encodePassword(password);
			userService.setPassword(user, encodedPassword);
			redirectAttributes.addFlashAttribute(ATTR_STATUS, STATUS_PASSWORD_CHANGED);
			return "redirect:/password";
		} catch (NoSuchElementException e) {
			LOG.info("Tried to change expired password for non-existent user {}", username, e);
			return "redirect:/password/expired?retry=nomatch";
		}
	}

	@GetMapping(path = "/{token}")
	public String askToChangePassword(@PathVariable(value = "token") String token,
			@RequestParam(value = "retry", defaultValue = "") String retry, Model model) {
		String status = statusForToken(token);
		LOG.info("Showing page for password reset token {}, state {}", token, status);
		if (status.equals(STATUS_TOKEN_OK)) {
			model.addAttribute("passwordMinLen", passwordService.minPasswordLength());
			return "password";
		} else {
			model.addAttribute(ATTR_STATUS, status);
			return "passwordStatus";
		}
	}

	@PostMapping
	public String changePasswordWithToken(@RequestParam(value = "password", defaultValue = "") String password,
			@RequestParam(value = "passwordAgain", defaultValue = "") String passwordAgain,
			@RequestParam("token") String token, RedirectAttributes redirectAttributes) {
		Token passwordToken = passwordToken(token);
		String status = statusForToken(passwordToken);
		LOG.info("Attempting to change password using token {}, state {}", token, status);
		if (status.equals(STATUS_TOKEN_OK)) {
			if (!passwordService.isValidPassword(password)) {
				return "redirect:/password/" + token + "?retry=nomatch";
			}
			if (!password.equals(passwordAgain)) {
				return "redirect:/password/" + token + "?retry=nomatch";
			}
			String encodedPassword = passwordService.encodePassword(password);
			userService.setPassword(passwordToken.getUser(), encodedPassword);
			tokenService.deleteToken(passwordToken);
			redirectAttributes.addFlashAttribute(ATTR_STATUS, STATUS_PASSWORD_CHANGED);
		} else {
			redirectAttributes.addFlashAttribute(ATTR_STATUS, status);
		}
		return "redirect:/password";
	}

	private Token passwordToken(String token) {
		Token passwordToken = null;
		try {
			passwordToken = tokenService.getToken(token);
		} catch (NotFoundException e) {
			LOG.info("Token {} was not found.", token);
		}

		return passwordToken;
	}

	private String statusForToken(Token passwordToken) {
		if (passwordToken != null) {
			if (tokenService.isValid(passwordToken)) {
				return STATUS_TOKEN_OK;
			} else {
				return STATUS_TOKEN_EXPIRED;
			}
		} else {
			return STATUS_TOKEN_NOT_FOUND;
		}
	}

	private String statusForToken(String token) {
		Token passwordToken = passwordToken(token);
		return statusForToken(passwordToken);
	}
}
