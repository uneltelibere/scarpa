package eu.uneltelibere.scarpa.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;

public class DefaultRoleGroupServiceTest {

	@InjectMocks
	private DefaultRoleGroupService roleGroupService;

	@Mock
	private RoleGroupDao roleGroupDao;

	@Mock
	private RoleService roleService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSetRolesForUserUnchanged() {
		RoleGroup inputGroup = new RoleGroup();
		inputGroup.setId(42);
		when(roleGroupDao.retrieve(inputGroup.getId())).thenReturn(Optional.of(groupWithRoles(1, 2)));
		when(roleService.retrieve(inputGroup, Locale.ENGLISH)).thenReturn(roles(1, 2));

		roleGroupService.setRolesForGroup(inputGroup, Arrays.asList(1L, 2L));

		verify(roleGroupDao, never()).removeRolesFromGroup(any(), any());
		verify(roleGroupDao, never()).addRolesToGroup(any(), any());
	}

	@Test
	public void testSetRolesForUserRemoveOneKeepOneAddOne() {
		RoleGroup inputGroup = new RoleGroup();
		inputGroup.setId(42);
		RoleGroup dbGroup = groupWithRoles(1, 2);
		when(roleGroupDao.retrieve(inputGroup.getId())).thenReturn(Optional.of(dbGroup));
		when(roleService.retrieve(inputGroup, Locale.ENGLISH)).thenReturn(roles(1, 2));

		roleGroupService.setRolesForGroup(inputGroup, Arrays.asList(2L, 3L));

		verify(roleGroupDao).removeRolesFromGroup(dbGroup, Arrays.asList(1L));
		verify(roleGroupDao).addRolesToGroup(dbGroup, Arrays.asList(3L));
	}

	private RoleGroup groupWithRoles(long... roleIDs) {
		RoleGroup group = new RoleGroup();
		group.setRoles(roles(roleIDs));
		return group;
	}

	private List<Role> roles(long... roleIDs) {
		List<Role> roles = new ArrayList<>(roleIDs.length);
		for (long roleId : roleIDs) {
			roles.add(role(roleId, "role " + roleId));
		}
		return roles;
	}

	private Role role(long id, String name) {
		Role role = new Role();
		role.setId(id);
		role.setName(name);
		return role;
	}
}
