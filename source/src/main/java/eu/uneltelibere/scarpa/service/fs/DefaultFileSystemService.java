package eu.uneltelibere.scarpa.service.fs;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DefaultFileSystemService implements FileSystemService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultFileSystemService.class);

	@Override
	public void ensureDirExists(String dir) throws URISyntaxException, IOException {
		Path path = Paths.get(dir);
		if (Files.notExists(path)) {
			LOG.info(path + " does not exist, creating.");
			Files.createDirectories(path);
		}
	}
}
