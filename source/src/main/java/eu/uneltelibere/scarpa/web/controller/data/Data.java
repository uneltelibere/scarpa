package eu.uneltelibere.scarpa.web.controller.data;

public class Data {

	public static final long NEW_ID = -1;

	private long id = NEW_ID;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
