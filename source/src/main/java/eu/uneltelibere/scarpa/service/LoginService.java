package eu.uneltelibere.scarpa.service;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.User;

@Service
public class LoginService implements UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(LoginService.class);

	@Autowired
	private AuthorizedUserService authorizedUserService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = authorizedUserService.getUserDetails(username).orElseGet(User::new);
		return new UserDetails() {

			private static final long serialVersionUID = 1L;

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				Set<String> roleCodes = new LinkedHashSet<>();
				for (Role role : user.getRoles()) {
					roleCodes.add(role.getCode());
				}
				LOG.info("Roles of {}: {}.", user, roleCodes);

				Collection<GrantedAuthority> authorities = new LinkedList<>();
				for (String roleCode : roleCodes) {
					authorities.add(new SimpleGrantedAuthority(roleCode));
				}
				return authorities;
			}

			@Override
			public String getPassword() {
				return user.getPassword();
			}

			@Override
			public String getUsername() {
				return user.getUsername();
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return user.isCredentialsNonExpired();
			}

			@Override
			public boolean isEnabled() {
				return user.isEnabled();
			}
		};
	}
}
