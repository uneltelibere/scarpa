package eu.uneltelibere.scarpa.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationLogger implements ApplicationRunner {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationLogger.class);

	private static final String TEMP_UPLOAD_LOCATION = "spring.servlet.multipart.location";
	private static final String DEFAULT_CONFIGURATION_VALUE = "unset";

	@Value("${" + TEMP_UPLOAD_LOCATION + ":" + DEFAULT_CONFIGURATION_VALUE + "}")
	private String tempUploadLocation;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		LOG.info("{} = {}", TEMP_UPLOAD_LOCATION, tempUploadLocation);
	}
}
