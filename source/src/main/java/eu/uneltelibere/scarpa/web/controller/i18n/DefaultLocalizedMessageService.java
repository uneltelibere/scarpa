package eu.uneltelibere.scarpa.web.controller.i18n;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

@Service
public class DefaultLocalizedMessageService implements LocalizedMessageService {

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private ResourceBundleMessageSource resourceBundleMessageSource;

	@Override
	public String getMessageTemplate(String code, HttpServletRequest request) {
		return getMessage(code, request);
	}

	@Override
	public String getMessage(String code, HttpServletRequest request) {
		return getMessage(code, new Object[] {}, request);
	}

	@Override
	public String getMessage(String code, Object[] arguments, HttpServletRequest request) {
		Locale locale = localeResolver.resolveLocale(request);
		String localizedTemplate = resourceBundleMessageSource.getMessage(code, arguments, locale);
		return localizedTemplate;
	}
}
