package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcRoleDao extends JdbcMasterDao implements RoleDao {

	// @formatter:off
	private static final String SUBQUERY_GET_ROLES = "SELECT "
			+ "a.id AS role_id, a.code AS role_code, a.label AS role_name, "
			+ "a.description_translation_id "
			+ "FROM authorities a ";

	private static final String GET_ROLES = SUBQUERY_GET_ROLES
			+ "ORDER BY a.id";

	private static final String GET_ROLE = SUBQUERY_GET_ROLES
			+ "WHERE a.id = :id";

	private static final String GET_ROLES_OF_USER = SUBQUERY_GET_ROLES
			+ "LEFT JOIN user_authority ua ON ua.authority_id = a.id "
			+ "WHERE ua.user_id = :userId";

	private static final String GET_ROLES_IN_GROUP = SUBQUERY_GET_ROLES
			+ "LEFT JOIN role_group_role rg ON rg.role_id = a.id "
			+ "WHERE rg.role_group_id = :groupId";

	private static final String UPDATE_ROLE_LABEL = "UPDATE authorities SET "
			+ "label = :label "
			+ "WHERE id = :id";
	// @formatter:on

	@Override
	public List<Role> getRoles() {
		return jdbcTemplate.query(GET_ROLES, new RowMapper<Role>() {

			@Override
			public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
				return roleFromResultSet(rs);
			}
		});
	}

	@Override
	public Optional<Role> getRole(long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbcTemplate.query(GET_ROLE, params, new ResultSetExtractor<Optional<Role>>() {

			@Override
			public Optional<Role> extractData(ResultSet rs) throws SQLException, DataAccessException {
				Optional<Role> maybeRole = Optional.empty();
				if (rs.next()) {
					Role role = roleFromResultSet(rs);
					maybeRole = Optional.of(role);
				}
				return maybeRole;
			}
		});
	}

	@Override
	public List<Role> getRolesOfUser(User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", user.getId());
		return jdbcTemplate.query(GET_ROLES_OF_USER, params, new RowMapper<Role>() {

			@Override
			public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
				return roleFromResultSet(rs);
			}
		});
	}

	@Override
	public List<Role> getRolesInGroup(RoleGroup group) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("groupId", group.getId());
		return jdbcTemplate.query(GET_ROLES_IN_GROUP, params, new RowMapper<Role>() {

			@Override
			public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
				return roleFromResultSet(rs);
			}
		});
	}

	private Role roleFromResultSet(ResultSet rs) throws SQLException {
		Translation translation = new Translation();
		Integer translationId = rs.getObject("description_translation_id", Integer.class);
		if (translationId != null) {
			translation.setId(translationId);
		}

		Role role = new Role();
		role.setId(rs.getLong("role_id"));
		role.setCode(rs.getString("role_code"));
		role.setName(rs.getString("role_name"));
		role.setTranslatedDescription(translation);
		return role;
	}

	@Override
	public void update(Role role) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", role.getId());
		params.addValue("label", role.getName());
		jdbcTemplate.update(UPDATE_ROLE_LABEL, params);
	}
}
