package eu.uneltelibere.scarpa.service;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.model.QuickSearchResultEntry;

public interface QuickSearchService<T extends Entity> {

	List<QuickSearchResultEntry> search(String searchString);

	String label(T entity);
}
