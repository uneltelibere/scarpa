CREATE TABLE IF NOT EXISTS attachment (
	id SERIAL PRIMARY KEY NOT NULL,
	original_filename VARCHAR(1024) NOT NULL,
	size INT,
	content_type VARCHAR(128) NOT NULL
);
