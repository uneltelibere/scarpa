package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetUtil {

	private ResultSetUtil() {
	}

	public static String getStringOrEmpty(ResultSet rs, String columnLabel) throws SQLException {
		String value = rs.getString(columnLabel);
		if (rs.wasNull()) {
			value = "";
		}
		return value;
	}
}
