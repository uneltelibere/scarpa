package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.api.LocalizedPagedRetrievingService;
import eu.uneltelibere.scarpa.api.PagedRetrievingService;
import eu.uneltelibere.scarpa.model.User;

public interface UserService extends PagedRetrievingService<User>, LocalizedPagedRetrievingService<User> {

	/**
	 * Gets the user with the specified ID.
	 * 
	 * @param id the ID of the user to fetch
	 * @return an Optional user with the specified ID
	 */
	Optional<User> getById(long id);

	/**
	 * Gets the user with the specified ID, using the specified locale for
	 * translatable attributes.
	 * 
	 * @param id     the ID of the user to fetch
	 * @param locale the locale to use for translatable attributes
	 * @return an Optional user with the specified ID
	 */
	Optional<User> getById(long id, Locale locale);

	User getOrMake(long id);

	Optional<User> getUser(String username, Locale locale);

	Optional<User> getUserByEmail(String email);

	Optional<User> getUserDetails(String username);

	Optional<User> currentUser(HttpServletRequest httpServletRequest);

	User newUser(Locale locale);

	User save(User user);

	long saveWithoutPassword(User user);

	long register(User user);

	void setRoleGroupsForUser(final User user, List<Long> groupIDs);

	Future<Boolean> resetPassword(User user);

	void setPassword(User user, String encodedPassword);

	void delete(long userId);

	boolean userHasRole(User user, eu.uneltelibere.scarpa.api.Role roleToCheck);
}
