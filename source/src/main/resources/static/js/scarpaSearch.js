(function() {
	var adjustVisibilityOfRightHandOperator = function(item) {
		var op = item.children(':selected').data('operator');
		var target = item.data('right-hand');
		var rh = $('#' + target)
		if (op === 'unary') {
			rh.hide();
		} else {
			rh.show();
		}
	};

	$(document).ready(function() {
		$("*[data-operator] select").each(function() {
			adjustVisibilityOfRightHandOperator($(this));
		});
	});

	$("select").change(function() {
		adjustVisibilityOfRightHandOperator($(this));
	});
})();
