package eu.uneltelibere.scarpa.service.enums;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.ScarpaEnum;

public interface ScarpaEnumService<T extends ScarpaEnum> {

	Optional<T> getById(long id);

	List<T> getAll();
}
