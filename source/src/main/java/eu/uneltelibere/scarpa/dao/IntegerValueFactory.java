package eu.uneltelibere.scarpa.dao;

public class IntegerValueFactory implements ValueFactory {

	@Override
	public Object createValue(String valueAsString) {
		return Integer.parseInt(valueAsString);
	}
}
