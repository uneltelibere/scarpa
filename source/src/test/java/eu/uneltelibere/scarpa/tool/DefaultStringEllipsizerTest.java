package eu.uneltelibere.scarpa.tool;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DefaultStringEllipsizerTest {

	private StringEllipsizer ellipsizer = new DefaultStringEllipsizer();

	@Test
	public void testEllipsizeLong() {
		assertEquals("abcd…", ellipsizer.ellipsize("abcdef", 4));
	}

	@Test
	public void testEllipsizeFitPlusOne() {
		assertEquals("abcdef", ellipsizer.ellipsize("abcdef", 5));
	}

	@Test
	public void testEllipsizeFit() {
		assertEquals("abcdef", ellipsizer.ellipsize("abcdef", 6));
	}

	@Test
	public void testEllipsizeShort() {
		assertEquals("abcdef", ellipsizer.ellipsize("abcdef", 7));
	}
}
