package eu.uneltelibere.scarpa.service;

import java.io.File;
import java.util.List;

public interface LogService {

	List<File> logFiles();

	File fileNamed(String fileName);
}
