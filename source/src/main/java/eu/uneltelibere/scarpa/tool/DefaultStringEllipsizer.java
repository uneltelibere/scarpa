package eu.uneltelibere.scarpa.tool;

import org.springframework.stereotype.Component;

@Component
public class DefaultStringEllipsizer implements StringEllipsizer {

	private static final char ELLIPSIS = '\u2026';

	@Override
	public String ellipsize(String s, int maxChars) {
		if (s == null || s.length() <= maxChars + 1) {
			return s;
		} else {
			return s.substring(0, maxChars) + ELLIPSIS;
		}
	}
}
