package eu.uneltelibere.scarpa.web.controller.data.column;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.LocaleResolver;

import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractDateColumnFactory<T extends Entity> extends AbstractTableColumnFactory<T> {

	private static final String NEVER = "-";

	@Autowired
	private LocaleResolver localeResolver;

	@Override
	protected Function<T, String> columnContent(HttpServletRequest request) {
		Locale locale = localeResolver.resolveLocale(request);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(datePattern(), locale);
		return entity -> {
			LocalDate lastDate = entityDate().apply(entity);
			if (lastDate != null) {
				return lastDate.format(dateTimeFormatter);
			} else {
				return NEVER;
			}
		};
	}

	protected abstract Function<T, LocalDate> entityDate();

	protected abstract String datePattern();
}
