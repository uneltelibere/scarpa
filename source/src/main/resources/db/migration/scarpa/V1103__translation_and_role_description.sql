ALTER TABLE user_authority
	DROP CONSTRAINT fk_user_authority_authorities,
	ADD CONSTRAINT fk_user_authority_authorities
		FOREIGN KEY(authority_id)
		REFERENCES authorities(id)
		ON DELETE CASCADE;

DELETE FROM authorities WHERE code = 'ROLE_LOG_VIEWER';

--

CREATE TABLE IF NOT EXISTS language (
	id SERIAL PRIMARY KEY NOT NULL,
	code VARCHAR(16) UNIQUE NOT NULL,
	name VARCHAR
);

CREATE TABLE IF NOT EXISTS translation (
	id SERIAL PRIMARY KEY NOT NULL,
	neutral_text VARCHAR
);

CREATE TABLE IF NOT EXISTS translation_text (
	id SERIAL PRIMARY KEY NOT NULL,
	translation_id INT REFERENCES translation,
	language_id INT REFERENCES language,
	translated_text VARCHAR
);

ALTER TABLE authorities
	ADD COLUMN description_translation_id INT REFERENCES translation;
