package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

public interface RoleGroupService {

	List<RoleGroup> retrieve(Locale locale);

	Optional<RoleGroup> retrieve(long id, Locale locale);

	Optional<RoleGroup> retrieveByCode(String code, Locale locale);

	RoleGroup create(Locale locale);

	void save(RoleGroup roleGroup);

	void setRolesForGroup(final RoleGroup group, List<Long> roleIDs);

	List<RoleGroup> getRoleGroupsOfUser(User user);
}
