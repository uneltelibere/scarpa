package eu.uneltelibere.scarpa.spring;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "scarpa.db")
@Validated
public class JdbcProperties {

	private final String driverClassName = "org.postgresql.Driver";

	@NotEmpty
	private String host;
	@NotEmpty
	private String port;
	@NotEmpty
	private String dbName;
	@NotEmpty
	private String masterSchema;
	@NotEmpty
	private String applicationSchema;
	@NotEmpty
	private String username;
	@NotEmpty
	private String password;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String db) {
		this.dbName = db;
	}

	public String getMasterSchema() {
		return masterSchema;
	}

	public void setMasterSchema(String masterSchema) {
		this.masterSchema = masterSchema;
	}

	public String getApplicationSchema() {
		return applicationSchema;
	}

	public void setApplicationSchema(String applicationSchema) {
		this.applicationSchema = applicationSchema;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public String getUrl() {
		return String.format("jdbc:postgresql://%s:%s/%s", host, port, dbName);
	}

	public String getMasterUrl() {
		return String.format("jdbc:postgresql://%s:%s/%s?currentSchema=%s", host, port, dbName, masterSchema);
	}

	public String getApplicationUrl() {
		return String.format("jdbc:postgresql://%s:%s/%s?currentSchema=%s", host, port, dbName, applicationSchema);
	}
}
