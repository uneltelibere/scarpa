package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.InstallAdminRole;

@Component
public class InstallMenu extends AbstractMenu{

	public InstallMenu() {
		super("menu.install", "/install", new InstallAdminRole());
	}
}
