package eu.uneltelibere.scarpa.service.user;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.service.ValidationException;

@ExtendWith(MockitoExtension.class)
class ScarpaUserValidationServiceTest {

	private static final String USER = "user";
	private static final long EXISTING_ID = 123;
	private static final String INVALID_MAIL = "someone-at-example-dot-com";
	private static final String VALID_MAIL = "someone@example.com";

	@Mock
	private UserDuplicateAttributeDetectionService userDuplicateAttributeDetectionService;

	@Mock
	private SettingService settingService;

	@InjectMocks
	private UserValidationService userValidationService = new ScarpaUserValidationService();

	@Test
	@DisplayName("Reject null user")
	void rejectNullUser() {
		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(null));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_MISSING_DATA),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject null email")
	void rejectNullEmail() {
		User newUser = new User.Builder().id(User.NEW_ID).email(null).build();
		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_MISSING_EMAIL),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject null username")
	void rejectNullUsername() {
		User newUser = new User.Builder().id(User.NEW_ID).username(null).build();
		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_MISSING_USERNAME),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject invalid e-mail address")
	void rejectInvalidMail() {
		User existingUser = new User.Builder().id(EXISTING_ID).email(INVALID_MAIL).build();

		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(existingUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_INVALID_MAIL),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject email from other domain")
	void rejectEmailFromOtherDomain() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).build();
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING)).thenReturn(Optional.of("foo.com"));
		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.MESSAGE_OTHER_DOMAIN),
				() -> "Message was " + thrown.getMessage());
		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}

	@Test
	@DisplayName("Reject email from regex-matching similar domain")
	void rejectEmailFromSimilarDomain() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).build();
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING))
				.thenReturn(Optional.of("examplexcom"));
		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.MESSAGE_OTHER_DOMAIN),
				() -> "Message was " + thrown.getMessage());
		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}

	@Test
	@DisplayName("Reject duplicate e-mail address")
	void rejectDuplicateMailUpdate() {
		User existingUser = new User.Builder().id(EXISTING_ID).email(VALID_MAIL).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(existingUser)).thenReturn(false);

		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(existingUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_DUPLICATE_MAIL),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject existing username")
	void rejectExistingUsername() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).username(USER).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(newUser)).thenReturn(true);
		when(userDuplicateAttributeDetectionService.isUsernameUnique(newUser)).thenReturn(false);

		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_DUPLICATE_USERNAME),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Reject non-alphanumeric username")
	void rejectNonAlphanumericUsername() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).username("bo$$").build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(newUser)).thenReturn(true);

		ValidationException thrown = assertThrows(ValidationException.class,
				() -> userValidationService.validate(newUser));
		assertTrue(thrown.getMessage().contains(ScarpaUserValidationService.VALIDATION_INVALID_USERNAME),
				() -> "Message was " + thrown.getMessage());
	}

	@Test
	@DisplayName("Accept when missing setting")
	void acceptMissingSetting() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).username(USER).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(newUser)).thenReturn(true);
		when(userDuplicateAttributeDetectionService.isUsernameUnique(newUser)).thenReturn(true);
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING)).thenReturn(Optional.empty());

		userValidationService.validate(newUser);

		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}

	@Test
	@DisplayName("Accept update without username when missing setting")
	void acceptUpdateNoNameMissingSetting() {
		User existingUser = new User.Builder().id(EXISTING_ID).email(VALID_MAIL).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(existingUser)).thenReturn(true);
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING)).thenReturn(Optional.empty());

		userValidationService.validate(existingUser);

		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}

	@Test
	@DisplayName("Accept")
	void accept() {
		User newUser = new User.Builder().id(User.NEW_ID).email(VALID_MAIL).username(USER).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(newUser)).thenReturn(true);
		when(userDuplicateAttributeDetectionService.isUsernameUnique(newUser)).thenReturn(true);
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING))
				.thenReturn(Optional.of("example.com,foo.com"));

		userValidationService.validate(newUser);

		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}

	@Test
	@DisplayName("Accept update without username")
	void acceptUpdateNoName() {
		User existingUser = new User.Builder().id(EXISTING_ID).email(VALID_MAIL).build();
		when(userDuplicateAttributeDetectionService.isEmailUnique(existingUser)).thenReturn(true);
		when(settingService.get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING))
				.thenReturn(Optional.of("example.com,foo.com"));

		userValidationService.validate(existingUser);

		verify(settingService).get(ScarpaUserValidationService.EMAIL_DOMAIN_SETTING);
	}
}
