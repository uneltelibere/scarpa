package eu.uneltelibere.scarpa.service;

import java.io.File;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultLogService implements LogService {

	@Autowired
	private DiagnosticConfigurationService diagnosticConfigurationService;

	@Override
	public List<File> logFiles() {
		return Collections.emptyList();
	}

	@Override
	public File fileNamed(String fileName) {
		String uploadDir = diagnosticConfigurationService.logDirectory();
		return Paths.get(uploadDir, fileName).toFile();
	}
}
