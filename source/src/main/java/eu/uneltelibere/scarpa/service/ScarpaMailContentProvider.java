package eu.uneltelibere.scarpa.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import eu.uneltelibere.scarpa.RuntimeContext;
import eu.uneltelibere.scarpa.api.MailContent;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

@Service("scarpaMailContentProvider")
public class ScarpaMailContentProvider implements MailContentProvider {

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private RuntimeContext runtimeContext;

	@Autowired
	private TokenService tokenService;

	@Value("${scarpa.app.name}")
	private String appName;

	@Override
	public MailContent welcomeMailContent(Token token) {
		MailContent content = new MailContent();

		String userName = token.getUser().getUsername();
		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		Context ctx = new Context(locale);
		ctx.setVariable("applicationName", appName);
		ctx.setVariable("username", userName);
		ctx.setVariable("passwordResetUrl", tokenService.tokenUrl(token));
		String htmlBody = templateEngine.process("scarpaWelcomeToken", ctx);
		content.setHtmlBody(htmlBody);

		return content;
	}

	@Override
	public MailContent welcomeMailContent(User user) {
		MailContent content = new MailContent();

		String userName = user.getUsername();
		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		Context ctx = new Context(locale);
		ctx.setVariable("applicationName", appName);
		ctx.setVariable("username", userName);
		ctx.setVariable("applicationUrl", runtimeContext.getAppContext());
		String htmlBody = templateEngine.process("scarpaWelcomeSelfRegistered", ctx);
		content.setHtmlBody(htmlBody);

		return content;
	}

	@Override
	public MailContent recoveryMailContent(Token token) {
		MailContent content = new MailContent();

		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		Context ctx = new Context(locale);
		ctx.setVariable("passwordResetUrl", tokenService.tokenUrl(token));
		String htmlBody = templateEngine.process("scarpaRecover", ctx);
		content.setHtmlBody(htmlBody);

		return content;
	}
}
