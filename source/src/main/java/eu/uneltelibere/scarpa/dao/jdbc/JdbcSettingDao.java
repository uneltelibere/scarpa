package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.SettingDao;
import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcSettingDao extends JdbcMasterDao implements SettingDao {

	// @formatter:off
	private static final String NEW_SETTING = "INSERT INTO setting ("
			+ "setting_key, setting_value"
			+ ") VALUES ("
			+ ":key, :value)";

	private static final String NEW_USER_SETTING = "INSERT INTO user_setting ("
			+ "user_id, setting_key, setting_value"
			+ ") VALUES ("
			+ ":userId, :key, :value)";

	private static final String GET_SETTING = "SELECT s.id, s.setting_key AS key, s.setting_value AS value "
			+ "FROM setting s "
			+ "WHERE s.setting_key = :key";

	private static final String GET_SETTINGS = "SELECT s.id, s.setting_key AS key, s.setting_value AS value "
			+ "FROM setting s "
			+ "WHERE s.setting_key LIKE :keyPrefix "
			+ "ORDER BY s.setting_key ";

	private static final String GET_USER_SETTING = "SELECT s.id, s.setting_key AS key, s.setting_value AS value "
			+ "FROM user_setting s "
			+ "WHERE s.setting_key = :key "
			+ "AND s.user_id = :userId";

	private static final String UPDATE_SETTING = "UPDATE setting "
			+ "SET setting_value = :value "
			+ "WHERE id = :id";

	private static final String UPDATE_USER_SETTING = "UPDATE user_setting "
			+ "SET setting_value = :value "
			+ "WHERE id = :id "
			+ "AND user_id = :userId";
	// @formatter:on

	private ResultSetExtractor<Optional<Setting>> resultSetExtractor = new ResultSetExtractor<Optional<Setting>>() {

		@Override
		public Optional<Setting> extractData(ResultSet rs) throws SQLException, DataAccessException {
			Optional<Setting> maybeSetting = Optional.empty();
			if (rs.next()) {
				maybeSetting = Optional.of(entityFromResultSet(rs));
			}
			return maybeSetting;
		}
	};

	private RowMapper<Setting> rowMapper = new RowMapper<Setting>() {

		@Override
		public Setting mapRow(ResultSet rs, int rowNum) throws SQLException {
			return entityFromResultSet(rs);
		}
	};

	private Setting entityFromResultSet(ResultSet rs) throws SQLException {
		Setting setting = new Setting();
		setting.setId(rs.getInt("id"));
		setting.setKey(rs.getString("key"));
		setting.setValue(rs.getString("value"));
		return setting;
	}

	@Override
	public long create(Setting setting) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("key", setting.getKey());
		params.addValue("value", setting.getValue());
		jdbcTemplate.update(NEW_SETTING, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public long create(Setting setting, User user) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("key", setting.getKey());
		params.addValue("value", setting.getValue());
		params.addValue("userId", user.getId());
		jdbcTemplate.update(NEW_USER_SETTING, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public Optional<Setting> retrieve(String key) {
		SqlParameterSource params = new MapSqlParameterSource("key", key);
		return jdbcTemplate.query(GET_SETTING, params, resultSetExtractor);
	}

	@Override
	public List<Setting> retrieveFromGroup(String keyPrefix) {
		SqlParameterSource params = new MapSqlParameterSource("keyPrefix", keyPrefix + "%");
		return jdbcTemplate.query(GET_SETTINGS, params, rowMapper);
	}

	@Override
	public Optional<Setting> retrieve(String key, User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("key", key);
		params.addValue("userId", user.getId());
		return jdbcTemplate.query(GET_USER_SETTING, params, resultSetExtractor);
	}

	@Override
	public void update(Setting setting) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", setting.getId());
		params.addValue("value", setting.getValue());
		jdbcTemplate.update(UPDATE_SETTING, params);
	}

	@Override
	public void update(Setting setting, User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", setting.getId());
		params.addValue("value", setting.getValue());
		params.addValue("userId", user.getId());
		jdbcTemplate.update(UPDATE_USER_SETTING, params);
	}
}
