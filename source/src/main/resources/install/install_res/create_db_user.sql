DO 
$body$
DECLARE
    user_count integer;
BEGIN
    SELECT COUNT(*) 
        INTO user_count
    FROM pg_user
    WHERE usename = 'DB_USER';

    IF user_count = 0 THEN
        CREATE ROLE DB_USER LOGIN ENCRYPTED PASSWORD 'DB_PASS';
    ELSE
        RAISE NOTICE 'Database user already exists'; 
    END IF;
END
$body$
;

