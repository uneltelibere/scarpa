package eu.uneltelibere.scarpa.web.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.UserService;

@Controller
@RequestMapping("/forgot")
public class ForgotPasswordController {

	private static final Logger LOG = LoggerFactory.getLogger(ForgotPasswordController.class);

	@Autowired
	private UserService userService;

	@GetMapping
	public String showForgotPasswordForm(Model model) {
		LOG.info("Showing the forgot password page");
		return "forgot";
	}

	@PostMapping
	public String forgotPassword(@RequestParam("email") String email, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		LOG.info("Password reset was requested for user {}", email);

		Optional<User> maybeUser = userService.getUserByEmail(email);
		if (maybeUser.isPresent()) {
			User user = maybeUser.get();
			LOG.info("User with email {} was found, sending recovery mail", email);
			userService.resetPassword(user);
			return "redirect:/login";
		} else {
			LOG.info("User with email {} was not found", email);
			redirectAttributes.addFlashAttribute("emailNotFound", "true");
			return "redirect:/forgot";
		}
	}
}
