package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.List;

import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.web.controller.data.Option;
import eu.uneltelibere.scarpa.web.controller.data.OptionBuilder;
import eu.uneltelibere.scarpa.web.controller.data.Select;

@Service
public class DefaultTableColumnOptionsProvider implements TableColumnOptionsProvider {

	@Override
	public Select<String> columnHeadingOptions(List<TableColumn.Heading> columnHeadings) {
		Select<String> select = new Select<>();

		for (TableColumn.Heading heading : columnHeadings) {
			String label;
			if (heading.getAbbreviationTitle().isEmpty()) {
				label = heading.getText();
			} else {
				label = String.format("%s (%s)", heading.getText(), heading.getAbbreviationTitle());
			}
			Option<String> option = new OptionBuilder<String>().withLabel(label).withValue(heading.getCode()).build();
			select.getOptions().add(option);
		}

		return select;
	}
}
