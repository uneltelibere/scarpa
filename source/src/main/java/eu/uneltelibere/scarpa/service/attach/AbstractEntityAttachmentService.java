package eu.uneltelibere.scarpa.service.attach;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.EntityAttachmentsDao;
import eu.uneltelibere.scarpa.dao.Retriever;
import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.model.EntityWithAttachments;

public abstract class AbstractEntityAttachmentService<T extends EntityWithAttachments>
		implements EntityAttachmentService<T> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractEntityAttachmentService.class);

	@Autowired
	private Retriever<T> entityDao;

	@Autowired
	private EntityAttachmentsDao<T> entityAttachmentsDao;

	@Override
	public void setAttachments(T entity, List<Long> attachmentIDs) {
		Optional<T> maybeEntityInDb = entityDao.retrieve(entity.getId());
		if (maybeEntityInDb.isPresent()) {
			T entityInDb = maybeEntityInDb.get();
			List<Long> existingAttachments = entityAttachmentsDao.getAttachments(entityInDb);

			List<Long> attachmentsToRemove = existingAttachments.stream()
					.filter(attachment -> !attachmentIDs.contains(attachment)).collect(Collectors.toList());
			if (!attachmentsToRemove.isEmpty()) {
				entityAttachmentsDao.removeAttachments(entityInDb, attachmentsToRemove);
			}

			List<Long> attachmentsToAdd = attachmentIDs.stream()
					.filter(attachment -> !existingAttachments.contains(attachment)).collect(Collectors.toList());
			if (!attachmentsToAdd.isEmpty()) {
				entityAttachmentsDao.addAttachments(entityInDb, attachmentsToAdd);
			}
		} else {
			LOG.warn("Entity was not found in the database.");
		}
	}

	@Override
	public void addAttachment(Attachment attachment, T entity) {
		Collection<Attachment> atts = entity.getAttachments();
		atts.add(attachment);
		entity.setAttachments(atts);
	}

	@Override
	public void removeAttachment(long idOfAttachmentToRemove, T entity) {
		entity.getAttachments().removeIf(attachment -> attachment.getId() == idOfAttachmentToRemove);
	}
}
