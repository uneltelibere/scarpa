package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;

public interface SettingService {

	void set(String key, String value);

	void set(String key, String value, User user);

	Optional<String> get(String key);

	List<Setting> getFromGroup(String keyPrefix);

	Optional<String> get(String key, User user);
}
