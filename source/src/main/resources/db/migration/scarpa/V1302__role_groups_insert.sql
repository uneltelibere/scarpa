DO $$
	DECLARE
		language_en_id_value language.id%TYPE;
		language_ro_id_value language.id%TYPE;
		translation_id_value translation.id%TYPE;
	BEGIN
		SELECT language.id INTO language_en_id_value FROM language WHERE code = 'en';
		SELECT language.id INTO language_ro_id_value FROM language WHERE code = 'ro';

		-- group Administrator
		INSERT INTO role_group(code)
		VALUES ('GROUP_ADMIN');

		INSERT INTO translation(neutral_text) VALUES('Administrator')
		RETURNING id INTO translation_id_value;

		INSERT INTO translation_text(translation_id, language_id, translated_text)
		VALUES(
			translation_id_value,
			language_en_id_value,
			'Administrator'
		);

		INSERT INTO translation_text(translation_id, language_id, translated_text)
		VALUES(
			translation_id_value,
			language_ro_id_value,
			'Administrator'
		);

		UPDATE role_group SET name_translation_id = translation_id_value
		WHERE code = 'GROUP_ADMIN';

		-- group Basic user
		INSERT INTO role_group(code)
		VALUES ('GROUP_USER');

		INSERT INTO translation(neutral_text) VALUES('Basic user')
		RETURNING id INTO translation_id_value;

		INSERT INTO translation_text(translation_id, language_id, translated_text)
		VALUES(
			translation_id_value,
			language_en_id_value,
			'Basic user'
		);

		INSERT INTO translation_text(translation_id, language_id, translated_text)
		VALUES(
			translation_id_value,
			language_ro_id_value,
			'Utilizator de bază'
		);

		UPDATE role_group SET name_translation_id = translation_id_value
		WHERE code = 'GROUP_USER';
	END
$$;

INSERT INTO role_group_role(role_group_id, role_id)
VALUES(
	(SELECT id FROM role_group WHERE code = 'GROUP_ADMIN'),
	(SELECT id FROM authorities WHERE code = 'ROLE_USER_ADMIN')
), (
	(SELECT id FROM role_group WHERE code = 'GROUP_ADMIN'),
	(SELECT id FROM authorities WHERE code = 'ROLE_INSTALL_ADMIN')
), (
	(SELECT id FROM role_group WHERE code = 'GROUP_USER'),
	(SELECT id FROM authorities WHERE code = 'ROLE_USER')
);
