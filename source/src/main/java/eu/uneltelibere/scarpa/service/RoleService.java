package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;

public interface RoleService {

	List<Role> retrieve(Locale locale);

	List<Role> retrieve(RoleGroup group, Locale locale);

	Optional<Role> retrieve(long id, Locale locale);

	Role save(Role role, Locale locale);
}
