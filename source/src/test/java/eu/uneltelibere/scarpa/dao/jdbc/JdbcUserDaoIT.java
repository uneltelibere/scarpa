package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcUserDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private UserDao userDao;

	@Test
	@DatabaseSetup("/db/in/init.xml")
	@ExpectedDatabase(value = "/db/out/user/created_user.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreate() {
		User user = new User();
		user.setUsername("testuser");
		user.setEmail("someone@example.com");
		user.setPassword("secret");
		user.setEnabled(true);
		user.setCredentialsNonExpired(true);

		long id = userDao.create(user);

		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	public void testCount() {
		int count = userDao.count();
		assertEquals(1, count);
	}

	@Test
	@DatabaseSetup("/db/in/user/eight_users.xml")
	public void testCountRetrieveFirstPage() {
		List<User> page = userDao.retrieve(0, 5);
		assertEquals(5, page.size());
		assertEquals(1, page.get(0).getId());
		assertEquals(5, page.get(4).getId());
	}

	@Test
	@DatabaseSetup("/db/in/user/eight_users.xml")
	public void testCountRetrieveLastPage() {
		List<User> page = userDao.retrieve(5, 5);
		assertEquals(3, page.size());
		assertEquals(6, page.get(0).getId());
		assertEquals(8, page.get(2).getId());
	}

	@Test
	@DatabaseSetup("/db/in/user/eight_users.xml")
	public void testCountRetrieveNoPage() {
		List<User> page = userDao.retrieve(0, 0);
		assertEquals(0, page.size());
	}

	@Test
	@DatabaseSetup("/db/in/init.xml")
	public void testCountNoData() {
		int count = userDao.count();
		assertEquals(0, count);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	public void testGetById() {
		Optional<User> userInDb = userDao.getUser(1);
		assertEquals(1, userInDb.get().getId());
		assertEquals("user1", userInDb.get().getUsername());
		assertEquals("", userInDb.get().getPassword());
	}

	@Test
	public void testGetByIdNonexistent() {
		Optional<User> userInDb = userDao.getUser(-1);
		assertFalse(userInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	public void testGetByUserame() {
		Optional<User> userInDb = userDao.getUser("user1");
		assertEquals(1, userInDb.get().getId());
		assertEquals("", userInDb.get().getPassword());
	}

	@Test
	public void testGetByUsernameNonexistent() {
		Optional<User> userInDb = userDao.getUser("nobody");
		assertFalse(userInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	public void testGetByEmail() {
		Optional<User> userInDb = userDao.getUserByEmail("user1@example.com");
		assertEquals(1, userInDb.get().getId());
		assertEquals("", userInDb.get().getPassword());
	}

	@Test
	public void testGetByEmailNonexistent() {
		Optional<User> userInDb = userDao.getUserByEmail("nobody@nowhere.com");
		assertFalse(userInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	public void testGetUserDetails() {
		Optional<User> userInDb = userDao.getUserDetails("user1");
		assertEquals(1, userInDb.get().getId());
		assertEquals("secret", userInDb.get().getPassword());
	}

	@Test
	public void testGetUserDetailsNonexistent() {
		Optional<User> userInDb = userDao.getUserDetails("nobody");
		assertFalse(userInDb.isPresent());
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_updated.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdate() {
		User user = userWithUpdatedAttributes(1);
		userDao.update(user);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	@ExpectedDatabase(value = "/db/out/user/user_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateNonexistent() {
		User user = userWithUpdatedAttributes(-1);
		userDao.update(user);
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_updated_without_password.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateWithoutPassword() {
		User user = userWithUpdatedAttributes(1);
		userDao.updateWithoutPassword(user);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	@ExpectedDatabase(value = "/db/out/user/user_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateWithoutPasswordNonexistent() {
		User user = userWithUpdatedAttributes(-1);
		userDao.updateWithoutPassword(user);
	}

	private User userWithUpdatedAttributes(long id) {
		User user = new User();
		user.setId(id);
		user.setEmail("someone-updated@example.com");
		user.setPassword("secret-updated");
		user.setEnabled(false);
		return user;
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_updated_changed_password.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testSetPassword() {
		User user = new User();
		user.setId(1);
		user.setPassword("secret-updated");
		userDao.setPassword(user);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	@ExpectedDatabase(value = "/db/out/user/user_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testSetPasswordNonexistent() {
		User user = new User();
		user.setId(-1);
		userDao.setPassword(user);
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml", "/db/in/user/user_two.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDelete() {
		userDao.delete(2);
	}

	@Test
	@DatabaseSetup("/db/in/user/user_one.xml")
	@ExpectedDatabase(value = "/db/out/user/user_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteNonexistent() {
		userDao.delete(-1);
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml", "/db/in/roleGroup/groups_12345.xml",
			"/db/in/user/user_groups_123.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_groups_2.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testRemoveGroupsFromUser() {
		User user = new User();
		user.setId(1);
		List<Long> groupIDsToRemove = Arrays.asList(1L, 3L);
		userDao.removeGroupsFromUser(user, groupIDsToRemove);
	}

	@Test
	@DatabaseSetup(value = { "/db/in/user/user_one.xml", "/db/in/roleGroup/groups_12345.xml",
			"/db/in/user/user_groups_123.xml" })
	@ExpectedDatabase(value = "/db/out/user/user_groups_12345.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testAddGroupsToUser() {
		User user = new User();
		user.setId(1);
		List<Long> groupIDsToRemove = Arrays.asList(4L, 5L);
		userDao.addGroupsToUser(user, groupIDsToRemove);
	}
}
