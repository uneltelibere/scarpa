package eu.uneltelibere.scarpa.dao;

import java.util.Optional;

import eu.uneltelibere.scarpa.model.Entity;

public interface Retriever<T extends Entity> {

	Optional<T> retrieve(long id);
}
