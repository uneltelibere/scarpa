package eu.uneltelibere.scarpa.filter;

import java.util.Collection;

public class OrConditionGroup<T> extends ConditionGroup<T> {

	@SafeVarargs
	public OrConditionGroup(Condition<T>... conditions) {
		super(conditions);
	}

	public OrConditionGroup(Collection<Condition<T>> conditions) {
		super(conditions);
	}
}
