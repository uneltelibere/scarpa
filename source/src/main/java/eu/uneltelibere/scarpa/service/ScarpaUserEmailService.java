package eu.uneltelibere.scarpa.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.RuntimeContext;
import eu.uneltelibere.scarpa.api.MailContent;
import eu.uneltelibere.scarpa.api.ScarpaMailer;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

@Service
public class ScarpaUserEmailService implements UserEmailService {

	private static final String SUBJECT_RESET = "mail.subject.recover";

	private static final String SUBJECT_WELCOME = "mail.subject.welcome";

	@Value("${app.password-reset.mail.from}")
	private String fromReset;

	@Value("${app.welcome.mail.from}")
	private String fromWelcome;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private RuntimeContext runtimeContext;

	@Autowired
	private ResourceBundleMessageSource resourceBundleMessageSource;

	@Value("${scarpa.app.name}")
	private String appName;

	@Override
	public boolean sendRecoveryMail(Token token) {
		MailContentProvider mailContentProvider = runtimeContext.getMailContentProvider();
		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		MailContent content = mailContentProvider.recoveryMailContent(token);
		String subjectReset = resourceBundleMessageSource.getMessage(SUBJECT_RESET, new Object[] {}, locale);

		return new ScarpaMailer().from(fromReset).to(token.getUser().getEmail()).subject(subjectReset).content(content)
				.sendHtmlMail(mailSender);
	}

	@Override
	public boolean sendWelcomeMail(Token token) {
		MailContentProvider mailContentProvider = runtimeContext.getMailContentProvider();
		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		MailContent content = mailContentProvider.welcomeMailContent(token);
		String subjectWelcome = resourceBundleMessageSource.getMessage(SUBJECT_WELCOME, new Object[] {}, locale);

		return new ScarpaMailer().from(fromWelcome).to(token.getUser().getEmail()).subject(subjectWelcome)
				.content(content).sendHtmlMail(mailSender);
	}

	@Override
	public boolean sendWelcomeMail(User user) {
		MailContentProvider mailContentProvider = runtimeContext.getMailContentProvider();
		Locale locale = Locale.forLanguageTag(runtimeContext.getDefaultLocale());
		MailContent content = mailContentProvider.welcomeMailContent(user);
		String subjectWelcome = resourceBundleMessageSource.getMessage(SUBJECT_WELCOME, new Object[] {}, locale);

		return new ScarpaMailer().from(fromWelcome).to(user.getEmail()).subject(subjectWelcome).content(content)
				.sendHtmlMail(mailSender);
	}
}
