package eu.uneltelibere.scarpa.web.controller.data.group;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.web.controller.data.AbstractWebEntityFactory;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityFactory;

@Service
public class WebUserFactory extends AbstractWebEntityFactory<User, WebUser> {

	@Autowired
	private WebEntityFactory<RoleGroup, WebRoleGroup> webRoleGroupFactory;

	@Override
	public WebUser webEntity(User user) {
		WebUser webUser = new WebUser();
		BeanUtils.copyProperties(user, webUser);
		webUser.setWebRoleGroups(webRoleGroupFactory.webEntities(user.getRoleGroups()));
		return webUser;
	}
}
