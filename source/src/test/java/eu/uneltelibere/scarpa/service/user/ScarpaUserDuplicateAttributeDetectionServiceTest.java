package eu.uneltelibere.scarpa.service.user;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.uneltelibere.scarpa.api.FilteredPagedRetrievingService;
import eu.uneltelibere.scarpa.model.User;

@ExtendWith(MockitoExtension.class)
class ScarpaUserDuplicateAttributeDetectionServiceTest {

	private static final long EXISTING_ID = 123;

	@Mock
	private FilteredPagedRetrievingService<User> userRetrievingService;

	@InjectMocks
	private UserDuplicateAttributeDetectionService userDuplicateAttributeDetectionService = new ScarpaUserDuplicateAttributeDetectionService();

	@Test
	@DisplayName("Detect duplicate email")
	void detectDuplicateEmail() {
		User newUser = new User.Builder().id(User.NEW_ID).build();
		User existingUser = new User.Builder().id(EXISTING_ID).build();

		when(userRetrievingService.count(any())).thenReturn(1);
		when(userRetrievingService.retrieveFiltered(any(), eq(0), eq(1))).thenReturn(singletonList(existingUser));

		assertFalse(userDuplicateAttributeDetectionService.isEmailUnique(newUser));
	}

	@Test
	@DisplayName("Detect unique email")
	void detectUniqueEmail() {
		User newUser = new User.Builder().id(User.NEW_ID).build();

		when(userRetrievingService.count(any())).thenReturn(0);

		assertTrue(userDuplicateAttributeDetectionService.isEmailUnique(newUser));
	}

	@Test
	@DisplayName("Detect unique email on update")
	void detectUniqueEmailOnUpdate() {
		User existingUser = new User.Builder().id(EXISTING_ID).build();

		when(userRetrievingService.count(any())).thenReturn(1);
		when(userRetrievingService.retrieveFiltered(any(), eq(0), eq(1))).thenReturn(singletonList(existingUser));

		assertTrue(userDuplicateAttributeDetectionService.isEmailUnique(existingUser));
	}

	@Test
	@DisplayName("Detect duplicate username")
	void detectDuplicateUsername() {
		User newUser = new User.Builder().id(User.NEW_ID).build();
		User existingUser = new User.Builder().id(EXISTING_ID).build();

		when(userRetrievingService.count(any())).thenReturn(1);
		when(userRetrievingService.retrieveFiltered(any(), eq(0), eq(1))).thenReturn(singletonList(existingUser));

		assertFalse(userDuplicateAttributeDetectionService.isUsernameUnique(newUser));
	}

	@Test
	@DisplayName("Detect unique username")
	void detectUniqueUsername() {
		User newUser = new User.Builder().id(User.NEW_ID).build();

		when(userRetrievingService.count(any())).thenReturn(0);

		assertTrue(userDuplicateAttributeDetectionService.isUsernameUnique(newUser));
	}

	@Test
	@DisplayName("Detect unique username on update")
	void detectUniqueUsernameOnUpdate() {
		User existingUser = new User.Builder().id(EXISTING_ID).build();

		when(userRetrievingService.count(any())).thenReturn(1);
		when(userRetrievingService.retrieveFiltered(any(), eq(0), eq(1))).thenReturn(singletonList(existingUser));

		assertTrue(userDuplicateAttributeDetectionService.isUsernameUnique(existingUser));
	}
}
