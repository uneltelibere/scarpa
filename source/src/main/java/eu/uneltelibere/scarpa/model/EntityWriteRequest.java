package eu.uneltelibere.scarpa.model;

import java.util.Objects;

public class EntityWriteRequest<T extends Entity> {

	private final T entity;

	private User actor = new User();

	private EntityWriteRequest(T entity) {
		this.entity = entity;
	}

	public static <T extends Entity> EntityWriteRequest<T> of(T entity) {
		return new EntityWriteRequest<>(entity);
	}

	public EntityWriteRequest<T> by(User actor) {
		this.actor = actor;
		return this;
	}

	public T getEntity() {
		return entity;
	}

	public User getActor() {
		return actor;
	}

	@Override
	public int hashCode() {
		return Objects.hash(actor, entity);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof EntityWriteRequest)) {
			return false;
		}
		EntityWriteRequest<?> other = (EntityWriteRequest<?>) obj;
		return Objects.equals(actor, other.actor) && Objects.equals(entity, other.entity);
	}

	@Override
	public String toString() {
		return "EntityWriteRequest [entity=" + entity + ", actor=" + actor + "]";
	}
}
