package eu.uneltelibere.scarpa.service.choose.api;

import java.util.Locale;

import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.filter.EmptyConditionGroup;

public class DependencyChooserParameters<T> {

	private ConditionGroup<T> conditions = new EmptyConditionGroup<>();
	private String listAttributeName;
	private String chooserUrl;
	private String pageToReturn;
	private String pageToChoose;
	private int pageNumber = 1;
	private Locale locale = Locale.ENGLISH;

	public ConditionGroup<T> getConditions() {
		return conditions;
	}

	public void setConditions(ConditionGroup<T> conditions) {
		this.conditions = conditions;
	}

	public String getListAttributeName() {
		return listAttributeName;
	}

	public void setListAttributeName(String listAttributeName) {
		this.listAttributeName = listAttributeName;
	}

	public String getChooserUrl() {
		return chooserUrl;
	}

	public void setChooserUrl(String chooserUrl) {
		this.chooserUrl = chooserUrl;
	}

	public String getPageToReturn() {
		return pageToReturn;
	}

	public void setPageToReturn(String pageToReturn) {
		this.pageToReturn = pageToReturn;
	}

	public String getPageToChoose() {
		return pageToChoose;
	}

	public void setPageToChoose(String pageToChoose) {
		this.pageToChoose = pageToChoose;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
