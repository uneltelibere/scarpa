package eu.uneltelibere.scarpa.model;

public class Setting extends Entity {

	private static final long serialVersionUID = 1L;

	private String key = "";

	private String value = "";

	public static class Builder {

		private Setting setting = new Setting();

		public Builder id(long id) {
			setting.setId(id);
			return this;
		}

		public Builder key(String key) {
			setting.setKey(key);
			return this;
		}

		public Builder value(String value) {
			setting.setValue(value);
			return this;
		}

		public Setting build() {
			return setting;
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
