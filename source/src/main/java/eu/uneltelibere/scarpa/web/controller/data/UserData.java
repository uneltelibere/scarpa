package eu.uneltelibere.scarpa.web.controller.data;

public class UserData extends Data {

	private String username = "";

	private String email = "";

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
