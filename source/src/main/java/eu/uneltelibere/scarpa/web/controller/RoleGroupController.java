package eu.uneltelibere.scarpa.web.controller;

import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.error;
import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.success;
import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.warn;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.service.RoleGroupService;
import eu.uneltelibere.scarpa.service.TranslationService;
import eu.uneltelibere.scarpa.web.controller.api.EntityDeleteHandler;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityFactory;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityWriter;
import eu.uneltelibere.scarpa.web.controller.data.group.WebRoleGroup;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;
import eu.uneltelibere.scarpa.web.menu.UsersMenu;

@Controller
@RequestMapping("/role-group")
public class RoleGroupController {

	private static final Logger LOG = LoggerFactory.getLogger(RoleGroupController.class);

	private static final String MESSAGE_GROUP_EDIT_NOT_FOUND = "page.role.group.edit.notfound";

	private static final String MESSAGE_GROUP_SAVE_FAILED = "message.save.role.group.failed";

	private static final String MESSAGE_SAVED = "generic.alert.saved";

	private static final String CONFIRMATION_GROUP_DELETE = "confirmation.role.group.delete";

	@Autowired
	private MenuHolder menu;

	@Autowired
	private RoleGroupService roleGroupService;

	@Autowired
	private WebEntityFactory<RoleGroup, WebRoleGroup> webRoleGroupFactory;

	@Autowired
	private WebEntityWriter<WebRoleGroup> webRoleGroupWriter;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private EntityDeleteHandler<RoleGroup> roleGroupDeleteHandler;

	@GetMapping
	public String listGroups(Model model, HttpServletRequest request) {
		LOG.info("Showing group list page");
		menu.set(UsersMenu.class, request, model);

		Locale locale = localeResolver.resolveLocale(request);
		List<RoleGroup> groups = roleGroupService.retrieve(locale);
		List<WebRoleGroup> webRoleGroups = webRoleGroupFactory.webEntities(groups);
		webRoleGroups = webRoleGroupWriter.write(webRoleGroups, request);
		model.addAttribute("groups", webRoleGroups);
		model.addAttribute("roleGroupDeleteConfirmation",
				localizedMessageService.getMessageTemplate(CONFIRMATION_GROUP_DELETE, request));

		return "roleGroupList";
	}

	@GetMapping(value = { "/new", "/{id}" })
	public String editGroupPage(@PathVariable("id") Optional<Long> groupId, Model model, HttpServletRequest request) {
		Long id = groupId.orElse(RoleGroup.NEW_ID);
		LOG.info("Showing details page for group {}", id);
		menu.set(UsersMenu.class, request, model);
		Locale locale = localeResolver.resolveLocale(request);
		Optional<RoleGroup> maybeGroup = roleGroupService.retrieve(id, locale);
		if (!maybeGroup.isPresent() && id != RoleGroup.NEW_ID) {
			warn(model, localizedMessageService.getMessage(MESSAGE_GROUP_EDIT_NOT_FOUND, request));
			model.addAttribute("mayAddGroup", false);
		} else {
			RoleGroup group = maybeGroup.orElse(roleGroupService.create(locale));
			WebRoleGroup webRoleGroup = webRoleGroupFactory.webEntity(group);
			webRoleGroup = webRoleGroupWriter.write(webRoleGroup, request);
			model.addAttribute("group", webRoleGroup);
		}
		return "roleGroupEdit";
	}

	@PostMapping(value = "/{id}")
	public String postGroup(@PathVariable("id") long id,
			@RequestParam(name = "groupCode", defaultValue = "") String code,
			@RequestParam(name = "groupName", defaultValue = "") String name,
			@RequestParam(name = "roles[]", defaultValue = "") List<Long> roleIDs, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		LOG.info("Received data for group {}", id);

		Locale locale = localeResolver.resolveLocale(request);
		Optional<RoleGroup> maybeGroup = roleGroupService.retrieve(id, locale);
		if (!maybeGroup.isPresent() && id != RoleGroup.NEW_ID) {
			throw new IllegalStateException("Group not found.");
		}
		RoleGroup group = maybeGroup.orElse(roleGroupService.create(locale));
		group.setId(id);
		group.setCode(code);

		Translation nameTranslation = group.getName();
		nameTranslation = translationService.retrieve(nameTranslation.getId()).orElse(nameTranslation);
		translationService.set(nameTranslation, name, locale);
		group.setName(nameTranslation);

		try {
			roleGroupService.save(group);
			roleGroupService.setRolesForGroup(group, roleIDs);
			success(redirectAttributes, localizedMessageService.getMessage(MESSAGE_SAVED, request));
		} catch (Exception e) {
			LOG.warn("Failed to save role group.", e);
			error(redirectAttributes, localizedMessageService.getMessage(MESSAGE_GROUP_SAVE_FAILED,
					new Object[] { e.getLocalizedMessage() }, request));
		}

		return "redirect:/role-group/" + group.getId();
	}

	@PostMapping(value = { "/delete" })
	public String delete(@RequestParam("id") long id,
			@RequestParam(value = "filter", defaultValue = "") List<String> filter,
			@RequestParam(name = "page", defaultValue = "1") int page, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		Supplier<Optional<RoleGroup>> entitySupplier = () -> {
			Locale locale = localeResolver.resolveLocale(request);
			return roleGroupService.retrieve(id, locale);
		};
		return roleGroupDeleteHandler.delete(id, entitySupplier, filter, page, request, redirectAttributes);
	}
}
