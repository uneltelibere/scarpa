package eu.uneltelibere.scarpa.service;

import org.junit.Test;

import eu.uneltelibere.scarpa.model.PageNavigator;
import eu.uneltelibere.scarpa.service.PageNavigationBuilder;

import org.junit.Assert;

public class PageNavigationBuilderTest {

	private static final String ELLIPSIS = "\u2026";

	@Test
	public void testBuildWithTwoPages() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(2).build();

		Assert.assertEquals(2, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
	}

	@Test
	public void testBuildWithSinglePageInsteadOfLeadingEllipsis() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(4).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertTrue(nav.getButtons().get(0).isEnabled());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildWithSinglePageInsteadOfTrailingEllipsis() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildCurrent93Count203() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(93).pageCount(203).build();

		Assert.assertEquals("Navigator was " + nav, 13, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(ELLIPSIS, nav.getButtons().get(3).getLabel());
		Assert.assertEquals(91, nav.getButtons().get(4).getTargetPage());
		Assert.assertEquals(92, nav.getButtons().get(5).getTargetPage());
		Assert.assertEquals(93, nav.getButtons().get(6).getTargetPage());
		Assert.assertEquals(94, nav.getButtons().get(7).getTargetPage());
		Assert.assertEquals(95, nav.getButtons().get(8).getTargetPage());
		Assert.assertEquals(ELLIPSIS, nav.getButtons().get(9).getLabel());
		Assert.assertEquals(201, nav.getButtons().get(10).getTargetPage());
		Assert.assertEquals(202, nav.getButtons().get(11).getTargetPage());
		Assert.assertEquals(203, nav.getButtons().get(12).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count1() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(1).build();

		Assert.assertEquals(1, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count2() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(2).build();

		Assert.assertEquals(2, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
	}

	@Test
	public void testBuildCurrent2Count2() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(2).pageCount(2).build();

		Assert.assertEquals(2, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count3() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(3).build();

		Assert.assertEquals(3, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
	}

	@Test
	public void testBuildCurrent2Count3() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(2).pageCount(3).build();

		Assert.assertEquals(3, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
	}

	@Test
	public void testBuildCurrent3Count3() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(3).pageCount(3).build();

		Assert.assertEquals(3, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count4() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildCurrent2Count4() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(2).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildCurrent3Count4() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(3).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildCurrent4Count4() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(4).pageCount(4).build();

		Assert.assertEquals(4, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count5() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(5).build();

		Assert.assertEquals(5, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(ELLIPSIS, nav.getButtons().get(3).getLabel());
		Assert.assertEquals(5, nav.getButtons().get(4).getTargetPage());
	}

	@Test
	public void testBuildCurrent2Count5() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(2).pageCount(5).build();

		Assert.assertEquals(5, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
		Assert.assertEquals(5, nav.getButtons().get(4).getTargetPage());
	}

	@Test
	public void testBuildCurrent3Count5() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(3).pageCount(5).build();

		Assert.assertEquals(5, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
		Assert.assertEquals(5, nav.getButtons().get(4).getTargetPage());
	}

	@Test
	public void testBuildCurrent4Count5() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(4).pageCount(5).build();

		Assert.assertEquals(5, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
		Assert.assertEquals(5, nav.getButtons().get(4).getTargetPage());
	}

	@Test
	public void testBuildCurrent5Count5() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(5).pageCount(5).build();

		Assert.assertEquals(5, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
		Assert.assertEquals(5, nav.getButtons().get(4).getTargetPage());
	}

	@Test
	public void testBuildCurrent1Count8() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(8).build();

		Assert.assertEquals(7, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(ELLIPSIS, nav.getButtons().get(3).getLabel());
		Assert.assertEquals(6, nav.getButtons().get(4).getTargetPage());
		Assert.assertEquals(7, nav.getButtons().get(5).getTargetPage());
		Assert.assertEquals(8, nav.getButtons().get(6).getTargetPage());
	}

	@Test
	public void testBuildCurrent2Count8() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(2).pageCount(8).build();

		Assert.assertEquals(8, nav.getButtons().size());
		Assert.assertEquals(1, nav.getButtons().get(0).getTargetPage());
		Assert.assertEquals(2, nav.getButtons().get(1).getTargetPage());
		Assert.assertEquals(3, nav.getButtons().get(2).getTargetPage());
		Assert.assertEquals(4, nav.getButtons().get(3).getTargetPage());
		// FIXME 5 would be better than ellipsis
		Assert.assertEquals(ELLIPSIS, nav.getButtons().get(4).getLabel());
		Assert.assertEquals(6, nav.getButtons().get(5).getTargetPage());
		Assert.assertEquals(7, nav.getButtons().get(6).getTargetPage());
		Assert.assertEquals(8, nav.getButtons().get(7).getTargetPage());
	}

	@Test
	public void testBuildWithManyPagesCanGoToLast() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(1).pageCount(8).build();
		Assert.assertTrue(nav.getLast().isEnabled());
	}

	@Test
	public void testBuildWithManyPagesCanGoToFirst() {
		PageNavigator nav = new PageNavigationBuilder().currentPage(8).pageCount(8).build();
		Assert.assertTrue(nav.getFirst().isEnabled());
	}
}
