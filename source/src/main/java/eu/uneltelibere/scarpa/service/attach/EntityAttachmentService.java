package eu.uneltelibere.scarpa.service.attach;

import java.util.List;

import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.model.Entity;

public interface EntityAttachmentService<T extends Entity> {

	void setAttachments(final T entity, List<Long> attachmentIDs);

	void addAttachment(final Attachment attachment, T entity);

	void removeAttachment(long idOfAttachmentToRemove, T entity);
}
