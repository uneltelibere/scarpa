package eu.uneltelibere.scarpa.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import eu.uneltelibere.scarpa.dao.util.EntityIdDecoder;
import eu.uneltelibere.scarpa.dao.util.IdDecoder;
import eu.uneltelibere.scarpa.model.User;

public class EntityIdDecoderTest {

	private IdDecoder decoder = new EntityIdDecoder();

	@Test
	public void testIdFromEntity() {
		User u1 = new User.Builder().id(1).build();

		Long id = decoder.id(u1, User.class);

		assertEquals(1, id.longValue());
	}

	@Test
	public void testIdFromLong() {
		Long id = decoder.id(1L, User.class);

		assertEquals(1, id.longValue());
	}

	@Test
	public void testIdFromInt() {
		Long id = decoder.id(1, User.class);

		assertEquals(1, id.longValue());
	}

	@Test
	public void testListOfIdsFromListOfEntities() {
		User u1 = new User.Builder().id(1).build();
		User u2 = new User.Builder().id(2).build();
		Object value = Arrays.asList(u1, u2);

		List<Long> ids = decoder.listOfIds(value);

		List<Long> expectedIds = Arrays.asList(1L, 2L);
		assertThat(ids, is(expectedIds));
	}

	@Test
	public void testListOfIdsFromLongs() {
		Object value = Arrays.asList(1L, 2L);

		List<Long> ids = decoder.listOfIds(value);

		assertThat(ids, is(value));
	}

	@Test
	public void testListOfIdsFromSingleEntity() {
		User u1 = new User.Builder().id(1).build();

		List<Long> ids = decoder.listOfIds(u1);

		assertThat(ids, is(Collections.singletonList(1L)));
	}

	@Test
	public void testListOfIdsFromSingleLong() {
		List<Long> ids = decoder.listOfIds(1L);

		assertThat(ids, is(Collections.singletonList(1L)));
	}

	@Test
	public void testListOfIdsFromSingleInteger() {
		List<Long> ids = decoder.listOfIds(1);

		assertThat(ids, is(Collections.singletonList(1L)));
	}
}
