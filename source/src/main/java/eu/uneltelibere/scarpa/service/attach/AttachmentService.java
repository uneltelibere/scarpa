package eu.uneltelibere.scarpa.service.attach;

import java.io.File;
import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Attachment;

public interface AttachmentService {

	Attachment save(Attachment entity);

	Optional<Attachment> getById(long id);

	List<Attachment> retrieve(List<Long> ids);

	void delete(Attachment entity);

	File file(Attachment file);
}
