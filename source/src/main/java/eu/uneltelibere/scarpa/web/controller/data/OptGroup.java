package eu.uneltelibere.scarpa.web.controller.data;

import java.util.LinkedList;
import java.util.List;

public class OptGroup<T> {

	private String label = "";

	private List<Option<T>> options = new LinkedList<>();

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Option<T>> getOptions() {
		return options;
	}

	public void setOptions(List<Option<T>> options) {
		this.options = options;
	}
}
