package eu.uneltelibere.scarpa.spring;

import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import eu.uneltelibere.scarpa.RuntimeContext;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private HandlerInterceptor modelEnricher;

	@Autowired
	private HandlerInterceptor loggingInterceptor;

	@Autowired
	private RuntimeContext runtimeContext;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(modelEnricher);
		registry.addInterceptor(loggingInterceptor);
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("512MB");
		factory.setMaxRequestSize("512MB");
		return factory.createMultipartConfig();
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		String locale = runtimeContext.getDefaultLocale();
		slr.setDefaultLocale(Locale.forLanguageTag(locale));
		return slr;
	}

	@Bean
	public Filter logFilter() {
		PostRequestLoggingFilter filter = new PostRequestLoggingFilter();
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(5120);
		return filter;
	}
}
