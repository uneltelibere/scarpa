package eu.uneltelibere.scarpa.web.controller.data;

import java.util.HashMap;
import java.util.Map;

public class Option<T> {

	private T value;

	private String label = "";

	private Map<String, Object> data = new HashMap<>();

	private boolean selected;

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
