package eu.uneltelibere.scarpa.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.api.FilteredPagedRetrievingService;
import eu.uneltelibere.scarpa.dao.UserCondition;
import eu.uneltelibere.scarpa.dao.UserCondition.UserField;
import eu.uneltelibere.scarpa.dao.UserCondition.UserOperator;
import eu.uneltelibere.scarpa.filter.Condition;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.filter.SingleConditionGroup;
import eu.uneltelibere.scarpa.model.User;

@Service
class ScarpaUserDuplicateAttributeDetectionService implements UserDuplicateAttributeDetectionService {

	@Autowired
	private FilteredPagedRetrievingService<User> userRetrievingService;

	@Override
	public boolean isUsernameUnique(User user) {
		Condition<User> nameCondition = new UserCondition(UserField.USERNAME, UserOperator.EQUALS, user.getUsername());
		return isAttributeUnique(user, nameCondition);
	}

	@Override
	public boolean isEmailUnique(User user) {
		Condition<User> mailCondition = new UserCondition(UserField.EMAIL, UserOperator.EQUALS, user.getEmail());
		return isAttributeUnique(user, mailCondition);
	}

	private boolean isAttributeUnique(User user, Condition<User> sameAttributeCondition) {
		ConditionGroup<User> conditions = new SingleConditionGroup<>(sameAttributeCondition);
		int usersWithTheSameAttributeCount = userRetrievingService.count(conditions);
		if (usersWithTheSameAttributeCount > 0) {
			List<User> maybeDuplicates = userRetrievingService.retrieveFiltered(conditions, 0,
					usersWithTheSameAttributeCount);
			for (User maybeDuplicate : maybeDuplicates) {
				if (maybeDuplicate.getId() != user.getId()) {
					return false;
				}
			}
		}
		return true;
	}
}
