package eu.uneltelibere.scarpa.dao.util;

import static java.util.stream.Collectors.joining;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.filter.Condition;
import eu.uneltelibere.scarpa.filter.ScarpaOperator;
import eu.uneltelibere.scarpa.model.Entity;

@Component
public class EntityConditionSerializer implements ConditionSerializer {

	@Autowired
	private IdDecoder entityIdDecoder;

	@Override
	public <T extends Entity> String serializeValue(Condition<T> condition) {
		if (Long.class.equals(condition.getField().getType())) {
			return serializeLongValue(condition);
		}
		return condition.getValue().toString();
	}

	private <T extends Entity> String serializeLongValue(Condition<T> condition) {
		Object idOrIds;
		if (ScarpaOperator.EQUALS.equals(condition.getOperator())) {
			idOrIds = entityIdDecoder.id(condition.getValue(), Entity.class);
		} else if (ScarpaOperator.IN.equals(condition.getOperator())) {
			idOrIds = entityIdDecoder.listOfIds(condition.getValue()).stream().map(id -> String.valueOf(id))
					.collect(joining("~"));
		} else {
			throw new IllegalArgumentException("Unknown operator " + condition.getOperator());
		}
		return idOrIds.toString();
	}
}
