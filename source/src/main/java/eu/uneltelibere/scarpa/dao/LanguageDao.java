package eu.uneltelibere.scarpa.dao;

import java.util.Optional;

import eu.uneltelibere.scarpa.model.Language;

public interface LanguageDao {

	Optional<Language> getByCode(String code);
}
