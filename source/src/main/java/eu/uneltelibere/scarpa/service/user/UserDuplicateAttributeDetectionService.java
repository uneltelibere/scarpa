package eu.uneltelibere.scarpa.service.user;

import eu.uneltelibere.scarpa.model.User;

interface UserDuplicateAttributeDetectionService {

	boolean isUsernameUnique(User user);

	boolean isEmailUnique(User user);
}
