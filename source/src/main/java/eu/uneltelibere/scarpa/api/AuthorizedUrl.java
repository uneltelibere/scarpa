package eu.uneltelibere.scarpa.api;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;

public final class AuthorizedUrl {

	private final String urlPattern;
	private final List<Role> roles;
	private final HttpMethod httpMethod;

	public AuthorizedUrl(String urlPattern, Role... roles) {
		this(null, urlPattern, roles);
	}

	public AuthorizedUrl(HttpMethod httpMethod, String urlPattern, Role... roles) {
		this.httpMethod = httpMethod;
		this.urlPattern = urlPattern;
		this.roles = Arrays.asList(roles);
	}

	public String getUrlPattern() {
		return urlPattern;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public boolean hasHttpMethod() {
		return httpMethod != null;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}
}
