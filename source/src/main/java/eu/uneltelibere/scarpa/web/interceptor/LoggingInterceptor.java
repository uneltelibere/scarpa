package eu.uneltelibere.scarpa.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import eu.uneltelibere.scarpa.model.User;

@Component
public class LoggingInterceptor extends HandlerInterceptorAdapter {

	private static final String NOBODY = "nobody";

	@Autowired
	private CurrentUserService currentUserService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		MDC.put("sessionId", sessionId);
		MDC.put("user", currentUserName(request.getSession()));
		return true;
	}

	private String currentUserName(HttpSession session) {
		return currentUserService.getCurrentUser(session).map(User::getUsername).orElseGet(() -> NOBODY);
	}
}
