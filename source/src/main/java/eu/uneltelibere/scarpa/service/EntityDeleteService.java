package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.model.Entity;

public interface EntityDeleteService<T extends Entity> {

	void delete(long id);
}
