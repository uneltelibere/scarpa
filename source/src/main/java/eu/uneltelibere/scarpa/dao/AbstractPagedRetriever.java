package eu.uneltelibere.scarpa.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import eu.uneltelibere.scarpa.api.Page;
import eu.uneltelibere.scarpa.dao.sql.SqlCondition;
import eu.uneltelibere.scarpa.filter.Condition;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.filter.EmptyConditionGroup;
import eu.uneltelibere.scarpa.filter.SingleConditionGroup;

public abstract class AbstractPagedRetriever<T> implements FilteredPagedRetriever<T> {

	protected static final String SCARPA_CONDITION = "$SCARPA_CONDITION";

	@Override
	public List<T> retrieveFiltered(ConditionGroup<T> conditions, Page page) {
		return retrieveFilteredAndSorted(conditions, Collections.emptyList(), page);
	}

	@Override
	public List<T> retrieveSorted(Collection<SortCriteria<T>> sortCriteria, Page page) {
		return retrieveFilteredAndSorted(new EmptyConditionGroup<>(), sortCriteria, page);
	}

	protected int count(ConditionGroup<T> conditions, String queryTemplate) {
		SqlCondition condition = sqlCondition(conditions);

		String query = queryTemplate.replace(SCARPA_CONDITION, condition.query());

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValues(condition.params());

		return getJdbcTemplate().queryForObject(query, params, Integer.class);
	}

	protected List<T> retrieveFilteredAndSorted(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria,
			Page page, String queryTemplate, RowMapper<T> rowMapper) {
		SqlCondition condition = sqlCondition(conditions);

		String query = queryTemplate.replace(SCARPA_CONDITION, condition.query());

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("offset", page.getOffset());
		params.addValue("limit", page.getLimit());
		params.addValues(condition.params());

		return getJdbcTemplate().query(query, params, rowMapper);
	}

	protected abstract SqlCondition sqlCondition(ConditionGroup<T> conditions);

	public final SqlCondition sqlCondition(Condition<T> condition) {
		return sqlCondition(new SingleConditionGroup<T>(condition));
	}

	protected abstract NamedParameterJdbcTemplate getJdbcTemplate();
}
