package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcRoleGroupDao extends JdbcMasterDao implements RoleGroupDao {

	// @formatter:off
	private static final String NEW_GROUP = "INSERT INTO role_group ("
			+ "code, name_translation_id"
			+ ") VALUES ("
			+ ":code, :nameTranslationId)";

	private static final String SUBQUERY_GET_ROLES = "SELECT "
			+ "g.id AS group_id, g.code AS group_code, "
			+ "g.name_translation_id "
			+ "FROM role_group g ";

	private static final String GET_GROUPS = SUBQUERY_GET_ROLES
			+ "ORDER BY g.id";

	private static final String GET_GROUP = SUBQUERY_GET_ROLES
			+ "WHERE g.id = :id";

	private static final String GET_GROUP_BY_CODE = SUBQUERY_GET_ROLES
			+ "WHERE g.code = :code";

	private static final String GET_GROUPS_OF_USER = SUBQUERY_GET_ROLES
			+ "LEFT JOIN user_role_group ug ON ug.role_group_id = g.id "
			+ "WHERE ug.user_id = :userId";

	private static final String UPDATE_GROUP_CODE = "UPDATE role_group SET "
			+ "code = :code "
			+ "WHERE id = :id";

	private static final String DELETE = "DELETE FROM role_group WHERE id = :id";

	private static final String REMOVE_ROLES = "DELETE FROM role_group_role "
			+ "WHERE role_group_id = :groupId "
			+ "AND role_id IN (:roleIds)";

	private static final String ADD_ROLES = "INSERT INTO role_group_role ("
			+ "role_group_id, role_id"
			+ ") VALUES ("
			+ ":groupId, :roleId)";
	// @formatter:on

	private ResultSetExtractor<Optional<RoleGroup>> roleGroupResultSetExtractor = new ResultSetExtractor<Optional<RoleGroup>>() {

		@Override
		public Optional<RoleGroup> extractData(ResultSet rs) throws SQLException {
			Optional<RoleGroup> maybeRoleGroup = Optional.empty();
			if (rs.next()) {
				RoleGroup roleGroup = roleGroupFromResultSet(rs);
				maybeRoleGroup = Optional.of(roleGroup);
			}
			return maybeRoleGroup;
		}
	};

	@Override
	public long create(RoleGroup group) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("code", group.getCode());
		params.addValue("nameTranslationId", group.getName().dbRef());
		jdbcTemplate.update(NEW_GROUP, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public List<RoleGroup> retrieve() {
		return jdbcTemplate.query(GET_GROUPS, new RowMapper<RoleGroup>() {

			@Override
			public RoleGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
				return roleGroupFromResultSet(rs);
			}
		});
	}

	@Override
	public Optional<RoleGroup> retrieve(long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbcTemplate.query(GET_GROUP, params, roleGroupResultSetExtractor);
	}

	@Override
	public Optional<RoleGroup> retrieveByCode(String code) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("code", code);
		return jdbcTemplate.query(GET_GROUP_BY_CODE, params, roleGroupResultSetExtractor);
	}

	@Override
	public List<RoleGroup> getRoleGroupsOfUser(User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", user.getId());
		return jdbcTemplate.query(GET_GROUPS_OF_USER, params, new RowMapper<RoleGroup>() {

			@Override
			public RoleGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
				return roleGroupFromResultSet(rs);
			}
		});
	}

	private RoleGroup roleGroupFromResultSet(ResultSet rs) throws SQLException {
		Translation translation = new Translation();
		Integer translationId = rs.getObject("name_translation_id", Integer.class);
		if (translationId != null) {
			translation.setId(translationId);
		}

		RoleGroup roleGroup = new RoleGroup();
		roleGroup.setId(rs.getLong("group_id"));
		roleGroup.setCode(rs.getString("group_code"));
		roleGroup.setName(translation);
		return roleGroup;
	}

	@Override
	public void update(RoleGroup roleGroup) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", roleGroup.getId());
		params.addValue("code", roleGroup.getCode());
		jdbcTemplate.update(UPDATE_GROUP_CODE, params);
	}

	@Override
	public void delete(long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		jdbcTemplate.update(DELETE, params);
	}

	@Override
	public void removeRolesFromGroup(RoleGroup group, List<Long> roleIDsToRemove) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("groupId", group.getId());
		params.addValue("roleIds", roleIDsToRemove);
		jdbcTemplate.update(REMOVE_ROLES, params);
	}

	@Override
	public void addRolesToGroup(RoleGroup group, List<Long> roleIDsToAdd) {
		MapSqlParameterSource[] batchArgs = new MapSqlParameterSource[roleIDsToAdd.size()];
		for (int i = 0; i < roleIDsToAdd.size(); i++) {
			batchArgs[i] = new MapSqlParameterSource();
			batchArgs[i].addValue("groupId", group.getId());
			batchArgs[i].addValue("roleId", roleIDsToAdd.get(i));
		}
		jdbcTemplate.batchUpdate(ADD_ROLES, batchArgs);
	}
}
