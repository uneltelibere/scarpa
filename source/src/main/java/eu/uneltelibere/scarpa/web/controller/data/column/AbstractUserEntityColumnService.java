package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.web.interceptor.CurrentUserService;

public abstract class AbstractUserEntityColumnService implements UserEntityColumnService {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractUserEntityColumnService.class);

	@Autowired
	private CurrentUserService currentUserService;

	@Autowired
	private SettingService settingService;

	@Override
	public void save(String shownList, HttpServletRequest request) {
		User currentUser = currentUserService.getCurrentUserOrFail(request.getSession());
		LOG.info("User {} wants to show colummns {}.", currentUser, shownList);
		settingService.set(columnsListSettingKey(), shownList, currentUser);
	}

	protected abstract String columnsListSettingKey();

	@Override
	public String getHiddenList(HttpServletRequest request) {
		String shownColumns = getShownList(request);
		String[] allColumnCodes = allColumns().split(",");
		String[] shownColumnCodesArray = shownColumns.split(",");
		List<String> shownColumnCodes = Arrays.asList(shownColumnCodesArray);
		List<String> hiddenCodes = new LinkedList<>();
		for (String columnCode : allColumnCodes) {
			if (!shownColumnCodes.contains(columnCode)) {
				hiddenCodes.add(columnCode);
			}
		}
		return String.join(",", hiddenCodes);
	}

	protected abstract String allColumns();

	@Override
	public String getShownList(HttpServletRequest request) {
		User currentUser = currentUserService.getCurrentUserOrFail(request.getSession());
		return settingService.get(columnsListSettingKey(), currentUser).orElse(defaultColumns());
	}

	protected abstract String defaultColumns();
}
