package eu.uneltelibere.scarpa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.UserRole;
import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

public class UserServiceTest {

	@InjectMocks
	private DefaultUserService userService;

	@Mock
	private UserDao userDao;

	@Mock
	private RoleService roleService;

	@Mock
	private RoleGroupDao roleGroupDao;

	@Mock
	private AuthorizedUserService authorizedUserService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCount() {
		when(userDao.count()).thenReturn(123);

		int count = userService.count();

		assertEquals(123, count);
		verify(userDao).count();
		verifyNoMoreInteractions(userDao);
	}

	@Test
	public void testGetById() {
		when(roleService.retrieve(Locale.ENGLISH)).thenReturn(roles(1, 2, 3));
		when(userDao.getUser(42)).thenReturn(Optional.of(userWithRoles(1)));

		User user = userService.getById(42).get();

		assertEquals(3, user.getRoles().size());
		assertTrue(user.getRoles().get(0).isAssigned());
		assertFalse(user.getRoles().get(1).isAssigned());
		assertFalse(user.getRoles().get(2).isAssigned());
	}

	@Test
	public void testGetUserByUsername() {
		User userInDb = userWithRoles(1);
		when(roleService.retrieve(Locale.ENGLISH)).thenReturn(roles(1, 2, 3));
		when(userDao.getUser("john")).thenReturn(Optional.of(userInDb));
		when(authorizedUserService.userWithRoles(userInDb)).thenReturn(userInDb);

		Optional<User> maybeUser = userService.getUser("john", Locale.ENGLISH);

		User user = maybeUser.get();
		assertEquals(3, user.getRoles().size());
		assertTrue(user.getRoles().get(0).isAssigned());
		assertFalse(user.getRoles().get(1).isAssigned());
		assertFalse(user.getRoles().get(2).isAssigned());
	}

	@Test
	public void testGetUserByEmail() {
		User userInDb = userWithRoles(1);
		when(userDao.getUserByEmail("john@example.com")).thenReturn(Optional.of(userInDb));

		Optional<User> maybeUser = userService.getUserByEmail("john@example.com");

		User user = maybeUser.get();
		assertEquals(userInDb, user);
	}

	@Test
	public void testGetUserDetails() {
		User userInDb = userWithRoles(1);
		when(authorizedUserService.getUserDetails("john")).thenReturn(Optional.of(userInDb));

		Optional<User> maybeUser = userService.getUserDetails("john");

		User user = maybeUser.get();
		assertEquals(userInDb, user);
	}

	private User userWithRoles(long... roleIDs) {
		User user = new User();
		user.setRoles(roles(roleIDs));
		return user;
	}

	private List<Role> roles(long... roleIDs) {
		List<Role> roles = new ArrayList<>(roleIDs.length);
		for (long roleId : roleIDs) {
			roles.add(role(roleId, "role " + roleId));
		}
		return roles;
	}

	private Role role(long id, String name) {
		Role role = new Role();
		role.setId(id);
		role.setName(name);
		return role;
	}

	@Test
	public void testSetRoleGroupsForUserUnchanged() {
		User inputUser = new User();
		inputUser.setId(42);
		when(roleGroupDao.getRoleGroupsOfUser(inputUser)).thenReturn(roleGroups(1, 2));

		userService.setRoleGroupsForUser(inputUser, Arrays.asList(1L, 2L));

		verify(userDao, never()).removeGroupsFromUser(any(), any());
		verify(userDao, never()).addGroupsToUser(any(), any());
	}

	@Test
	public void testSetRoleGroupsForUserRemoveOneKeepOneAddOne() {
		User inputUser = new User();
		inputUser.setId(42);
		when(roleGroupDao.getRoleGroupsOfUser(inputUser)).thenReturn(roleGroups(1, 2));

		userService.setRoleGroupsForUser(inputUser, Arrays.asList(2L, 3L));

		verify(userDao).removeGroupsFromUser(inputUser, Arrays.asList(1L));
		verify(userDao).addGroupsToUser(inputUser, Arrays.asList(3L));
	}

	private List<RoleGroup> roleGroups(long... roleIDs) {
		List<RoleGroup> roles = new ArrayList<>(roleIDs.length);
		for (long roleId : roleIDs) {
			roles.add(roleGroup(roleId, "role_group_" + roleId));
		}
		return roles;
	}

	private RoleGroup roleGroup(long id, String code) {
		RoleGroup role = new RoleGroup();
		role.setId(id);
		role.setCode(code);
		return role;
	}

	@Test
	public void testDelete() {
		userService.delete(123);

		verify(userDao).delete(123);
		verifyNoMoreInteractions(userDao);
	}

	@Test
	public void testSaveWithPassword() {
		User user = new User.Builder().id(1).password("secret").build();
		userService.save(user);

		verify(userDao).update(user);
		verifyNoMoreInteractions(userDao);
	}

	@Test
	public void testSaveWithoutPassword() {
		User user = new User.Builder().id(1).build();
		userService.save(user);

		verify(userDao).updateWithoutPassword(user);
		verifyNoMoreInteractions(userDao);
	}

	@Test
	public void testSetPassword() {
		User user = new User.Builder().build();
		userService.setPassword(user, "secret");

		assertEquals("secret", user.getPassword());
		verify(userDao).setPassword(user);
		verifyNoMoreInteractions(userDao);
	}

	@Test
	public void testUserHasRole() {
		eu.uneltelibere.scarpa.api.Role userRole = new UserRole();
		User user = new User.Builder().build();
		Role role = new Role.Builder().id(1).code("ROLE_" + userRole.name()).build();
		user.setRoles(Collections.singletonList(role));

		assertTrue(userService.userHasRole(user, userRole));
	}

	@Test
	public void testUserHasRoleNotFound() {
		eu.uneltelibere.scarpa.api.Role userRole = new UserRole();
		User user = new User.Builder().build();
		Role role = new Role.Builder().id(1).code("ROLE_TEST").build();
		user.setRoles(Collections.singletonList(role));

		assertFalse(userService.userHasRole(user, userRole));
	}
}
