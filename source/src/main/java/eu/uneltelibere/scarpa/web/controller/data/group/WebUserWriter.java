package eu.uneltelibere.scarpa.web.controller.data.group;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.service.RoleGroupService;
import eu.uneltelibere.scarpa.web.controller.data.AbstractWebEntityWriter;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityFactory;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityWriter;

@Service
public class WebUserWriter extends AbstractWebEntityWriter<WebUser> {

	@Autowired
	private RoleGroupService roleGroupService;

	@Autowired
	private WebEntityFactory<RoleGroup, WebRoleGroup> webRoleGroupFactory;

	@Autowired
	private WebEntityWriter<WebRoleGroup> webRoleWriter;

	@Override
	public WebUser write(WebUser webUser, HttpServletRequest request) {
		webUser = setExistingRoleGroups(webUser, request);
		webUser = setGroupList(webUser, request);
		webUser = markActiveRoleGroups(webUser, request);
		return webUser;
	}

	private WebUser setExistingRoleGroups(WebUser webUser, HttpServletRequest request) {
		List<RoleGroup> groups = roleGroupService.getRoleGroupsOfUser(webUser);
		List<WebRoleGroup> webGroups = webRoleGroupFactory.webEntities(groups);
		webGroups = webRoleWriter.write(webGroups, request);
		webUser.setWebRoleGroups(webGroups);
		return webUser;
	}

	private WebUser markActiveRoleGroups(WebUser webUser, HttpServletRequest request) {
		List<RoleGroup> allRoles = roleGroupService.retrieve(Locale.ENGLISH);
		List<WebRoleGroup> allWebRoles = webRoleGroupFactory.webEntities(allRoles);
		allWebRoles = webRoleWriter.write(allWebRoles, request);
		for (WebRoleGroup generalRole : allWebRoles) {
			generalRole.setAssigned(false);
			for (WebRoleGroup userRole : webUser.getWebRoleGroups()) {
				if (generalRole.equals(userRole)) {
					generalRole.setAssigned(true);
					break;
				}
			}
		}
		webUser.setWebRoleGroups(allWebRoles);
		return webUser;
	}

	private WebUser setGroupList(WebUser webUser, HttpServletRequest request) {
		String roleList;
		if (!webUser.getWebRoleGroups().isEmpty()) {
			roleList = webUser.getWebRoleGroups().stream().map(role -> role.getDisplayName())
					.collect(Collectors.joining(", "));
		} else {
			roleList = "-";
		}
		webUser.setGroupList(roleList);
		return webUser;
	}
}
