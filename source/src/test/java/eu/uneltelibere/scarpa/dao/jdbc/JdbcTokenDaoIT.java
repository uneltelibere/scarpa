package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.TokenDao;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcTokenDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private TokenDao tokenDao;

	@Test
	@ExpectedDatabase(value = "/db/out/token/created_token.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreate() {
		User user = new User();
		user.setId(1);

		Token token = new Token();
		token.setUser(user);
		token.setToken("foo");
		token.setExpiryDate(LocalDateTime.of(2018, 3, 19, 12, 34));

		long id = tokenDao.create(token);
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("/db/in/token/token_foo.xml")
	public void testGetById() {
		Optional<Token> tokenInDb = tokenDao.read("foo");
		assertEquals(1, tokenInDb.get().getId());
		assertEquals(1, tokenInDb.get().getUser().getId());
	}

	@Test
	public void testGetByIdNonexistent() {
		Optional<Token> tokenInDb = tokenDao.read("bar");
		assertFalse(tokenInDb.isPresent());
	}

	@Test
	@DatabaseSetup(value = { "/db/in/token/token_foo.xml", "/db/in/token/token_bar.xml" })
	@ExpectedDatabase(value = "/db/in/token/token_bar.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDelete() {
		Token token = new Token();
		token.setId(1);
		tokenDao.deleteToken(token);
	}

	@Test
	@DatabaseSetup("/db/in/token/token_foo.xml")
	@ExpectedDatabase(value = "/db/in/token/token_foo.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteNonexistent() {
		Token token = new Token();
		token.setId(-1);
		tokenDao.deleteToken(token);
	}

	@Test
	@DatabaseSetup(value = { "/db/in/token/token_foo.xml", "/db/in/token/token_bar.xml" })
	@ExpectedDatabase(value = "/db/in/token/token_foo.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteForUser() {
		tokenDao.deleteTokensForUser(2);
	}

	@Test
	@DatabaseSetup("/db/in/token/token_foo.xml")
	@ExpectedDatabase(value = "/db/in/token/token_foo.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteForUserNonexistent() {
		tokenDao.deleteTokensForUser(-1);
	}
}
