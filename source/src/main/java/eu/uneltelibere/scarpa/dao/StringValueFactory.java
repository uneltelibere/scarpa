package eu.uneltelibere.scarpa.dao;

public class StringValueFactory implements ValueFactory {

	@Override
	public Object createValue(String valueAsString) {
		return valueAsString;
	}
}
