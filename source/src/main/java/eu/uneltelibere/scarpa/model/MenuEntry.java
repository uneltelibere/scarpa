package eu.uneltelibere.scarpa.model;

import eu.uneltelibere.scarpa.web.menu.Menu;

public class MenuEntry {

	private final String label;
	private final String link;
	private boolean active;
	private int badge;

	public MenuEntry(Menu menu) {
		this.label = menu.getLabel();
		this.link = menu.getLink();
	}

	public String getLabel() {
		return label;
	}

	public String getLink() {
		return link;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive() {
		active = true;
	}

	public int getBadge() {
		return badge;
	}

	public void setBadge(int badgeValue) {
		this.badge = badgeValue;
	}
}
