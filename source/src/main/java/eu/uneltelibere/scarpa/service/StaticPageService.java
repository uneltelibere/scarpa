package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.StaticPage;

public interface StaticPageService {

	Optional<StaticPage.Version> getLatestVersionOfPage(long pageId);

	Optional<StaticPage.Version> getVersionOfPage(long pageId, int versionNumber);

	List<StaticPage.Version> getVersionsOfPage(long pageId);

	void save(StaticPage.Version pageVersion);

	int currentVersionNumber(StaticPage staticPage);
}
