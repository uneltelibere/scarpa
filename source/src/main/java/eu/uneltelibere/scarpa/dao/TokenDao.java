package eu.uneltelibere.scarpa.dao;

import java.util.Optional;

import eu.uneltelibere.scarpa.model.Token;

public interface TokenDao {

	long create(Token token);

	Optional<Token> read(String token);

	void deleteToken(Token token);

	void deleteTokensForUser(long userId);
}
