package eu.uneltelibere.scarpa.service.poi;

@SuppressWarnings("serial")
public class CellException extends RuntimeException {

	public CellException(String message) {
		super(message);
	}
}
