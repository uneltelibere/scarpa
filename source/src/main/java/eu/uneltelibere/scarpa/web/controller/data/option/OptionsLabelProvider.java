package eu.uneltelibere.scarpa.web.controller.data.option;

import javax.servlet.http.HttpServletRequest;

public interface OptionsLabelProvider {

	String labelAny();

	String labelFormat(String label, HttpServletRequest request);
}
