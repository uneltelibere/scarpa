package eu.uneltelibere.scarpa.model;

public class TranslationText extends Entity {

	private static final long serialVersionUID = 1L;

	private Translation translation = new Translation();

	private Language language = new Language();

	private String translatedText = "";

	public Translation getTranslation() {
		return translation;
	}

	public void setTranslation(Translation translation) {
		this.translation = translation;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getTranslatedText() {
		return translatedText;
	}

	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}
}
