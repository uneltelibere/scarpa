package eu.uneltelibere.scarpa.filter;

import java.util.LinkedList;

public class EmptyConditionGroup<T> extends ConditionGroup<T> {

	public EmptyConditionGroup() {
		super(new LinkedList<>());
	}
}
