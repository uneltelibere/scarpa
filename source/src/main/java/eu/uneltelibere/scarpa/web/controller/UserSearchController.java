package eu.uneltelibere.scarpa.web.controller;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.lister.PagedLocalizedLister;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityFactory;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityWriter;
import eu.uneltelibere.scarpa.web.controller.data.group.WebUser;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;
import eu.uneltelibere.scarpa.web.menu.UsersMenu;

@Controller
@RequestMapping("/users")
public class UserSearchController {

	private static final Logger LOG = LoggerFactory.getLogger(UserSearchController.class);

	private static final String CONFIRMATION_USER_DELETE = "confirmation.user.delete";

	@Autowired
	private MenuHolder menu;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private PagedLocalizedLister<User> userLister;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Autowired
	private WebEntityFactory<User, WebUser> webUserFactory;

	@Autowired
	private WebEntityWriter<WebUser> webUserWriter;

	@RequestMapping(value = { "", "/", "/page/{page}" }, method = RequestMethod.GET)
	public String showFilterPage(@PathVariable Optional<Integer> page,
			@RequestParam(value = "filter", defaultValue = "") List<String> filter, Model model,
			HttpServletRequest request) {
		int pageNumber = page.orElse(1);
		LOG.info("Showing user filter page for filter = [{}], page {}.", filter, pageNumber);

		menu.set(UsersMenu.class, request, model);

		if (!filter.isEmpty()) {
			model.addAttribute("listMode", "filter");
		}

		Locale locale = localeResolver.resolveLocale(request);
		List<User> users = userLister.getList(pageNumber, locale);
		List<WebUser> webUsers = webUserFactory.webEntities(users);
		webUsers = webUserWriter.write(webUsers, request);

		model.addAttribute("users", webUsers);
		userLister.getNavigator("/users", pageNumber).ifPresent(nav -> model.addAttribute("pageNavigator", nav));
		model.addAttribute("filter", filter);
		model.addAttribute("doNotShowSearchBar", users.isEmpty() && filter.isEmpty());

		model.addAttribute("userDeleteConfirmation",
				localizedMessageService.getMessageTemplate(CONFIRMATION_USER_DELETE, request));
		model.addAttribute("pageNumber", pageNumber);

		return "userList";
	}
}
