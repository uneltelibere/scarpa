package eu.uneltelibere.scarpa.model;

import org.springframework.web.multipart.MultipartFile;

public class Attachment extends Entity {

	private static final long serialVersionUID = 1L;

	private String originalFilename = "";

	private long size;

	private String contentType = "";

	private MultipartFile incomingFile;

	public static class Builder {

		private final Attachment attachment = new Attachment();

		public Builder id(long id) {
			attachment.setId(id);
			return this;
		}

		public Builder originalFilename(String originalFilename) {
			attachment.setOriginalFilename(originalFilename);
			return this;
		}

		public Builder size(long size) {
			attachment.setSize(size);
			return this;
		}

		public Builder contentType(String contentType) {
			attachment.setContentType(contentType);
			return this;
		}

		public Attachment build() {
			return attachment;
		}
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getInternalFilename() {
		return String.format("%d_%s", getId(), getOriginalFilename());
	}

	public MultipartFile getIncomingFile() {
		return incomingFile;
	}

	public void setIncomingFile(MultipartFile incomingFile) {
		this.incomingFile = incomingFile;
	}
}
