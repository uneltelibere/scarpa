package eu.uneltelibere.scarpa.dao.sql;

public class OrSqlCondition extends SqlCondition {

	@Override
	protected String conjunction() {
		return " OR ";
	}
}
