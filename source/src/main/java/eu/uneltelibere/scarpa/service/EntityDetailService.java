package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.model.Entity;

public interface EntityDetailService<T extends Entity> {

	T entityWithDetails(T entity);
}
