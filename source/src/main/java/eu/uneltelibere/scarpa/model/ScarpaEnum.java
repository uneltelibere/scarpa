package eu.uneltelibere.scarpa.model;

public abstract class ScarpaEnum extends Entity {

	private static final long serialVersionUID = 1L;

	private String code = "";

	private String name = "";

	public static abstract class Builder<T extends ScarpaEnum, B extends Builder<T, B>> {

		private T object;

		public Builder() {
			object = createObject();
		}

		public B id(long id) {
			object.setId(id);
			return getThis();
		}

		public B code(String code) {
			object.setCode(code);
			return getThis();
		}

		public B name(String name) {
			object.setName(name);
			return getThis();
		}

		public T build() {
			return object;
		}

		protected abstract T createObject();

		protected T getObject() {
			return object;
		}

		protected abstract B getThis();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
