package eu.uneltelibere.scarpa.dao;

import java.util.List;

public interface PagedRetriever<T> {

	List<T> retrieve(int offset, int limit);

	int count();
}
