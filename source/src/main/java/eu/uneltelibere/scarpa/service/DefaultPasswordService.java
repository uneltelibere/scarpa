package eu.uneltelibere.scarpa.service;

import java.security.SecureRandom;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DefaultPasswordService implements PasswordService {

	public static final String VALIDATION_PASSWORD_INVALID = "validation.password.invalid";
	public static final String VALIDATION_PASSWORDS_NOT_MATCHING = "validation.password.again.not.matching";

	private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static final int DEFAULT_LENGTH = 12;
	private static final int MIN_PASSWORD_LENGTH = 4;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public String generatePassword() {
		return RandomStringUtils.random(DEFAULT_LENGTH, 0, CHARACTERS.length(), false, false, CHARACTERS.toCharArray(),
				new SecureRandom());
	}

	@Override
	public String encodePassword(String clearText) {
		return passwordEncoder.encode(clearText);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return passwordEncoder.matches(rawPassword, encodedPassword);
	}

	@Override
	public boolean isValidPassword(CharSequence clearText) {
		if (clearText == null) {
			return false;
		}
		if (clearText.length() < MIN_PASSWORD_LENGTH) {
			return false;
		}
		return true;
	}

	@Override
	public int minPasswordLength() {
		return MIN_PASSWORD_LENGTH;
	}

	@Override
	public void validatePassword(CharSequence password, CharSequence passwordAgain) {
		if (!isValidPassword(password)) {
			throw new ValidationException(VALIDATION_PASSWORD_INVALID);
		}
		if (!password.equals(passwordAgain)) {
			throw new ValidationException(VALIDATION_PASSWORDS_NOT_MATCHING);
		}
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
}
