package eu.uneltelibere.scarpa.spring;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.thymeleaf.TemplateEngine;

import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

import eu.uneltelibere.scarpa.RuntimeContext;
import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.PropertiesVersionService;
import eu.uneltelibere.scarpa.service.UserService;
import eu.uneltelibere.scarpa.service.lister.PagedLister;
import eu.uneltelibere.scarpa.service.lister.PagedLocalizedLister;

@Configuration
@ComponentScan(basePackageClasses = { UserDao.class, UserService.class }, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = PropertiesVersionService.class) })
public class TestConfig {

	@Value("${dbPort:5432}")
	private String dbPort;

	@Bean(name = "dbUnitDatabaseConnection")
	public DatabaseDataSourceConnectionFactoryBean databaseDataSourceConnectionFactoryBean()
			throws IOException, URISyntaxException {
		DatabaseDataSourceConnectionFactoryBean databaseConfigBean = new DatabaseDataSourceConnectionFactoryBean();
		databaseConfigBean.setDatabaseConfig(databaseConfigBean());
		databaseConfigBean.setDataSource(masterDataSource());
		return databaseConfigBean;
	}

	@Bean(name = "dbUnitDatabaseConfig")
	public DatabaseConfigBean databaseConfigBean() {
		DatabaseConfigBean databaseConfigBean = new DatabaseConfigBean();
		final IDataTypeFactory dataTypeFactory = new PostgresqlDataTypeFactory();
		databaseConfigBean.setDatatypeFactory(dataTypeFactory);
		return databaseConfigBean;
	}

	@Bean
	public DataSource masterDataSource() {
		String connectionUrl = "jdbc:postgresql://localhost:" + dbPort + "/scarpatest";
		connectionUrl += "?currentSchema=scarpa";
		connectionUrl += "&stringtype=unspecified"; // for PostgreSQL enums
		// @formatter:off
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName("org.postgresql.Driver")
				.url(connectionUrl)
				.username("scarpatest")
				.password("scarpatest")
			.build();
		// @formatter:on
		return dataSource;
	}

	@Bean(name = "scarpaTransactionManager")
	public PlatformTransactionManager scarpaTxManager() {
		return new DataSourceTransactionManager(masterDataSource());
	}

	@Bean
	public PagedLister<User> userLister() {
		return new PagedLister<User>() {
		};
	}

	@Bean
	public PagedLocalizedLister<User> localizedUserLister() {
		return new PagedLocalizedLister<User>() {
		};
	}

	@Bean(name = "htmlMailSender")
	public JavaMailSender mockJavaMailSender() {
		return Mockito.mock(JavaMailSender.class);
	}

	@Bean
	public RuntimeContext mockRuntimeContext() {
		return Mockito.mock(RuntimeContext.class);
	}

	@Bean
	public TemplateEngine mockTemplateEngine() {
		return Mockito.mock(TemplateEngine.class);
	}

	@Bean
	public ResourceBundleMessageSource mockResourceBundleMessageSource() {
		return Mockito.mock(ResourceBundleMessageSource.class);
	}

	@Bean
	public PasswordEncoder mockPasswordEncoder() {
		return Mockito.mock(PasswordEncoder.class);
	}
}
