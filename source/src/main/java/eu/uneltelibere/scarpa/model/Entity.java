package eu.uneltelibere.scarpa.model;

import java.io.Serializable;

public abstract class Entity implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final long NEW_ID = -1;

	private long id = NEW_ID;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long dbRef() {
		if (id == NEW_ID || id == 0) {
			return null;
		}
		return id;
	}

	public String getDisplayName() {
		return String.valueOf(id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [id=" + id + "]";
	}
}
