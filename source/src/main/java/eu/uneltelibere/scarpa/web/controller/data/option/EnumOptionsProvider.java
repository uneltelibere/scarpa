package eu.uneltelibere.scarpa.web.controller.data.option;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.model.ScarpaEnum;
import eu.uneltelibere.scarpa.web.controller.data.Select;

public interface EnumOptionsProvider<T extends ScarpaEnum> {

	Select<Long> enumOptions(HttpServletRequest request, long selectedEnumId);

	Select<Long> enumOptions(HttpServletRequest request, long selectedEnumId,
			OptionsLabelProvider optionsLabelProvider);
}
