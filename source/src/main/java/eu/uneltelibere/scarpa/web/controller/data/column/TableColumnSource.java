package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class TableColumnSource<T> {

	private List<? extends T> entities;
	private String preferredColumns;
	private HttpServletRequest request;
	private List<String> noSearchForCodes = new LinkedList<>();
	private boolean noSearchAtAll;

	public List<? extends T> getEntities() {
		return entities;
	}

	public TableColumnSource<T> entities(List<? extends T> entities) {
		this.entities = entities;
		return this;
	}

	public String getPreferredColumns() {
		return preferredColumns;
	}

	public TableColumnSource<T> preferredColumns(String preferredColumns) {
		this.preferredColumns = preferredColumns;
		return this;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public TableColumnSource<T> request(HttpServletRequest request) {
		this.request = request;
		return this;
	}

	public boolean isNoSearch(String columnCode) {
		return noSearchForCodes.contains(columnCode);
	}

	public TableColumnSource<T> noSearch(String columnCode) {
		this.noSearchForCodes.add(columnCode);
		return this;
	}

	public boolean isNoSearchAtAll() {
		return noSearchAtAll;
	}

	public TableColumnSource<T> noSearchAtAll() {
		this.noSearchAtAll = true;
		return this;
	}
}
