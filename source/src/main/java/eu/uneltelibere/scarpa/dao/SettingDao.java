package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;

public interface SettingDao {

	long create(Setting setting);

	long create(Setting setting, User user);

	Optional<Setting> retrieve(String key);

	List<Setting> retrieveFromGroup(String keyPrefix);

	Optional<Setting> retrieve(String key, User user);

	void update(Setting setting);

	void update(Setting setting, User user);
}
