package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.TokenDao;
import eu.uneltelibere.scarpa.model.Token;

@Repository
public class JdbcTokenDao extends JdbcMasterDao implements TokenDao {

	private static final String NEW_TOKEN = "INSERT INTO password_reset_tokens (user_id, token, expiry_date) VALUES (:userId, :token, :expiryDate)";

	private static final String GET_TOKEN = "SELECT t.id, t.user_id, t.token, t.expiry_date FROM password_reset_tokens t WHERE t.token = :token";

	private static final String DELETE_TOKEN = "DELETE FROM password_reset_tokens t WHERE t.id = :id";

	private static final String DELETE_TOKENS_FOR_USER = "DELETE FROM password_reset_tokens t WHERE t.user_id = :id";

	@Override
	public long create(Token token) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", token.getUser().getId());
		params.addValue("token", token.getToken());
		params.addValue("expiryDate", token.getExpiryDate());
		jdbcTemplate.update(NEW_TOKEN, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public Optional<Token> read(String token) {
		SqlParameterSource params = new MapSqlParameterSource("token", token);
		return jdbcTemplate.query(GET_TOKEN, params, new ResultSetExtractor<Optional<Token>>() {

			@Override
			public Optional<Token> extractData(ResultSet rs) throws SQLException, DataAccessException {
				Optional<Token> maybeToken = Optional.empty();
				if (rs.next()) {
					Token token = new Token();
					token.setId(rs.getInt("id"));
					token.setToken(rs.getString("token"));
					token.setExpiryDate(rs.getObject("expiry_date", LocalDateTime.class));
					token.getUser().setId(rs.getInt("user_id"));
					maybeToken = Optional.of(token);
				}
				return maybeToken;
			}
		});
	}

	@Override
	public void deleteToken(Token token) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", token.getId());
		jdbcTemplate.update(DELETE_TOKEN, params);
	}

	@Override
	public void deleteTokensForUser(long userId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", userId);
		jdbcTemplate.update(DELETE_TOKENS_FOR_USER, params);
	}
}
