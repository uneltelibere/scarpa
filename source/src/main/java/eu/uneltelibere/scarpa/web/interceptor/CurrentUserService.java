package eu.uneltelibere.scarpa.web.interceptor;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import eu.uneltelibere.scarpa.model.User;

public interface CurrentUserService {

	Optional<User> getCurrentUser(HttpSession session);

	User getCurrentUserOrFail(HttpSession session);

	User getCurrentUserOrFail(HttpServletRequest request);

	void setCurrentUser(HttpServletRequest request);
}
