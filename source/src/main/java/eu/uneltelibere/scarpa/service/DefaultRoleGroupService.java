package eu.uneltelibere.scarpa.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

@Service
public class DefaultRoleGroupService implements RoleGroupService, EntityDeleteService<RoleGroup> {

	@Autowired
	private RoleGroupDao roleGroupDao;

	@Autowired
	private RoleService roleService;

	@Autowired
	private TranslationService translationService;

	@Override
	public List<RoleGroup> retrieve(Locale locale) {
		List<RoleGroup> groups = roleGroupDao.retrieve();
		for (RoleGroup group : groups) {
			List<Role> roles = roleService.retrieve(group, locale);
			group.setRoles(roles);
		}
		return groups;
	}

	@Override
	public Optional<RoleGroup> retrieve(long id, Locale locale) {
		return roleGroupDao.retrieve(id).map(group -> roleGroupWithRoles(group, locale));
	}

	@Override
	public Optional<RoleGroup> retrieveByCode(String code, Locale locale) {
		return roleGroupDao.retrieveByCode(code).map(group -> roleGroupWithRoles(group, locale));
	}

	private RoleGroup roleGroupWithRoles(RoleGroup group, Locale locale) {
		List<Role> roles = roleService.retrieve(group, locale);
		group.setRoles(roles);
		putAllRoles(group, locale);
		return group;
	}

	@Override
	public RoleGroup create(Locale locale) {
		RoleGroup group = new RoleGroup();
		putAllRoles(group, locale);
		return group;
	}

	@Override
	public void save(RoleGroup roleGroup) {
		translationService.save(roleGroup.getName());

		if (roleGroup.getId() == RoleGroup.NEW_ID) {
			long id = roleGroupDao.create(roleGroup);
			roleGroup.setId(id);
		} else {
			roleGroupDao.update(roleGroup);
		}
	}

	private RoleGroup putAllRoles(RoleGroup group, Locale locale) {
		List<Role> allRoles = roleService.retrieve(locale);
		for (Role generalRole : allRoles) {
			generalRole.setAssigned(false);
			for (Role userRole : group.getRoles()) {
				if (generalRole.getId() == userRole.getId()) {
					generalRole.setAssigned(true);
					break;
				}
			}
		}
		group.setRoles(allRoles);
		return group;
	}

	@Override
	public void delete(long id) {
		roleGroupDao.delete(id);
	}

	@Override
	public void setRolesForGroup(final RoleGroup group, List<Long> roleIDs) {
		RoleGroup groupWithRoles = roleGroupDao.retrieve(group.getId())
				.orElseThrow(() -> new IllegalStateException("Role group not found"));
		List<Long> existingRoles = roleService.retrieve(group, Locale.ENGLISH).stream().map(Role::getId)
				.collect(toList());

		List<Long> rolesToRemove = existingRoles.stream().filter(role -> !roleIDs.contains(role)).collect(toList());
		if (!rolesToRemove.isEmpty()) {
			roleGroupDao.removeRolesFromGroup(groupWithRoles, rolesToRemove);
		}

		List<Long> rolesToAdd = roleIDs.stream().filter(role -> !existingRoles.contains(role)).collect(toList());
		if (!rolesToAdd.isEmpty()) {
			roleGroupDao.addRolesToGroup(groupWithRoles, rolesToAdd);
		}
	}

	@Override
	public List<RoleGroup> getRoleGroupsOfUser(User user) {
		return roleGroupDao.getRoleGroupsOfUser(user);
	}
}
