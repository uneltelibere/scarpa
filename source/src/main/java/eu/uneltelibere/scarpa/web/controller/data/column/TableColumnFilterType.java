package eu.uneltelibere.scarpa.web.controller.data.column;

public enum TableColumnFilterType {

	TEXT, ASSISTED_TEXT, SELECT, NULLABLE_DATE
}
