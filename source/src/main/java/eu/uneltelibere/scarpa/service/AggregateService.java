package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.stream.Stream;

import eu.uneltelibere.scarpa.model.Entity;

public interface AggregateService<T extends Entity, U extends Entity> {

	void setPartsForWhole(final T whole, Stream<U> parts, List<Long> partIDs);

	List<Long> readPartIds(final T whole);

	List<U> retrieveParts(List<Long> partIDs);
}
