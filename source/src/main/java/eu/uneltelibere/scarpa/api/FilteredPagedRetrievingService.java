package eu.uneltelibere.scarpa.api;

import java.util.Collection;
import java.util.List;

import eu.uneltelibere.scarpa.dao.SortCriteria;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.Entity;

public interface FilteredPagedRetrievingService<T extends Entity> {

	int count(ConditionGroup<T> conditions);

	List<T> retrieveFiltered(ConditionGroup<T> conditions, int offset, int limit);

	List<T> retrieveFilteredAndSorted(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria,
			int offset, int limit);
}
