package eu.uneltelibere.scarpa.web.menu;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.AnyRole;
import eu.uneltelibere.scarpa.api.OrderedMenu;
import eu.uneltelibere.scarpa.api.Role;
import eu.uneltelibere.scarpa.model.MenuEntry;

public abstract class AbstractMenuHolder implements MenuHolder {

	protected abstract OrderedMenu getOrderedMenu();

	@Override
	public void set(Class<? extends Menu> active, HttpServletRequest request, Model model) {
		set(active, "menu", request, model);
	}

	@Override
	public void setSubmenu(Class<? extends Menu> active, HttpServletRequest request, Model model) {
		set(active, "subMenu", request, model);
	}

	private void set(Class<? extends Menu> active, String modelKey, HttpServletRequest request, Model model) {
		List<MenuEntry> entries = new LinkedList<>();
		for (Menu menu : getOrderedMenu().menus()) {
			if (userHasAnyRole(menu.getRoles())) {
				MenuEntry entry = new MenuEntry(menu);
				if (active.equals(menu.getClass())) {
					entry.setActive();
				}
				entry.setBadge(menu.getBadge(request));
				entries.add(entry);
			}
		}
		model.addAttribute(modelKey, entries);
	}

	private boolean userHasAnyRole(List<Role> roles) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return false;
		}

		for (Role role : roles) {
			if (role instanceof AnyRole) {
				return true;
			}
		}

		for (GrantedAuthority auth : authentication.getAuthorities()) {
			for (Role role : roles) {
				String authority = "ROLE_" + role.name();
				if (authority.equals(auth.getAuthority())) {
					return true;
				}
			}
		}

		return false;
	}
}
