package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.web.controller.data.Input;
import eu.uneltelibere.scarpa.web.controller.data.Text;

public class TableColumnBuilder<T extends Entity> {

	private TableColumn<T> column = new TableColumn<>();

	private Function<T, String> valueText = anything -> "";

	private Function<T, String> valueTitle = anything -> "";

	private Function<T, List<String>> detailedValueTexts = anything -> Collections.emptyList();

	private List<? extends T> valueEntities = new LinkedList<>();

	private TableColumnFilterType filterType = TableColumnFilterType.TEXT;

	private Input filterInput = new Text();

	private boolean noSearch;

	public TableColumnBuilder<T> headingCode(String headingCode) {
		column.getHeading().setCode(headingCode);
		return this;
	}

	public TableColumnBuilder<T> heading(TableColumn.Heading heading) {
		column.setHeading(heading);
		return this;
	}

	public TableColumnBuilder<T> headingText(String headingText) {
		column.getHeading().setText(headingText);
		return this;
	}

	public TableColumnBuilder<T> headingAbbr(String headingAbbreviation) {
		column.getHeading().setAbbreviationTitle(headingAbbreviation);
		return this;
	}

	public TableColumnBuilder<T> valueText(Function<T, String> valueText) {
		this.valueText = valueText;
		return this;
	}

	public TableColumnBuilder<T> valueTitle(Function<T, String> valueTitle) {
		this.valueTitle = valueTitle;
		return this;
	}

	public TableColumnBuilder<T> detailedValueTexts(Function<T, List<String>> detailedValueTexts) {
		this.detailedValueTexts = detailedValueTexts;
		return this;
	}

	public TableColumnBuilder<T> valueEntities(List<? extends T> valueEntities) {
		this.valueEntities = valueEntities;
		return this;
	}

	public TableColumnBuilder<T> filterType(TableColumnFilterType filterType) {
		this.filterType = filterType;
		return this;
	}

	public TableColumnBuilder<T> noSearch() {
		this.noSearch = true;
		return this;
	}

	public TableColumnBuilder<T> noSearch(boolean noSearch) {
		this.noSearch = noSearch;
		return this;
	}

	public TableColumnBuilder<T> filterInput(Input filterInput) {
		this.filterInput = filterInput;
		return this;
	}

	public TableColumn<T> build() {
		for (T entity : valueEntities) {
			TableColumn.Value value = new TableColumn.Value();
			value.setText(valueText.apply(entity));
			value.setTitle(valueTitle.apply(entity));
			column.getValues().put(entity, value);

			List<TableColumn.Value> detailedValues = new LinkedList<>();
			List<String> detailedTexts = detailedValueTexts.apply(entity);
			for (int i = 0; i < detailedTexts.size(); i++) {
				TableColumn.Value detailedValue = new TableColumn.Value();
				detailedValue.setText(detailedTexts.get(i));
				detailedValues.add(detailedValue);
			}
			column.getDetailedValues().put(entity, detailedValues);
		}
		column.setFilterType(filterType);
		column.setFilterInput(filterInput);
		column.setNoSearch(noSearch);
		return column;
	}
}
