package eu.uneltelibere.scarpa.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class ScarpaAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ScarpaAuthenticationSuccessHandler.class);

	private static final Integer SECONDS_PER_HOUR = 60 * 60;
	private static final Integer SESSION_TIMEOUT_IN_SECONDS = 12 * SECONDS_PER_HOUR;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		LOG.info("User {} logged in.", authentication.getName());
		LOG.info("User-Agent: {}", request.getHeader("User-Agent"));

		request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_IN_SECONDS);

		super.onAuthenticationSuccess(request, response, authentication);
	}
}
