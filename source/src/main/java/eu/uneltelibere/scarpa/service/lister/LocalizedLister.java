package eu.uneltelibere.scarpa.service.lister;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.PageNavigator;

public interface LocalizedLister<T> {

	List<T> getList(int pageNumber, Locale locale);

	Optional<PageNavigator> getNavigator(String basePageUrl, int pageNumber);
}
