package eu.uneltelibere.scarpa.web.controller.json;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.model.QuickSearchResultEntry;
import eu.uneltelibere.scarpa.service.QuickSearchService;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;

@Controller
public abstract class QuickChooseController<T extends Entity> {

	private static final Logger LOG = LoggerFactory.getLogger(QuickChooseController.class);

	@Autowired
	private QuickSearchService<T> quickSearchService;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@GetMapping(path = "/choose")
	public @ResponseBody SearchResult<QuickSearchResultEntry> choose(
			@RequestParam(name = "q", defaultValue = "") String query, HttpServletRequest request) {
		LOG.info("Searching for {} with {}.", query, quickSearchService);
		List<QuickSearchResultEntry> result = quickSearchService.search(query);
		LOG.info("Found {} results.", result.size());

		SearchResult<QuickSearchResultEntry> searchResult = new SearchResult<>();
		searchResult.setResult(result);
		if (result.isEmpty()) {
			searchResult.setMessage(localizedMessageService.getMessageTemplate(messageNoneFound(), request));
		}
		return searchResult;
	}

	protected abstract String messageNoneFound();
}
