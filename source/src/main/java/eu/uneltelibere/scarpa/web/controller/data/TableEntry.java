package eu.uneltelibere.scarpa.web.controller.data;

import eu.uneltelibere.scarpa.model.Entity;

public class TableEntry<T extends Entity> {

	private T entity;

	private int extraEntryCount;

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public int getExtraEntryCount() {
		return extraEntryCount;
	}

	public void setExtraEntryCount(int extraEntryCount) {
		this.extraEntryCount = extraEntryCount;
	}
}
