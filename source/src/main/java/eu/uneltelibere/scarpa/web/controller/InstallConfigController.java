package eu.uneltelibere.scarpa.web.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.web.menu.InstallMenu;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;

@Controller
@RequestMapping("/install/config")
public class InstallConfigController {

	private static final Logger LOG = LoggerFactory.getLogger(InstallConfigController.class);

	private static final String SCARPA_CONFIG_SETTINGS_GROUP = "scarpa.config";

	@Autowired
	private MenuHolder menu;

	@Autowired
	private SettingService settingService;

	@GetMapping
	public String editInstallConfigPage(HttpServletRequest request, Model model) {
		LOG.info("Showing installation configuration edit page.");

		menu.set(InstallMenu.class, request, model);
		model.addAttribute("settings", settingService.getFromGroup(SCARPA_CONFIG_SETTINGS_GROUP));

		return "installConfig";
	}

	@PostMapping
	public String postInstallConfig(HttpServletRequest request, Model model) {
		LOG.info("Saving installation configuration.");

		int changeCount = 0;
		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramKey = params.nextElement();
			if (paramKey.startsWith(SCARPA_CONFIG_SETTINGS_GROUP)) {
				String oldValue = settingService.get(paramKey).orElseGet(() -> "");
				String newValue = request.getParameter(paramKey);
				if (!oldValue.equals(newValue)) {
					LOG.info("Setting {} has changed from [{}] to [{}].", paramKey, oldValue, newValue);
					settingService.set(paramKey, newValue);
					changeCount++;
				}
			}
		}
		LOG.info("Changed {} settings.", changeCount);

		return "redirect:/install/config";
	}
}
