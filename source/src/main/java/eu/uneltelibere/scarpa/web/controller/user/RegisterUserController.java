package eu.uneltelibere.scarpa.web.controller.user;

import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.error;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.PasswordService;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.service.ValidationException;
import eu.uneltelibere.scarpa.service.user.UserRegistrationService;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;
import eu.uneltelibere.scarpa.web.controller.user.data.RegisterUserRequest;

@Controller
@RequestMapping("/register")
public class RegisterUserController {

	private static final Logger LOG = LoggerFactory.getLogger(RegisterUserController.class);

	private static final String ATTRIBUTE_DATA = "registerUserRequest";

	private static final String MESSAGE_FAILED = "generic.alert.failed";

	public static final String VALIDATION_REGISTRATION_DISABLED = "validation.user.registration.disabled";

	public static final String SETTING_REGISTER_LANDING = "scarpa.config.register.users.landing.page";

	private static final String DEFAULT_VALUE_REGISTER_LANDING = "/";

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserRegistrationService userRegistrationService;

	@Autowired
	private PasswordService passwordService;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Autowired
	private SettingService settingService;

	@GetMapping
	public String showRegisterForm(Model model) {
		LOG.info("Showing the register account page");
		model.addAttribute("isRegisterEnabled", userRegistrationService.isUserRegistrationEnabled());
		model.addAttribute("passwordMinLen", passwordService.minPasswordLength());
		if (!model.containsAttribute(ATTRIBUTE_DATA)) {
			model.addAttribute(ATTRIBUTE_DATA, new RegisterUserRequest());
		}
		return "register";
	}

	@PostMapping
	public String register(@ModelAttribute RegisterUserRequest registerUserRequest, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		try {
			LOG.info("Received {}", registerUserRequest);
			if (!userRegistrationService.isUserRegistrationEnabled()) {
				throw new ValidationException(VALIDATION_REGISTRATION_DISABLED);
			}
			User user = modelMapper.map(registerUserRequest, User.class);
			LOG.info("Converted to email {} as {}", user.getEmail(), user.getUsername());
			passwordService.validatePassword(registerUserRequest.getPassword(), registerUserRequest.getPasswordAgain());
			userRegistrationService.register(user);
			try {
				request.login(registerUserRequest.getUsername(), registerUserRequest.getPassword());
			} catch (ServletException e) {
				LOG.warn("Failed to login automatically for {}.", registerUserRequest.getUsername());
				return "redirect:/";
			}
		} catch (ValidationException e) {
			LOG.warn("User is not valid: {}", e.getLocalizedMessage());
			error(redirectAttributes, localizedMessageService.getMessage(e.getMessage(), request));
			redirectAttributes.addFlashAttribute(ATTRIBUTE_DATA, registerUserRequest);
			return "redirect:/register";
		} catch (Exception e) {
			LOG.warn("Failed to process user registration request.", e);
			error(redirectAttributes, localizedMessageService.getMessage(MESSAGE_FAILED,
					new Object[] { e.getLocalizedMessage() }, request));
			redirectAttributes.addFlashAttribute(ATTRIBUTE_DATA, registerUserRequest);
			return "redirect:/register";
		}
		String landingPage = settingService.get(SETTING_REGISTER_LANDING)
				.orElseGet(() -> DEFAULT_VALUE_REGISTER_LANDING);
		return "redirect:" + landingPage;
	}
}
