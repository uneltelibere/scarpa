package eu.uneltelibere.scarpa.filter;

public class SingleConditionGroup<T> extends ConditionGroup<T> {

	public SingleConditionGroup(Condition<T> condition) {
		super(condition);
	}
}
