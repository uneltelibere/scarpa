package eu.uneltelibere.scarpa.model;

public class QuickSearchResultEntry extends Entity {

	private static final long serialVersionUID = 1L;

	private String label = "";

	public static class Builder {

		private final QuickSearchResultEntry location = new QuickSearchResultEntry();

		public Builder id(long id) {
			location.setId(id);
			return this;
		}

		public Builder label(String name) {
			location.setLabel(name);
			return this;
		}

		public QuickSearchResultEntry build() {
			return location;
		}
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
