package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

public interface RoleDao {

	List<Role> getRoles();

	Optional<Role> getRole(long id);

	List<Role> getRolesOfUser(User user);

	List<Role> getRolesInGroup(RoleGroup group);

	void update(Role role);
}
