package eu.uneltelibere.scarpa.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ping")
public class PingController {

	private static final Logger LOG = LoggerFactory.getLogger(PingController.class);

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody String ping() {
		LOG.info("Ping");
		return "pong\n";
	}
}
