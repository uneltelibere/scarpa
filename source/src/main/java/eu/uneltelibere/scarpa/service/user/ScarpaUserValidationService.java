package eu.uneltelibere.scarpa.service.user;

import java.util.Optional;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.service.ValidationException;

@Service
public class ScarpaUserValidationService implements UserValidationService {

	public static final String VALIDATION_DUPLICATE_USERNAME = "validation.user.name.duplicate";
	public static final String VALIDATION_DUPLICATE_MAIL = "validation.user.mail.duplicate";
	public static final String VALIDATION_INVALID_MAIL = "validation.user.mail.invalid";
	public static final String VALIDATION_INVALID_USERNAME = "validation.user.name.invalid";
	public static final String VALIDATION_MISSING_DATA = "validation.user.missing.data";
	public static final String VALIDATION_MISSING_EMAIL = "validation.user.missing.email";
	public static final String VALIDATION_MISSING_USERNAME = "validation.user.missing.username";
	public static final String MESSAGE_OTHER_DOMAIN = "validation.user.other.domain";

	public static final String EMAIL_DOMAIN_SETTING = "scarpa.config.register.users.from.domain";

	private static final Pattern USERNAME_PATTERN = Pattern.compile("([a-zA-Z0-9])+");

	private static final Logger LOG = LoggerFactory.getLogger(ScarpaUserValidationService.class);

	@Autowired
	private UserDuplicateAttributeDetectionService userDuplicateAttributeDetectionService;

	@Autowired
	private SettingService settingService;

	@Override
	public void validate(User user) {
		validateNotNull(user);
		validateMailAddress(user);
		validateUsername(user);
	}

	private void validateNotNull(User user) {
		if (user == null) {
			throw new ValidationException(VALIDATION_MISSING_DATA);
		}
		if (user.getEmail() == null) {
			throw new ValidationException(VALIDATION_MISSING_EMAIL);
		}
		if (user.getUsername() == null) {
			throw new ValidationException(VALIDATION_MISSING_USERNAME);
		}
	}

	private void validateMailAddress(User user) {
		String email = user.getEmail();
		validateMailAddress(email);
		validateAllowedDomain(email);
		validateUniqueEmail(user);
	}

	private void validateMailAddress(String email) {
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			throw new ValidationException(VALIDATION_INVALID_MAIL);
		}
	}

	private void validateAllowedDomain(String email) {
		if (!isDomainOk(email)) {
			throw new ValidationException(MESSAGE_OTHER_DOMAIN);
		}
	}

	private boolean isDomainOk(String email) {
		Optional<String> maybeDomainList = settingService.get(EMAIL_DOMAIN_SETTING);
		if (maybeDomainList.isPresent()) {
			String domainList = maybeDomainList.get();
			for (String domain : domainList.split(",")) {
				Pattern pattern = Pattern.compile(".+@" + Pattern.quote(domain));
				if (pattern.matcher(email).matches()) {
					LOG.info("{} matches {} from list {}", email, pattern, domainList);
					return true;
				}
			}
			LOG.info("{} didn't match any domain from list {}", email, domainList);
			return false;
		} else {
			LOG.info("Setting {} not found. Accepting user email addresses from any domain.", EMAIL_DOMAIN_SETTING);
			return true;
		}
	}

	private void validateUniqueEmail(User user) {
		if (!userDuplicateAttributeDetectionService.isEmailUnique(user)) {
			throw new ValidationException(VALIDATION_DUPLICATE_MAIL);
		}
	}

	/**
	 * Validate user name only for new users, because for existing users this
	 * property cannot be changed and thus it may not even be sent in the update
	 * request.
	 * 
	 * @param user the user for which to validate the user name
	 * @throws ValidationException if the user name is not valid
	 */
	private void validateUsername(User user) {
		if (user.getId() == User.NEW_ID) {
			validateUsernameAgainstPattern(user);
			validateUniqueUsername(user);
		}
	}

	private void validateUsernameAgainstPattern(User user) {
		if (!USERNAME_PATTERN.matcher(user.getUsername()).matches()) {
			throw new ValidationException(VALIDATION_INVALID_USERNAME);
		}
	}

	private void validateUniqueUsername(User user) {
		if (!userDuplicateAttributeDetectionService.isUsernameUnique(user)) {
			throw new ValidationException(VALIDATION_DUPLICATE_USERNAME);
		}
	}
}
