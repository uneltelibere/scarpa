package eu.uneltelibere.scarpa.model;

import java.time.LocalDateTime;

public class StaticPage extends Entity {

	private static final long serialVersionUID = 1L;

	public static class Version extends Entity {

		private static final long serialVersionUID = 1L;

		private StaticPage staticPage = new StaticPage();

		private String content = "";

		private int versionNumber;

		private User author = new User();

		private String comment = "";

		private LocalDateTime creationDate = LocalDateTime.now();

		public static class Builder {

			private Version version = new Version();

			public Builder id(long id) {
				version.setId(id);
				return this;
			}

			public Builder page(StaticPage page) {
				version.setStaticPage(page);
				return this;
			}

			public Builder versionNumber(int versionNumber) {
				version.setVersionNumber(versionNumber);
				return this;
			}

			public Version build() {
				return version;
			}
		}

		public StaticPage getStaticPage() {
			return staticPage;
		}

		public void setStaticPage(StaticPage staticPage) {
			this.staticPage = staticPage;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public int getVersionNumber() {
			return versionNumber;
		}

		public void setVersionNumber(int versionNumber) {
			this.versionNumber = versionNumber;
		}

		public User getAuthor() {
			return author;
		}

		public void setAuthor(User author) {
			this.author = author;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public LocalDateTime getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(LocalDateTime creationDate) {
			this.creationDate = creationDate;
		}
	}

	public static class Builder {

		private StaticPage page = new StaticPage();

		public Builder id(long id) {
			page.setId(id);
			return this;
		}

		public StaticPage build() {
			return page;
		}
	}
}
