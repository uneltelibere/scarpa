package eu.uneltelibere.scarpa.dao;

import eu.uneltelibere.scarpa.model.User;

public class UserCondition extends AbstractCondition<User> {

	public enum UserField implements Field {

		// @formatter:off
		USERNAME("username"),
		EMAIL("email"),
		GROUPS("groups"),
		STATE("state", Long.class);
		// @formatter:on

		private final String name;
		private final Class<?> type;

		private UserField(String name) {
			this(name, String.class);
		}

		private UserField(String name, Class<?> type) {
			this.name = name;
			this.type = type;
		}

		public ValueFactory valueFactory() {
			if (type.isAssignableFrom(Long.class)) {
				return new LongValueFactory();
			} else if (type.isAssignableFrom(Integer.class)) {
				return new IntegerValueFactory();
			} else if (type.isAssignableFrom(String.class)) {
				return new StringValueFactory();
			} else {
				throw new RuntimeException("Unknown type " + type);
			}
		}

		public static UserField from(String name) {
			for (UserField value : UserField.values()) {
				if (value.name.equals(name)) {
					return value;
				}
			}
			throw new IllegalArgumentException("No enum value with name " + name);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Class<?> getType() {
			return type;
		}
	}

	public enum UserOperator implements Operator {

		// @formatter:off
		ANY("any", Type.UNARY),
		EQUALS("eq", Type.BINARY),
		LIKE("like", Type.BINARY),
		IN("in", Type.BINARY),
		NONE("none", Type.UNARY);
		// @formatter:on

		private final String name;
		private final Type type;

		private UserOperator(String name, Type type) {
			this.name = name;
			this.type = type;
		}

		public static UserOperator from(String name) {
			for (UserOperator value : UserOperator.values()) {
				if (value.name.equals(name)) {
					return value;
				}
			}
			throw new IllegalArgumentException("No enum value with name " + name);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Type getType() {
			return type;
		}
	}

	public UserCondition(UserField field, UserOperator operator, Object value) {
		super(field, operator, value);
	}

	public UserCondition(UserField field, UserOperator operator) {
		super(field, operator);
	}
}
