package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import eu.uneltelibere.scarpa.dao.AggregateDao;
import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractAggregateDao<T extends Entity> implements AggregateDao<T> {

	private static final String WHOLE_ID = "wholeId";
	private static final String PART_ID = "partId";
	private static final String PART_IDS = "partIds";

	// @formatter:off
	private static final String REMOVE_PARTS = "DELETE FROM %s wp "
			+ "WHERE wp.%s = :" + WHOLE_ID + " "
			+ "AND wp.%s IN (:" + PART_IDS + ")";

	private static final String ADD_PARTS = "INSERT INTO %s (%s, %s) "
			+ "VALUES (:" + WHOLE_ID + ", :" + PART_ID + ")";

	private static final String READ_PART_IDS = "SELECT "
			+ "wp.%s "
			+ "FROM %s wp "
			+ "WHERE wp.%s = :" + WHOLE_ID + " ";
	// @formatter:on

	@Override
	public void removePartsFromWhole(T whole, List<Long> idsOfPartsToRemove) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue(WHOLE_ID, whole.getId());
		params.addValue(PART_IDS, idsOfPartsToRemove);
		jdbcTemplate().update(removePartsSql(), params);
	}

	private String removePartsSql() {
		return String.format(REMOVE_PARTS, wholePartTableName(), wholeFieldName(), partFieldName());
	}

	@Override
	public void addPartsToWhole(T whole, List<Long> idsOfPartsToAdd) {
		MapSqlParameterSource[] batchArgs = new MapSqlParameterSource[idsOfPartsToAdd.size()];
		for (int i = 0; i < idsOfPartsToAdd.size(); i++) {
			batchArgs[i] = new MapSqlParameterSource();
			batchArgs[i].addValue(WHOLE_ID, whole.getId());
			batchArgs[i].addValue(PART_ID, idsOfPartsToAdd.get(i));
		}
		jdbcTemplate().batchUpdate(addPartsSql(), batchArgs);
	}

	private String addPartsSql() {
		return String.format(ADD_PARTS, wholePartTableName(), wholeFieldName(), partFieldName());
	}

	@Override
	public List<Long> readPartIds(T whole) {
		MapSqlParameterSource params = new MapSqlParameterSource(WHOLE_ID, whole.getId());
		return jdbcTemplate().query(readPartIdsSql(), params, new RowMapper<Long>() {

			@Override
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong(partFieldName());
			}
		});
	}

	private String readPartIdsSql() {
		return String.format(READ_PART_IDS, partFieldName(), wholePartTableName(), wholeFieldName());
	}

	protected abstract NamedParameterJdbcTemplate jdbcTemplate();

	protected abstract String wholePartTableName();

	protected abstract String wholeFieldName();

	protected abstract String partFieldName();
}
