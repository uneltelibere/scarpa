package eu.uneltelibere.scarpa.dao.jdbc.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.QuickSearchDao;
import eu.uneltelibere.scarpa.dao.jdbc.JdbcMasterDao;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class UserQuickSearchDao extends JdbcMasterDao implements QuickSearchDao<User> {

	// @formatter:off
	private static final String GET_BY_SUBSTRING = "SELECT "
			+ "id, username "
			+ "FROM users "
			+ "WHERE lower(username) LIKE lower(:q) ";
	// @formatter:on

	@Override
	public List<User> search(String substring) {
		SqlParameterSource params = new MapSqlParameterSource("q", "%" + substring + "%");
		return jdbcTemplate.query(GET_BY_SUBSTRING, params, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User entity = new User();
				entity.setId(rs.getLong("id"));
				entity.setUsername(rs.getString("username"));
				return entity;
			}
		});
	}
}
