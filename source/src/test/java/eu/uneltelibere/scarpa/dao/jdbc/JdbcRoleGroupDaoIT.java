package eu.uneltelibere.scarpa.dao.jdbc;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.model.Language;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcRoleGroupDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private RoleGroupDao roleGroupDao;

	@Test
	@DatabaseSetup("/db/in/translation/one_translation_2_languages.xml")
	@ExpectedDatabase(value = "/db/out/roleGroup/created_role_group.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreate() {
		Language lang = new Language();
		lang.setId(1);

		Translation name = new Translation();
		name.setId(1);

		RoleGroup roleGroup = new RoleGroup();
		roleGroup.setCode("TEST_GROUP");
		roleGroup.setName(name);

		long id = roleGroupDao.create(roleGroup);

		assertThat(id, greaterThan(0L));
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroups() {
		List<RoleGroup> roleGroups = roleGroupDao.retrieve();

		assertEquals(3, roleGroups.size());
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupById() {
		RoleGroup roleGroup = roleGroupDao.retrieve(1).orElseThrow(IllegalStateException::new);

		assertEquals(1, roleGroup.getId());
		assertEquals("GROUP_ONE", roleGroup.getCode());
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupByIdNotFound() {
		Optional<RoleGroup> roleGroup = roleGroupDao.retrieve(0);
		assertFalse(roleGroup.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupByCode() {
		RoleGroup roleGroup = roleGroupDao.retrieveByCode("GROUP_ONE").orElseThrow(IllegalStateException::new);

		assertEquals(1, roleGroup.getId());
		assertEquals("GROUP_ONE", roleGroup.getCode());
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupByCodeNotFound() {
		Optional<RoleGroup> roleGroup = roleGroupDao.retrieveByCode("FOOBAR");
		assertFalse(roleGroup.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupWithTranslations() {
		Optional<RoleGroup> maybeRoleGroup = roleGroupDao.retrieve(1);

		assertTrue(maybeRoleGroup.isPresent());
		assertTrue(maybeRoleGroup.get().getName().getId() > 0);
	}

	@Test
	@DatabaseSetup("/db/in/roleGroup/role_groups_with_translations.xml")
	public void testGetRoleGroupWithNoTranslations() {
		Optional<RoleGroup> maybeRoleGroup = roleGroupDao.retrieve(3);

		assertTrue(maybeRoleGroup.isPresent());
		assertEquals(Translation.NEW_ID, maybeRoleGroup.get().getName().getId());
	}
}
