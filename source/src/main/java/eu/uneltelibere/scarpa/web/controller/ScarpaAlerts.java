package eu.uneltelibere.scarpa.web.controller;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public final class ScarpaAlerts {

	private static final String ALERT = "alert";
	private static final String ALERT_MESSAGE = "alertMessage";
	private static final String SUCCESS = "success";
	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	private ScarpaAlerts() {
	}

	public static void success(RedirectAttributes redirectAttributes, String message) {
		redirectAttributes.addFlashAttribute(ALERT, SUCCESS);
		redirectAttributes.addFlashAttribute(ALERT_MESSAGE, message);
	}

	public static void warn(Model model, String message) {
		model.addAttribute(ALERT, WARNING);
		model.addAttribute(ALERT_MESSAGE, message);
	}

	public static void warn(RedirectAttributes redirectAttributes, String message) {
		redirectAttributes.addFlashAttribute(ALERT, WARNING);
		redirectAttributes.addFlashAttribute(ALERT_MESSAGE, message);
	}

	public static void error(Model model, String message) {
		model.addAttribute(ALERT, ERROR);
		model.addAttribute(ALERT_MESSAGE, message);
	}

	public static void error(RedirectAttributes redirectAttributes, String message) {
		redirectAttributes.addFlashAttribute(ALERT, ERROR);
		redirectAttributes.addFlashAttribute(ALERT_MESSAGE, message);
	}
}
