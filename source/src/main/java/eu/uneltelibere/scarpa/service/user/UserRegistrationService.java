package eu.uneltelibere.scarpa.service.user;

import eu.uneltelibere.scarpa.model.User;

public interface UserRegistrationService {

	boolean isUserRegistrationEnabled();

	void register(User user);
}
