package eu.uneltelibere.scarpa.web.controller.search;

import java.util.Arrays;
import java.util.List;

public class SearchQueryBuilder {

	private static final List<String> UNARY_OPERATORS = Arrays.asList("any", "none");

	private final StringBuilder queryParams = new StringBuilder();

	private String exclude = "";

	public SearchQueryBuilder maybeAddLikeParam(String leftHand, String rightHand) {
		if (!rightHand.isEmpty()) {
			addParam(leftHand, "like", rightHand);
		}
		return this;
	}

	public <T> SearchQueryBuilder maybeAddEqParam(String leftHand, Object rightHand, T valueForAny) {
		if (!rightHand.equals(valueForAny)) {
			addParam(leftHand, "eq", rightHand);
		}
		return this;
	}

	public <T> SearchQueryBuilder maybeAddParam(String leftHand, String operator, Object rightHand, T valueForAny) {
		if (!"any".equals(operator)) {
			boolean rightHandIsAny = !isUnary(operator) && rightHand.equals(valueForAny);
			if (!rightHandIsAny) {
				addParam(leftHand, operator, rightHand);
			}
		}
		return this;
	}

	public SearchQueryBuilder addParam(String leftHand, String operator, Object rightHand) {
		if (leftHand.equalsIgnoreCase(exclude)) {
			return this;
		}

		if (queryParams.length() > 0) {
			queryParams.append("&");
		}
		queryParams.append("filter=");
		queryParams.append(leftHand).append("+").append(operator);
		if (!isUnary(operator)) {
			queryParams.append("+").append(rightHand);
		}

		return this;
	}

	private boolean isUnary(String operator) {
		return UNARY_OPERATORS.contains(operator);
	}

	public String query() {
		return queryParams.toString();
	}

	public SearchQueryBuilder exclude(String leftHand) {
		if (queryParams.length() > 0) {
			throw new IllegalStateException("exclude() has to be called before adding any parameters.");
		}
		exclude = leftHand;
		return this;
	}
}
