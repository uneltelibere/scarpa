package eu.uneltelibere.scarpa.dao;

import java.util.List;

public interface BulkRetriever<T> {

	List<T> retrieveByIds(List<Long> ids);
}
