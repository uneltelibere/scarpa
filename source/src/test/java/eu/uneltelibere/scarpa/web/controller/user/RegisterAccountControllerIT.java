package eu.uneltelibere.scarpa.web.controller.user;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration()
@AutoConfigureMockMvc
@Disabled("Fails with 401 access denied, to be investigated.")
class RegisterAccountControllerIT {

	@Autowired
	private MockMvc mvc;

	@Test
	void testShowRegisterForm() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/register")).andExpect(status().isOk());
	}
}
