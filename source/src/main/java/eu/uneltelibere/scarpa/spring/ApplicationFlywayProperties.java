package eu.uneltelibere.scarpa.spring;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "flyways.application")
public class ApplicationFlywayProperties extends FlywayProperties {
}
