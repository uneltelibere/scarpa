package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Attachment;

public interface AttachmentDao {

	long create(Attachment entity);

	Optional<Attachment> getById(long id);

	List<Attachment> retrieve(List<Long> ids);

	void delete(long id);
}
