package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.SettingDao;
import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcSettingDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private SettingDao settingDao;

	@Test
	@ExpectedDatabase(value = "/db/out/setting/created_setting.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreateSetting() {
		Setting setting = new Setting();
		setting.setKey("foo");
		setting.setValue("foo-value");

		long id = settingDao.create(setting);
		assertTrue(id > 0);
	}

	@Test
	@ExpectedDatabase(value = "/db/out/setting/created_user_setting.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreateUserSetting() {
		Setting setting = new Setting();
		setting.setKey("foo");
		setting.setValue("foo-value");

		User user = new User();
		user.setId(1);

		long id = settingDao.create(setting, user);
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("/db/in/setting/setting_foo.xml")
	public void testGetByKey() {
		Optional<Setting> settingInDb = settingDao.retrieve("foo");
		assertEquals(1, settingInDb.get().getId());
	}

	@Test
	public void testGetByKeyNonexistent() {
		Optional<Setting> settingInDb = settingDao.retrieve("bar");
		assertFalse(settingInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/setting/settings.xml")
	public void testGetByKeyPrefix() {
		List<Setting> settings = settingDao.retrieveFromGroup("scarpa.config");
		assertEquals(1, settings.size());
		assertEquals(2, settings.get(0).getId());
		assertEquals("scarpa.config.bar", settings.get(0).getKey());
		assertEquals("bar-value", settings.get(0).getValue());
	}

	@Test
	@DatabaseSetup("/db/in/setting/settings.xml")
	public void testGetByKeyPrefixNoneFound() {
		List<Setting> settings = settingDao.retrieveFromGroup("nonexistent.group");
		assertEquals(0, settings.size());
	}

	@Test
	@DatabaseSetup("/db/in/setting/user_setting_foo.xml")
	public void testGetByKeyAndUser() {
		User user = new User();
		user.setId(1);

		Optional<Setting> settingInDb = settingDao.retrieve("foo", user);
		assertEquals(1, settingInDb.get().getId());
	}

	@Test
	public void testGetByKeyAndUserNonexistent() {
		User user = new User();
		user.setId(1);

		Optional<Setting> settingInDb = settingDao.retrieve("bar", user);
		assertFalse(settingInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/setting/setting_foo.xml")
	@ExpectedDatabase(value = "/db/out/setting/setting_foo_updated.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdate() {
		Setting setting = new Setting();
		setting.setId(1);
		setting.setValue("foo-value-updated");
		settingDao.update(setting);
	}

	@Test
	@DatabaseSetup("/db/in/setting/setting_foo.xml")
	@ExpectedDatabase(value = "/db/in/setting/setting_foo.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateNonexistent() {
		Setting setting = new Setting();
		setting.setId(-1);

		settingDao.update(setting);
	}

	@Test
	@DatabaseSetup("/db/in/setting/user_setting_foo.xml")
	@ExpectedDatabase(value = "/db/out/setting/user_setting_foo_updated.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdateForUser() {
		Setting setting = new Setting();
		setting.setId(1);
		setting.setValue("foo-value-updated");

		User user = new User();
		user.setId(1);

		settingDao.update(setting, user);
	}

	@Test
	@DatabaseSetup("/db/in/setting/user_setting_foo.xml")
	@ExpectedDatabase(value = "/db/in/setting/user_setting_foo.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteForUserNonexistent() {
		Setting setting = new Setting();
		setting.setId(-1);

		User user = new User();
		user.setId(1);

		settingDao.update(setting, user);
	}
}
