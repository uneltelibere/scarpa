package eu.uneltelibere.scarpa;

import eu.uneltelibere.scarpa.api.AbstractRole;

public class UserAdminRole extends AbstractRole {

	public UserAdminRole() {
		super("USER_ADMIN");
	}
}
