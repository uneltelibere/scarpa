package eu.uneltelibere.scarpa.model;

import java.util.Collection;
import java.util.LinkedList;

public abstract class EntityWithAttachments extends Entity {

	private static final long serialVersionUID = 1L;

	private Collection<Attachment> attachments = new LinkedList<>();

	public Collection<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Collection<Attachment> attachments) {
		this.attachments = attachments;
	}
}
