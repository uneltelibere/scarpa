package eu.uneltelibere.scarpa.enums;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.ScarpaEnumDao;
import eu.uneltelibere.scarpa.model.ScarpaEnum;

public class AbstractScarpaEnum<T extends ScarpaEnum> {

	private Map<String, T> enumValueCache = new ConcurrentHashMap<>();

	@Autowired
	private ScarpaEnumDao<T> scarpaEnumDao;

	protected T enumValue(String code) {
		return enumValueCache.computeIfAbsent(code, this::getByCode);
	}

	private T getByCode(String code) {
		return scarpaEnumDao.getByCode(code)
				.orElseThrow(() -> new IllegalStateException("Enum value " + code + " not found in the database."));
	}
}
