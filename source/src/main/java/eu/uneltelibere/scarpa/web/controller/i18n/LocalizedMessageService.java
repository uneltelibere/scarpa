package eu.uneltelibere.scarpa.web.controller.i18n;

import javax.servlet.http.HttpServletRequest;

public interface LocalizedMessageService {

	String getMessageTemplate(String code, HttpServletRequest request);

	String getMessage(String code, HttpServletRequest request);

	String getMessage(String code, Object[] arguments, HttpServletRequest request);
}
