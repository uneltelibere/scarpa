package eu.uneltelibere.scarpa.web.controller;

import java.io.File;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.service.attach.AttachmentService;

@Controller
@RequestMapping("/attachment")
public class AttachmentController {

	@Autowired
	private AttachmentService attachmentService;

	@GetMapping(value = "/{id}/{fileName:.+}")
	@ResponseBody
	public FileSystemResource getFile(@PathVariable("id") long id, @PathVariable("fileName") String fileName,
			HttpServletResponse response) {
		response.setHeader("Content-Disposition", "attachment");

		Optional<Attachment> maybeAttachment = attachmentService.getById(id);
		if (maybeAttachment.isPresent()) {
			Attachment attachment = maybeAttachment.get();
			return new FileSystemResource(attachmentService.file(attachment));
		} else {
			return new FileSystemResource(new File("not-found"));
		}
	}
}
