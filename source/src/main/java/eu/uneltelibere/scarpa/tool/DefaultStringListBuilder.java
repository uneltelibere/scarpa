package eu.uneltelibere.scarpa.tool;

import java.util.Iterator;

public class DefaultStringListBuilder implements StringListBuilder {

	private static final int MAX_ELEMENTS_IN_LIST = 3;

	private int sourceSize;

	private Iterator<String> source;

	private int maxElementsInList = MAX_ELEMENTS_IN_LIST;

	public DefaultStringListBuilder withSourceSize(int sourceSize) {
		this.sourceSize = sourceSize;
		return this;
	}

	public DefaultStringListBuilder withSource(Iterator<String> source) {
		this.source = source;
		return this;
	}

	public DefaultStringListBuilder withElementsToShow(int maxElementsInList) {
		this.maxElementsInList = maxElementsInList;
		return this;
	}

	public String list() {
		int itemsInOutput = 0;
		if (!source.hasNext()) {
			return "";
		}
		int namesToShow = Math.min(maxElementsInList, sourceSize);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(source.next());
		itemsInOutput++;

		for (int i = 1; i < namesToShow - 1; i++) {
			stringBuilder.append(", ");
			stringBuilder.append(source.next());
			itemsInOutput++;
		}

		if (itemsInOutput == sourceSize - 1) {
			stringBuilder.append(" and ");
			stringBuilder.append(source.next());
		} else if (itemsInOutput < sourceSize - 1) {
			stringBuilder.append(", ");
			stringBuilder.append(source.next());
			stringBuilder.append(" and " + (sourceSize - itemsInOutput - 1) + " more");
		}

		return stringBuilder.toString();
	}
}
