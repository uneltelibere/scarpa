package eu.uneltelibere.scarpa.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.lister.PagedLister;
import eu.uneltelibere.scarpa.service.lister.PagedLocalizedLister;

@Configuration
public class ScarpaListerConfiguration {

	@Bean
	public PagedLister<User> userLister() {
		return new PagedLister<User>() {
		};
	}

	@Bean
	public PagedLocalizedLister<User> localizedUserLister() {
		return new PagedLocalizedLister<User>() {
		};
	}
}
