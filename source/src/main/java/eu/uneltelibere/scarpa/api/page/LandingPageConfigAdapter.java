package eu.uneltelibere.scarpa.api.page;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.api.Role;

public abstract class LandingPageConfigAdapter implements LandingPageConfig {

	@Override
	public Optional<String> applicationLandingPage() {
		return Optional.empty();
	}

	@Override
	public Map<Role, String> roleToWelcomePage() {
		return Collections.emptyMap();
	}

	@Override
	public void preprocess(Model model, HttpServletRequest request) {
	}
}
