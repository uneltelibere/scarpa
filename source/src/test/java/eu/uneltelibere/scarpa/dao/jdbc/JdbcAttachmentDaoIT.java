package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.AttachmentDao;
import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcAttachmentDaoIT {

	private static final String FILE_NAME_OUT = "test_out.pdf";
	private static final String FILE_NAME_1 = "test1.pdf";
	private static final String FILE_NAME_2 = "test2.pdf";

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private AttachmentDao attachmentDao;

	@Test
	@ExpectedDatabase(value = "/db/out/attachment/created_attachment.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreate() {
		Attachment attachment = new Attachment.Builder().originalFilename(FILE_NAME_OUT).size(1024)
				.contentType("application/pdf").build();
		long id = attachmentDao.create(attachment);
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("/db/in/attachment/attachment_one.xml")
	public void testGetById() {
		Optional<Attachment> attachmentInDb = attachmentDao.getById(1);
		assertEquals(1, attachmentInDb.get().getId());
		assertEquals(FILE_NAME_1, attachmentInDb.get().getOriginalFilename());
	}

	@Test
	public void testGetByIdNonexistent() {
		Optional<Attachment> attachmentInDb = attachmentDao.getById(-1);
		assertFalse(attachmentInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/attachment/attachment_one.xml")
	public void testRetrieveListOfOneFoundOne() {
		List<Attachment> attachmentInDb = attachmentDao.retrieve(Arrays.asList(1L));
		assertEquals(1, attachmentInDb.size());
		assertEquals(FILE_NAME_1, attachmentInDb.get(0).getOriginalFilename());
	}

	@Test
	@DatabaseSetup("/db/in/attachment/attachment_one.xml")
	public void testRetrieveListOfOneFoundNone() {
		List<Attachment> attachmentInDb = attachmentDao.retrieve(Arrays.asList(-1L));
		assertEquals(0, attachmentInDb.size());
	}

	@Test
	@DatabaseSetup(value = { "/db/in/attachment/attachment_one.xml", "/db/in/attachment/attachment_two.xml" })
	public void testRetrieveListOfTwoFoundTwo() {
		List<Attachment> attachmentInDb = attachmentDao.retrieve(Arrays.asList(1L, 2L));
		assertEquals(2, attachmentInDb.size());
		assertEquals(FILE_NAME_1, attachmentInDb.get(0).getOriginalFilename());
		assertEquals(FILE_NAME_2, attachmentInDb.get(1).getOriginalFilename());
	}

	@Test
	public void testRetrieveListNone() {
		List<Attachment> attachmentInDb = attachmentDao.retrieve(Collections.emptyList());
		assertEquals(0, attachmentInDb.size());
	}

	@Test
	public void testRetrieveListNull() {
		List<Attachment> attachmentInDb = attachmentDao.retrieve(null);
		assertEquals(0, attachmentInDb.size());
	}

	@Test
	@DatabaseSetup(value = { "/db/in/attachment/attachment_one.xml", "/db/in/attachment/attachment_two.xml" })
	@ExpectedDatabase(value = "/db/out/attachment/attachment_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDelete() {
		attachmentDao.delete(2);
	}

	@Test
	@DatabaseSetup("/db/in/attachment/attachment_one.xml")
	@ExpectedDatabase(value = "/db/out/attachment/attachment_one.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDeleteNonexistent() {
		attachmentDao.delete(-1);
	}
}
