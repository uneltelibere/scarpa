package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.List;

import eu.uneltelibere.scarpa.web.controller.data.Select;

public interface TableColumnOptionsProvider {

	Select<String> columnHeadingOptions(List<TableColumn.Heading> columnHeadings);
}
