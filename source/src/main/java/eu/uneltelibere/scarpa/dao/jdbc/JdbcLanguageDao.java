package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.LanguageDao;
import eu.uneltelibere.scarpa.model.Language;

@Repository
public class JdbcLanguageDao extends JdbcMasterDao implements LanguageDao {

	// @formatter:off
	private static final String LANGUAGE_BY_CODE = "SELECT "
			+ "id, code, name "
			+ "FROM language "
			+ "WHERE code = :code";
	// @formatter:off

	private ResultSetExtractor<Optional<Language>> singleLanguageExtractor = new ResultSetExtractor<Optional<Language>>() {

		@Override
		public Optional<Language> extractData(ResultSet rs) throws SQLException, DataAccessException {
			Optional<Language> maybeLanguage = Optional.empty();
			if (rs.next()) {
				Language state = languageFromResultSet(rs);
				maybeLanguage = Optional.of(state);
			}
			return maybeLanguage;
		}
	};

	private Language languageFromResultSet(ResultSet rs) throws SQLException {
		Language language = new Language();
		language.setId(rs.getLong("id"));
		language.setCode(rs.getString("code"));
		language.setName(rs.getString("name"));
		return language;
	}

	@Override
	public Optional<Language> getByCode(String code) {
		SqlParameterSource params = new MapSqlParameterSource("code", code);
		return jdbcTemplate.query(LANGUAGE_BY_CODE, params, singleLanguageExtractor);
	}
}
