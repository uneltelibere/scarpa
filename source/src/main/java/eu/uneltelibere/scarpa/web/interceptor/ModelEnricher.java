package eu.uneltelibere.scarpa.web.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import eu.uneltelibere.scarpa.RuntimeContext;
import eu.uneltelibere.scarpa.web.controller.UrlService;

@Component
public class ModelEnricher extends HandlerInterceptorAdapter {

	private static final Map<String, String> DEPENDENCIES_VERSIONS = new HashMap<>();

	static {
		DEPENDENCIES_VERSIONS.put("bootstrapVersion", "3.3.7-1");
		DEPENDENCIES_VERSIONS.put("bootstrap3TypeaheadVersion", "3.1.1");
		DEPENDENCIES_VERSIONS.put("html5shivVersion", "3.7.3");
		DEPENDENCIES_VERSIONS.put("respondVersion", "1.4.2");
		DEPENDENCIES_VERSIONS.put("jqueryVersion", "1.11.2");
		DEPENDENCIES_VERSIONS.put("jqueryUiVersion", "1.9.0");
		DEPENDENCIES_VERSIONS.put("jqueryFileUploadVersion", "9.10.1");
		DEPENDENCIES_VERSIONS.put("jqueryValidateVersion", "1.17.0");
	}

	private static final String SCARPA_APP_NAME_PROPERTY = "scarpa.app.name";
	private static final String SCARPA_APP_LOGO_PROPERTY = "scarpa.app.logo";
	private static final String SCARPA_FAVICON_PROPERTY = "scarpa.favicon.basename";

	private static final String DEFAULT_SCARPA_APP_NAME = "Scarpa";
	private static final String DEFAULT_SCARPA_APP_LOGO = "";
	private static final String DEFAULT_SCARPA_FAVICON = "favicon";

	private static final String SCARPA_APP_NAME_ATTRIBUTE = "scarpaAppName";
	private static final String SCARPA_APP_LOGO_ATTRIBUTE = "scarpaAppLogo";
	private static final String SCARPA_FAVICON_ATTRIBUTE = "scarpaFavicon";
	private static final String SCARPA_CUSTOM_CSS_ATTRIBUTE = "scarpaCustomCss";

	@Value("${" + SCARPA_APP_NAME_PROPERTY + ":" + DEFAULT_SCARPA_APP_NAME + "}")
	private String appName;

	@Value("${" + SCARPA_APP_LOGO_PROPERTY + ":" + DEFAULT_SCARPA_APP_LOGO + "}")
	private String appLogo;

	@Value("${" + SCARPA_FAVICON_PROPERTY + ":" + DEFAULT_SCARPA_FAVICON + "}")
	private String favIcon;

	@Autowired
	private RuntimeContext runtimeContext;

	@Autowired
	private UrlService urlService;

	@Autowired
	private CurrentUserService currentUserService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		currentUserService.setCurrentUser(request);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (modelAndView != null && !modelAndView.getViewName().startsWith("redirect:")) {
			setDependencyVersions(DEPENDENCIES_VERSIONS, modelAndView);

			Map<String, String> applicationDependencyVersions = runtimeContext.getDependenciesVersions();
			setDependencyVersions(applicationDependencyVersions, modelAndView);

			modelAndView.addObject(SCARPA_APP_NAME_ATTRIBUTE, appName);
			modelAndView.addObject(SCARPA_APP_LOGO_ATTRIBUTE, appLogo);
			modelAndView.addObject(SCARPA_FAVICON_ATTRIBUTE, favIcon);

			modelAndView.addObject(SCARPA_CUSTOM_CSS_ATTRIBUTE, runtimeContext.getCustomCss());

			setAppContext(request);
		}
	}

	private void setDependencyVersions(Map<String, String> dependencyVersions, ModelAndView modelAndView) {
		for (Map.Entry<String, String> dependencyVersion : dependencyVersions.entrySet()) {
			modelAndView.addObject(dependencyVersion.getKey(), dependencyVersion.getValue());
		}
	}

	private void setAppContext(HttpServletRequest request) {
		String appContext = runtimeContext.getAppContext();
		if (appContext.isEmpty()) {
			String url = urlService.appUrl(request);
			runtimeContext.setAppContext(url);
		}
	}
}
