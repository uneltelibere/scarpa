package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.model.PageNavigator;

public class PageNavigationBuilder {

	private static final String ELLIPSIS = "\u2026";

	private int pageCount;
	private int currentPage;
	private String basePageUrl = "/";

	public PageNavigationBuilder pageCount(int pageCount) {
		this.pageCount = pageCount;
		return this;
	}

	public PageNavigationBuilder currentPage(int currentPage) {
		this.currentPage = currentPage;
		return this;
	}

	public PageNavigationBuilder basePageUrl(String basePageUrl) {
		this.basePageUrl = basePageUrl;
		return this;
	}

	public PageNavigator build() {
		PageNavigator navigator = new PageNavigator();
		int earliest = Math.max(1, currentPage - 2);
		int latest = Math.min(pageCount, currentPage + 2);

		int prefixUpperLimit = Math.min(3, earliest);
		for (int prefixPageNumber = 1; prefixPageNumber <= prefixUpperLimit; prefixPageNumber++) {
			navigator.addButton(button(prefixPageNumber));
		}

		earliest = Math.max(prefixUpperLimit + 1, earliest);

		if (earliest > prefixUpperLimit + 1) {
			if (earliest - 1 > prefixUpperLimit) {
				navigator.addButton(ellipsisButton());
			} else {
				navigator.addButton(button(earliest - 1));
			}
		}
		for (int pageNumber = earliest; pageNumber <= latest; pageNumber++) {
			navigator.addButton(button(pageNumber));
		}
		if (latest < pageCount) {
			if (pageCount - latest > 1) {
				navigator.addButton(ellipsisButton());
			} else {
				navigator.addButton(button(latest + 1));
			}
		}

		int suffixLowerLimit = Math.max(pageCount - 2, latest + 2);
		for (int suffixPageNumber = suffixLowerLimit; suffixPageNumber <= pageCount; suffixPageNumber++) {
			navigator.addButton(button(suffixPageNumber));
		}

		PageNavigator.Button first = navigator.getFirst();
		if (currentPage == first.getTargetPage()) {
			first.setEnabled(false);
		}
		PageNavigator.Button last = navigator.getLast();
		if (currentPage == last.getTargetPage()) {
			last.setEnabled(false);
		}
		navigator.setBasePageUrl(basePageUrl);
		return navigator;
	}

	private PageNavigator.Button button(int pageNumber) {
		PageNavigator.Button button = new PageNavigator.Button();
		button.setLabel(String.valueOf(pageNumber));
		button.setTargetPage(pageNumber);
		if (pageNumber == currentPage) {
			button.setActive(true);
		}
		return button;
	}

	private PageNavigator.Button ellipsisButton() {
		PageNavigator.Button button = new PageNavigator.Button();
		button.setLabel(ELLIPSIS);
		button.setEnabled(false);
		return button;
	}
}
