package eu.uneltelibere.scarpa.spring;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DbConfig extends AbstractDataSourceConfig {

	@Bean(name = "masterDataSource")
	@Primary
	public DataSource masterDataSource(MasterFlywayProperties masterFlywayProperties, JdbcProperties jdbc) {
		migrateFlyway(masterFlywayProperties, jdbc);
		// @formatter:off
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName(jdbc.getDriverClassName())
				.url(jdbc.getMasterUrl())
				.username(jdbc.getUsername())
				.password(jdbc.getPassword())
				.build();
		// @formatter:on
		return dataSource;
	}

	@Bean(name = "applicationDataSource")
	public DataSource applicationDataSource(ApplicationFlywayProperties applicationFlywayProperties,
			JdbcProperties jdbc) {
		migrateFlyway(applicationFlywayProperties, jdbc);
		// @formatter:off
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName(jdbc.getDriverClassName())
				.url(jdbc.getApplicationUrl())
				.username(jdbc.getUsername())
				.password(jdbc.getPassword())
				.build();
		// @formatter:on
		return dataSource;
	}

	@Bean(name = "masterTransactionManager")
	public PlatformTransactionManager masterTxManager(MasterFlywayProperties masterFlywayProperties,
			JdbcProperties jdbc) {
		return new DataSourceTransactionManager(masterDataSource(masterFlywayProperties, jdbc));
	}

	@Bean(name = "applicationTransactionManager")
	public PlatformTransactionManager applicationTxManager(ApplicationFlywayProperties applicationFlywayProperties,
			JdbcProperties jdbc) {
		return new DataSourceTransactionManager(applicationDataSource(applicationFlywayProperties, jdbc));
	}
}
