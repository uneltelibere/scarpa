package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractEntityColumnProvider<T extends Entity> implements TableColumnProvider<T> {

	private static final String LABEL_UNKNOWN = "?";

	@Autowired(required = false)
	private List<TableColumnFactory<T>> tableColumnFactories;

	@Override
	public List<TableColumn.Heading> columnHeadings(String preferredColumns, HttpServletRequest request) {
		String[] columnCodes = preferredColumns.split(",");
		return columnHeadings(Arrays.stream(columnCodes), request);
	}

	@Override
	public List<TableColumn.Heading> columnHeadings(Stream<String> preferredColumnCodes, HttpServletRequest request) {
		List<TableColumn.Heading> columns = new LinkedList<>();
		preferredColumnCodes.forEach(columnCode -> {
			for (TableColumnFactory<T> columnFactory : tableColumnFactories) {
				if (columnFactory.canHandle(columnCode)) {
					columns.add(columnFactory.columnHeading(request));
				}
			}
		});
		return columns;
	}

	@Override
	public List<TableColumn<T>> columns(TableColumnSource<T> tableColumnSource) {
		List<TableColumn<T>> columns = new LinkedList<>();
		String[] columnCodes = tableColumnSource.getPreferredColumns().split(",");
		for (String code : columnCodes) {
			columns.add(entityColumn(tableColumnSource, code));
		}
		return columns;
	}

	private TableColumn<T> entityColumn(TableColumnSource<T> tableColumnSource, String columnCode) {
		for (TableColumnFactory<T> columnFactory : tableColumnFactories) {
			if (columnFactory.canHandle(columnCode)) {
				return columnFactory.column(tableColumnSource);
			}
		}

		return new TableColumnBuilder<T>().headingText(LABEL_UNKNOWN).valueText(text -> "")
				.detailedValueTexts(entity -> Collections.emptyList()).valueEntities(tableColumnSource.getEntities())
				.noSearch().build();
	}
}
