package eu.uneltelibere.scarpa.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ScarpaErrorController {

	@RequestMapping("/403.html")
	public ModelAndView handle403() {
		Map<String, Object> pageModel = new HashMap<>();
		pageModel.put("localizedTitle", "http.error.403.title");
		pageModel.put("code", "403");
		pageModel.put("type", "4xx");
		pageModel.put("message", "http.error.403.message");
		return new ModelAndView("error", pageModel);
	}
}
