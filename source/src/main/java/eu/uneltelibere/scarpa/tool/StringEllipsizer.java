package eu.uneltelibere.scarpa.tool;

public interface StringEllipsizer {

	String ellipsize(String s, int maxChars);
}
