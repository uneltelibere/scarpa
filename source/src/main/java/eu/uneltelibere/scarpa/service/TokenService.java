package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

public interface TokenService {

	Token createToken(User user);

	Token getToken(String token);

	boolean isValid(Token token);

	void deleteToken(Token token);

	void deleteTokensForUser(long userId);

	String tokenUrl(Token token);
}
