package eu.uneltelibere.scarpa.service;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DefaultUploadConfigurationService implements UploadConfigurationService {

	private static final String DEFAULT_UPLOAD_DIR = "";

	@Value("${app.install.upload-dir:" + DEFAULT_UPLOAD_DIR + "}")
	private String installUploadDir;

	@Value("${app.attachment.upload-dir:" + DEFAULT_UPLOAD_DIR + "}")
	private String attachmentUploadDir;

	@Override
	public String installUploadDirectory() throws URISyntaxException {
		if (uploadDirNotConfigured(installUploadDir)) {
			return pathToJar();
		} else {
			return installUploadDir;
		}
	}

	@Override
	public String attachmentUploadDirectory() throws URISyntaxException {
		if (uploadDirNotConfigured(attachmentUploadDir)) {
			return pathToJar();
		} else {
			return attachmentUploadDir;
		}
	}

	private boolean uploadDirNotConfigured(String uploadDir) {
		return DEFAULT_UPLOAD_DIR.equals(uploadDir);
	}

	private String pathToJar() throws URISyntaxException {
		CodeSource codeSource = getClass().getProtectionDomain().getCodeSource();
		File jarFile = new File(codeSource.getLocation().toURI().getPath());
		String jarDir = jarFile.getParentFile().getPath();
		return jarDir;
	}
}
