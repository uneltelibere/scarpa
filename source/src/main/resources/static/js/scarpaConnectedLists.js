(function() {
	var showButton = $('#showButton');
	var hideButton = $('#hideButton');
	var upButton = $('#upButton');
	var downButton = $('#downButton');
	var shownSelect = $('#shownSelect');
	var hiddenSelect = $('#hiddenSelect');

	var buildShownList = function() {
		var codes = $('#shownSelect option').map(function() {
			return this.value;
		}).get();
		$('#shownList').val(codes);
	}

	var isFirstSelectedForShown = function() {
		var selected = false;
		$('#shownSelect option:selected').each(function() {
			if ($(this).index() == 0) {
				selected = true;
			}
		});
		return selected;
	}

	var isLastSelectedForShown = function() {
		var selected = false;
		var lastIndex = $('#shownSelect option').length - 1;
		$('#shownSelect option:selected').each(function() {
			if ($(this).index() == lastIndex) {
				selected = true;
			}
		});
		return selected;
	}

	var isNothingSelectedForShown = function() {
		return $('#shownSelect option:selected').length == 0;
	}

	var isNothingSelectedForHidden = function() {
		return $('#hiddenSelect option:selected').length == 0;
	}

	hiddenSelect.change(function() {
		toggleShowButton();
	});

	shownSelect.change(function() {
		toggleHideButton();
		toggleUpButton();
		toggleDownButton();
	});

	var toggleShowButton = function() {
		if (isNothingSelectedForHidden()) {
			showButton.attr('disabled', 'disabled');
		} else {
			showButton.removeAttr('disabled');
		}
	}

	var toggleHideButton = function() {
		if (isNothingSelectedForShown()) {
			hideButton.attr('disabled', 'disabled');
		} else {
			hideButton.removeAttr('disabled');
		}
	}

	var toggleUpButton = function() {
		if (isFirstSelectedForShown() || isNothingSelectedForShown()) {
			upButton.attr('disabled', 'disabled');
		} else {
			upButton.removeAttr('disabled');
		}
	}

	var toggleDownButton = function() {
		if (isLastSelectedForShown() || isNothingSelectedForShown()) {
			downButton.attr('disabled', 'disabled');
		} else {
			downButton.removeAttr('disabled');
		}
	}

	var moveFromListToList = function(from, to) {
		var items = $('#' + from + ' option:selected');
		$.each(items, function(i, item) {
			$('#' + to).append($('<option>', {
				value : item.value,
				text : item.text
			}));
			$(item).remove();
		});
	}

	showButton.click(function() {
		moveFromListToList('hiddenSelect', 'shownSelect');
		buildShownList();
		toggleShowButton();
	});

	hideButton.click(function() {
		moveFromListToList('shownSelect', 'hiddenSelect');
		buildShownList();
		toggleHideButton();
	});

	upButton.click(function() {
		$('#shownSelect option:selected').each(function() {
			$(this).prev().before($(this));
		});
		buildShownList();
		toggleUpButton();
		toggleDownButton();
	});

	downButton.click(function() {
		$($('#shownSelect option:selected').get().reverse()).each(function() {
			$(this).next().after($(this));
		});
		buildShownList();
		toggleUpButton();
		toggleDownButton();
	});

	$(document).ready(function() {
		buildShownList();
		toggleShowButton();
		toggleHideButton();
		toggleUpButton();
		toggleDownButton();
	});
})();
