package eu.uneltelibere.scarpa.dao.sql;

import java.util.HashMap;
import java.util.Map;

public abstract class SqlCondition {

	private StringBuilder query = new StringBuilder();

	private Map<String, Object> params = new HashMap<>();

	public void addSql(String sql) {
		if (query.length() > 0) {
			query.append(conjunction());
		}
		query.append(sql);
	}

	protected abstract String conjunction();

	public void addSqlParameter(String key, Object value) {
		params.put(key, value);
	}

	public String query() {
		if (query.length() == 0) {
			return "true";
		}
		return query.toString();
	}

	public Map<String, Object> params() {
		return params;
	}
}
