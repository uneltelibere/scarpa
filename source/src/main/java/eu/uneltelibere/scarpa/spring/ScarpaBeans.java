package eu.uneltelibere.scarpa.spring;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScarpaBeans {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
