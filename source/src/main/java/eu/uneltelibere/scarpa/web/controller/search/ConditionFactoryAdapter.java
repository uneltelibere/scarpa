package eu.uneltelibere.scarpa.web.controller.search;

import java.util.Collection;
import java.util.List;

import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.filter.EmptyConditionGroup;

public class ConditionFactoryAdapter<T> implements ConditionFactory<T> {

	@Override
	public ConditionGroup<T> conditions(List<String> filters) {
		return new EmptyConditionGroup<>();
	}

	@Override
	public ConditionGroup<T> conditions(Collection<TextCondition> keyValues) {
		return new EmptyConditionGroup<>();
	}
}
