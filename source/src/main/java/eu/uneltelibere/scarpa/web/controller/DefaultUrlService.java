package eu.uneltelibere.scarpa.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DefaultUrlService implements UrlService {

	private static final String DEFAULT_CONTEXT_PATH = "";

	@Value("${server.contextPath:" + DEFAULT_CONTEXT_PATH + "}")
	private String contextPath;

	public String appUrl(HttpServletRequest request) {
		String requestUri = request.getRequestURI();
		String requestUrl = request.getRequestURL().toString();
		String baseUrl = requestUrl.substring(0, requestUrl.length() - requestUri.length());
		baseUrl = forceHttpsAndNoExplicitPort(baseUrl);
		baseUrl += contextPath;
		return baseUrl;
	}

	private String forceHttpsAndNoExplicitPort(String url) {
		return url.replace("http://", "https://").replaceFirst(":[0-9]+$", "");
	}
}
