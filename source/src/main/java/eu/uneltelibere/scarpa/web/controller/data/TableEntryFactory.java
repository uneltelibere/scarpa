package eu.uneltelibere.scarpa.web.controller.data;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface TableEntryFactory<T extends Entity> {

	List<TableEntry<T>> tableEntries(List<T> entities);
}
