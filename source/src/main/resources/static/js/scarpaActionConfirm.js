$(document).ready(function() {
	$('button[name=id]').click(function(event) {
		try {
			var q = $(this).data("question");
			var template = jQuery.validator.format(q);
			var question = template($(this).data("question-param"));
			if (!confirm(question)) {
				event.preventDefault();
				return false;
			}
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	});
});
