package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.List;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.model.Entity;

public interface TableColumnProvider<T extends Entity> {

	List<TableColumn.Heading> columnHeadings(String preferredColumns, HttpServletRequest request);

	List<TableColumn.Heading> columnHeadings(Stream<String> preferredColumnCodes, HttpServletRequest request);

	List<TableColumn<T>> columns(TableColumnSource<T> tableColumnSource);
}
