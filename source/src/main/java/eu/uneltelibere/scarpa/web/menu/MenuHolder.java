package eu.uneltelibere.scarpa.web.menu;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

public interface MenuHolder {

	void set(Class<? extends Menu> active, HttpServletRequest request, Model model);

	void setSubmenu(Class<? extends Menu> active, HttpServletRequest request, Model model);
}
