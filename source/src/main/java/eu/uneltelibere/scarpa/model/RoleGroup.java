package eu.uneltelibere.scarpa.model;

import java.util.Collection;
import java.util.LinkedList;

public class RoleGroup extends Entity {

	private static final long serialVersionUID = 1L;

	private String code = "";

	private Translation name = new Translation();

	private Collection<Role> roles = new LinkedList<>();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Translation getName() {
		return name;
	}

	public void setName(Translation name) {
		this.name = name;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
}
