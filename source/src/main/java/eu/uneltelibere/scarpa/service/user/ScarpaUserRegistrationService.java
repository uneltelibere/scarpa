package eu.uneltelibere.scarpa.service.user;

import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.RoleGroupService;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.service.UserService;

@Service
public class ScarpaUserRegistrationService implements UserRegistrationService {

	public static final String SETTING_REGISTER_ENABLED = "scarpa.config.register.users.enabled";

	private static final Logger LOG = LoggerFactory.getLogger(ScarpaUserRegistrationService.class);

	private static final String DEFAULT_VALUE_REGISTER_ENABLED = Boolean.FALSE.toString();

	private static final String GROUP_USER = "GROUP_USER";

	@Autowired
	private SettingService settingService;

	@Autowired
	private UserValidationService userValidationService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleGroupService roleGroupService;

	@Override
	public boolean isUserRegistrationEnabled() {
		return Boolean.valueOf(settingService.get(SETTING_REGISTER_ENABLED).orElse(DEFAULT_VALUE_REGISTER_ENABLED));
	}

	@Override
	public void register(User user) {
		userValidationService.validate(user);
		user.setId(userService.register(user));
		Optional<RoleGroup> maybeGroup = roleGroupService.retrieveByCode(GROUP_USER, Locale.ENGLISH);
		if (maybeGroup.isPresent()) {
			RoleGroup group = maybeGroup.get();
			List<Long> groupIDs = singletonList(group.getId());
			userService.setRoleGroupsForUser(user, groupIDs);
			LOG.debug("Saved groups {} for user {}", groupIDs, user.getId());
		} else {
			LOG.warn("Group {} not found. Registering {} without assigning to any group.", GROUP_USER, user);
		}
	}
}
