package eu.uneltelibere.scarpa.api;

import java.util.Collections;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class ScarpaMailer {

	private static final Logger LOG = LoggerFactory.getLogger(ScarpaMailer.class);

	private static final boolean MULTIPART_ON = true;

	private static final boolean AS_HTML = true;

	private String subject = "";
	private String from = "";
	private List<String> to = Collections.emptyList();
	private MailContent content = new MailContent();

	public ScarpaMailer subject(String subject) {
		this.subject = subject;
		return this;
	}

	public ScarpaMailer from(String from) {
		this.from = from;
		return this;
	}

	public ScarpaMailer to(String to) {
		this.to = Collections.singletonList(to);
		return this;
	}

	public ScarpaMailer to(List<String> to) {
		this.to = to;
		return this;
	}

	public ScarpaMailer content(MailContent content) {
		this.content = content;
		return this;
	}

	public boolean sendHtmlMail(JavaMailSender mailSender) {
		try {
			final MimeMessage mimeMessage = mailSender.createMimeMessage();
			final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, MULTIPART_ON, "UTF-8");
			message.setSubject(subject);
			message.setFrom(from);
			message.setTo(to.toArray(new String[to.size()]));
			message.setText(content.getHtmlBody(), AS_HTML);
			for (MailContent.InlineImage image : content.getImages()) {
				message.addInline(image.getResourceName(), image.getSource(), image.getImageContentType());
			}

			mailSender.send(mimeMessage);
			LOG.info("Mail sent to {}.", to);
			return true;
		} catch (MailException | MessagingException e) {
			LOG.warn("Error sending mail", e);
			return false;
		}
	}
}
