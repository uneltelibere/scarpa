package eu.uneltelibere.scarpa.api;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.FilteredPagedRetriever;
import eu.uneltelibere.scarpa.dao.SortCriteria;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.service.EntityDetailService;

public abstract class AbstractPagedRetrievingService<T extends Entity> implements FilteredPagedRetrievingService<T> {

	@Autowired
	private FilteredPagedRetriever<T> dao;

	private EntityDetailService<T> entityDetailService;

	@Override
	public int count(ConditionGroup<T> conditions) {
		return dao.count(conditions);
	}

	@Override
	public List<T> retrieveFiltered(ConditionGroup<T> conditions, int offset, int limit) {
		List<T> filteredList = dao.retrieveFiltered(conditions, Page.of(offset, limit));
		if (entityDetailService != null) {
			for (T filteredElement : filteredList) {
				entityDetailService.entityWithDetails(filteredElement);
			}
		}
		return filteredList;
	}

	@Override
	public List<T> retrieveFilteredAndSorted(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria,
			int offset, int limit) {
		return dao.retrieveFilteredAndSorted(conditions, sortCriteria, Page.of(offset, limit));
	}

	public void setEntityDetailService(EntityDetailService<T> entityDetailService) {
		this.entityDetailService = entityDetailService;
	}
}
