package eu.uneltelibere.scarpa;

import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackageClasses = { Scarpa.class })
@EnableAsync
public class Scarpa {

	private Class<?> configuration;

	public Scarpa withConfiguration(Class<?> configuration) {
		this.configuration = configuration;
		return this;
	}

	public void init(String[] args) {
		List<Class<?>> sources = new LinkedList<>();
		sources.add(Scarpa.class);
		if (configuration != null) {
			sources.add(configuration);
		}

		checkEnvironmentIsSane();
		SpringApplication.run(sources.toArray(new Class<?>[sources.size()]), args);
	}

	public void checkEnvironmentIsSane() {
		String scarpaHome = System.getenv("SCARPA_HOME");
		if (scarpaHome == null) {
			throw new IllegalStateException("The SCARPA_HOME environment variable is not set!");
		}
	}
}
