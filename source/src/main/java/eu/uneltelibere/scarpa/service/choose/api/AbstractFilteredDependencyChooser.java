package eu.uneltelibere.scarpa.service.choose.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.model.PageNavigator;
import eu.uneltelibere.scarpa.service.lister.FilteredPagedLister;

public abstract class AbstractFilteredDependencyChooser<T extends Entity> extends AbstractDependencyChooser<T> {

	@Autowired
	private FilteredPagedLister<T> lister;

	@Override
	protected List<T> getElements(DependencyChooserParameters<T> params) {
		ConditionGroup<T> conditions = params.getConditions();
		int pageNumber = params.getPageNumber();
		return lister.getList(conditions, pageNumber);
	}

	@Override
	protected Optional<PageNavigator> getNavigator(DependencyChooserParameters<T> params) {
		ConditionGroup<T> conditions = params.getConditions();
		String basePageUrl = params.getChooserUrl();
		int pageNumber = params.getPageNumber();
		return lister.getNavigator(conditions, basePageUrl, pageNumber);
	}
}
