package eu.uneltelibere.scarpa.web.controller;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import eu.uneltelibere.scarpa.service.UploadConfigurationService;
import eu.uneltelibere.scarpa.service.VersionService;
import eu.uneltelibere.scarpa.service.fs.FileSystemService;
import eu.uneltelibere.scarpa.web.controller.data.AjaxResult;
import eu.uneltelibere.scarpa.web.menu.InstallMenu;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;

@Controller
@MultipartConfig
@RequestMapping("/install")
public class InstallController {

	private static final Logger LOG = LoggerFactory.getLogger(InstallController.class);

	private static final ScheduledExecutorService WORKER = Executors.newSingleThreadScheduledExecutor();

	private static final int RESTART_RETURN_CODE = 64;

	@Autowired
	private VersionService versionService;

	@Autowired
	private UploadConfigurationService uploadConfigService;

	@Autowired
	private FileSystemService fileSystemService;

	@Autowired
	private ConfigurableApplicationContext context;

	@Autowired
	private MenuHolder menu;

	private Runnable shutdown = new Runnable() {

		@Override
		public void run() {
			LOG.info("Shutting down..." + context);
			context.close();
			System.exit(RESTART_RETURN_CODE);
		}
	};

	@RequestMapping(method = RequestMethod.GET)
	public String showInstallPage(HttpServletRequest request, Model model) {
		LOG.info("Showing the install page");
		menu.set(InstallMenu.class, request, model);
		model.addAttribute("artifactVersion", versionService.version());
		model.addAttribute("artifactDate", versionService.buildDate());
		return "install";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody AjaxResult ajaxUpload(@RequestParam("packageFile") MultipartFile incomingFile) {
		LOG.info("Received an upload request");
		AjaxResult uploadResult = new AjaxResult();

		if (!incomingFile.isEmpty()) {
			try {
				String uploadDir = uploadConfigService.installUploadDirectory();
				String fileName = incomingFile.getOriginalFilename();

				if (!fileName.endsWith(".jar")) {
					LOG.error("Tried to install a file that is not a JAR: " + fileName);
					uploadResult.setSuccess(false);
					uploadResult.setMessage("The file you uploaded is not a JAR.");
				} else {
					LOG.info("Uploading " + fileName + " to " + uploadDir);

					fileSystemService.ensureDirExists(uploadDir);
					File destination = Paths.get(uploadDir, fileName).toFile();

					if (destination.exists()) {
						LOG.error(destination + " already exists.");
						uploadResult.setSuccess(false);
						uploadResult.setMessage("This version is already installed.");
					} else {
						incomingFile.transferTo(destination);

						LOG.info("Uploaded " + incomingFile.getSize() + " bytes.");
						uploadResult.setSuccess(true);
					}
				}
			} catch (IOException | URISyntaxException e) {
				LOG.error("Upload failed.", e);
				uploadResult.setSuccess(false);
				uploadResult.setMessage("File was uploaded but could not be saved.");
			}
		} else {
			uploadResult.setSuccess(false);
			uploadResult.setMessage("The file was empty.");
		}

		return uploadResult;
	}

	@RequestMapping(path = "/restart", method = RequestMethod.POST)
	public @ResponseBody AjaxResult ajaxRestart() {
		LOG.info("Restart requested. Scheduling shutdown...");

		WORKER.schedule(shutdown, 1, TimeUnit.SECONDS);

		AjaxResult restartResult = new AjaxResult();
		restartResult.setSuccess(true);
		restartResult.setMessage("Restart requested.");
		return restartResult;
	}
}
