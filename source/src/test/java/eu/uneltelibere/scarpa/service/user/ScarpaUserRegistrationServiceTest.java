package eu.uneltelibere.scarpa.service.user;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.RoleGroupService;
import eu.uneltelibere.scarpa.service.SettingService;
import eu.uneltelibere.scarpa.service.UserService;
import eu.uneltelibere.scarpa.service.ValidationException;

@ExtendWith(MockitoExtension.class)
class ScarpaUserRegistrationServiceTest {

	private static final String EMAIL = "someone@example.com";
	private static final String USER = "user";

	@Mock
	private SettingService settingService;

	@Mock
	private UserValidationService userValidationService;

	@Mock
	private UserService userService;

	@Mock
	private RoleGroupService roleGroupService;

	@InjectMocks
	private UserRegistrationService userRegistrationService = new ScarpaUserRegistrationService();

	@Test
	@DisplayName("Detect registration disabled when setting is missing")
	void detectRegistrationDisabledSettingMissing() {
		when(settingService.get(ScarpaUserRegistrationService.SETTING_REGISTER_ENABLED)).thenReturn(Optional.empty());
		assertFalse(userRegistrationService.isUserRegistrationEnabled());
	}

	@Test
	@DisplayName("Detect registration disabled when setting is not a boolean")
	void detectRegistrationDisabledSettingNotBoolean() {
		when(settingService.get(ScarpaUserRegistrationService.SETTING_REGISTER_ENABLED)).thenReturn(Optional.of("foo"));
		assertFalse(userRegistrationService.isUserRegistrationEnabled());
	}

	@Test
	@DisplayName("Detect registration disabled when setting is false")
	void detectRegistrationDisabledSettingFalse() {
		when(settingService.get(ScarpaUserRegistrationService.SETTING_REGISTER_ENABLED))
				.thenReturn(Optional.of("false"));
		assertFalse(userRegistrationService.isUserRegistrationEnabled());
	}

	@Test
	@DisplayName("Detect registration enabled when setting is true")
	void detectRegistrationEnabledSettingTrue() {
		when(settingService.get(ScarpaUserRegistrationService.SETTING_REGISTER_ENABLED))
				.thenReturn(Optional.of("true"));
		assertTrue(userRegistrationService.isUserRegistrationEnabled());
	}

	@Test
	@DisplayName("Reject invalid user")
	void rejectInvalidUser() {
		User user = new User.Builder().username(USER).email(EMAIL).build();
		doThrow(ValidationException.class).when(userValidationService).validate(user);

		assertThrows(ValidationException.class, () -> userRegistrationService.register(user));
	}
}
