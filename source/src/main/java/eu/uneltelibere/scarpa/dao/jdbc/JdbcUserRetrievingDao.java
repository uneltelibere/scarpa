package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.api.Page;
import eu.uneltelibere.scarpa.dao.AbstractMasterPagedRetriever;
import eu.uneltelibere.scarpa.dao.SortCriteria;
import eu.uneltelibere.scarpa.dao.UserCondition;
import eu.uneltelibere.scarpa.dao.UserCondition.UserField;
import eu.uneltelibere.scarpa.dao.UserCondition.UserOperator;
import eu.uneltelibere.scarpa.dao.sql.AndSqlCondition;
import eu.uneltelibere.scarpa.dao.sql.SqlCondition;
import eu.uneltelibere.scarpa.filter.Condition;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcUserRetrievingDao extends AbstractMasterPagedRetriever<User> {

	// @formatter:off
	private static final String SCARPA_CONDITION = "$SCARPA_CONDITION";

	private static final String FROM_USERS = "FROM users u "
			+ "WHERE " + SCARPA_CONDITION;

	private static final String COUNT_USERS = "SELECT COUNT(*) " + FROM_USERS;

	private static final String GET_USERS = "SELECT "
			+ "id, username "
			+ FROM_USERS + " "
			+ "ORDER BY id DESC "
			+ "OFFSET :offset LIMIT :limit";
	// @formatter:on

	private RowMapper<User> rowMapper = new RowMapper<User>() {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getLong("id"));
			user.setUsername(rs.getString("username"));
			return user;
		}
	};

	@Override
	public int count(ConditionGroup<User> conditions) {
		return count(conditions, COUNT_USERS);
	}

	@Override
	public List<User> retrieveFilteredAndSorted(ConditionGroup<User> conditions,
			Collection<SortCriteria<User>> sortCriteria, Page page) {
		return retrieveFilteredAndSorted(conditions, sortCriteria, page, GET_USERS, rowMapper);
	}

	@Override
	protected SqlCondition sqlCondition(ConditionGroup<User> conditions) {
		SqlCondition sqlCondition = new AndSqlCondition();

		for (Condition<User> condition : conditions.getConditions()) {
			UserCondition entityCondition = (UserCondition) condition;
			addUsernameConditionToSql(entityCondition, sqlCondition);
			addEmailConditionToSql(entityCondition, sqlCondition);
		}
		return sqlCondition;
	}

	private void addUsernameConditionToSql(UserCondition entityCondition, SqlCondition sqlCondition) {
		if (entityCondition.getField().equals(UserField.USERNAME)) {
			if (entityCondition.getOperator().equals(UserOperator.EQUALS)) {
				sqlCondition.addSql("LOWER(u.username) = LOWER(:condition_name) ");
				sqlCondition.addSqlParameter("condition_name", entityCondition.getValue());
			} else if (entityCondition.getOperator().equals(UserOperator.LIKE)) {
				sqlCondition.addSql("LOWER(u.username) LIKE LOWER(:condition_name) ");
				sqlCondition.addSqlParameter("condition_name", "%" + entityCondition.getValue() + "%");
			}
		}
	}

	private void addEmailConditionToSql(UserCondition entityCondition, SqlCondition sqlCondition) {
		if (entityCondition.getField().equals(UserField.EMAIL)) {
			if (entityCondition.getOperator().equals(UserOperator.EQUALS)) {
				sqlCondition.addSql("LOWER(u.email) = LOWER(:condition_mail) ");
				sqlCondition.addSqlParameter("condition_mail", entityCondition.getValue());
			} else if (entityCondition.getOperator().equals(UserOperator.LIKE)) {
				sqlCondition.addSql("LOWER(u.email) LIKE LOWER(:condition_mail) ");
				sqlCondition.addSqlParameter("condition_mail", "%" + entityCondition.getValue() + "%");
			}
		}
	}
}
