package eu.uneltelibere.scarpa.dao;

public class AbstractSortCriteria<T> implements SortCriteria<T> {

	private final Field field;

	private final Order order;

	public AbstractSortCriteria(Field field, Order order) {
		this.field = field;
		this.order = order;
	}

	public AbstractSortCriteria(Field field) {
		this(field, Order.ASC);
	}

	@Override
	public Field getField() {
		return field;
	}

	@Override
	public Order getOrder() {
		return order;
	}
}
