package eu.uneltelibere.scarpa.web.controller.data;

import java.util.LinkedList;
import java.util.List;

public class AjaxResult {

	public static final int PROGRESS_UNKNOWN = -1;

	public static final int PROGRESS_DONE = 100;

	public static final AjaxResult OK = ok();

	public static final AjaxResult FAILURE = failure();

	private boolean success = true;

	private int progress = PROGRESS_UNKNOWN;

	private String message = "";

	private String details = "";

	private List<String> issues = new LinkedList<>();

	private List<String> warnings = new LinkedList<>();

	private static AjaxResult ok() {
		return new AjaxResult();
	}

	private static AjaxResult failure() {
		AjaxResult result = new AjaxResult();
		result.setSuccess(false);
		return result;
	}

	public static AjaxResult failure(String reason) {
		AjaxResult result = new AjaxResult();
		result.setSuccess(false);
		result.setMessage(reason);
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public List<String> getIssues() {
		return issues;
	}

	public void addIssue(String issue) {
		this.issues.add(issue);
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void addWarning(String warning) {
		this.warnings.add(warning);
	}
}
