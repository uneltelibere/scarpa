package eu.uneltelibere.scarpa.web.controller.data.group;

import java.util.List;

import eu.uneltelibere.scarpa.model.User;

public class WebUser extends User {

	private static final long serialVersionUID = 1L;

	private List<WebRoleGroup> webRoleGroups;

	private String groupList;

	public List<WebRoleGroup> getWebRoleGroups() {
		return webRoleGroups;
	}

	public void setWebRoleGroups(List<WebRoleGroup> webRoleGroups) {
		this.webRoleGroups = webRoleGroups;
	}

	public String getGroupList() {
		return groupList;
	}

	public void setGroupList(String groupList) {
		this.groupList = groupList;
	}
}
