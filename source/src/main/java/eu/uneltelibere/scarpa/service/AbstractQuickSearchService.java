package eu.uneltelibere.scarpa.service;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.QuickSearchDao;
import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.model.QuickSearchResultEntry;

public abstract class AbstractQuickSearchService<T extends Entity> implements QuickSearchService<T> {

	@Autowired
	private QuickSearchDao<T> quickSearchDao;

	@Override
	public List<QuickSearchResultEntry> search(String searchString) {
		return quickSearchDao.search(searchString).stream().map(entity -> resultEntry(entity)).collect(toList());
	}

	private QuickSearchResultEntry resultEntry(T entity) {
		return new QuickSearchResultEntry.Builder().id(entity.getId()).label(label(entity)).build();
	}
}
