package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.StaticPage;

public interface StaticPageDao {

	Optional<StaticPage.Version> getLatestVersionOfPage(long pageId);

	Optional<StaticPage.Version> getVersionOfPage(long pageId, long versionNumber);

	List<StaticPage.Version> getVersionsOfPage(long pageId);

	long create(StaticPage.Version pageVersion);
}
