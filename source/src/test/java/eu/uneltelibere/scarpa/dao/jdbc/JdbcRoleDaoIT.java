package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcRoleDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private RoleDao roleDao;

	@Test
	@DatabaseSetup("/db/in/role/roles_with_translations.xml")
	public void testGetRoles() {
		List<Role> roles = roleDao.getRoles();

		assertEquals(3, roles.size());
	}

	@Test
	@DatabaseSetup("/db/in/role/roles_with_translations.xml")
	public void testGetRoleWithTranslations() {
		Optional<Role> maybeRole = roleDao.getRole(1);

		assertTrue(maybeRole.isPresent());
		assertTrue(maybeRole.get().getTranslatedDescription().getId() > 0);
	}

	@Test
	@DatabaseSetup("/db/in/role/roles_with_translations.xml")
	public void testGetRoleWithNoTranslations() {
		Optional<Role> maybeRole = roleDao.getRole(3);

		assertTrue(maybeRole.isPresent());
		assertEquals(Translation.NEW_ID, maybeRole.get().getTranslatedDescription().getId());
	}

	@Test
	public void testGetRoleNonexistent() {
		Optional<Role> maybeRole = roleDao.getRole(-1);
		assertFalse(maybeRole.isPresent());
	}
}
