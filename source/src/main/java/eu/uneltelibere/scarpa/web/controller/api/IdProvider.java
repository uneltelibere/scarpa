package eu.uneltelibere.scarpa.web.controller.api;

public interface IdProvider<T> {

	String id(final T entity);

	String id(final long id);

	long id(String idString);
}
