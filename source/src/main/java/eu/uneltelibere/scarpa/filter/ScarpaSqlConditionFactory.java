package eu.uneltelibere.scarpa.filter;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.dao.sql.AndSqlCondition;
import eu.uneltelibere.scarpa.dao.sql.OrSqlCondition;
import eu.uneltelibere.scarpa.dao.sql.SqlCondition;

@Component
public class ScarpaSqlConditionFactory implements SqlConditionFactory {

	@Override
	public SqlCondition sqlCondition(ConditionGroup<?> conditionGroup) {
		if (conditionGroup instanceof OrConditionGroup<?>) {
			return new OrSqlCondition();
		}
		return new AndSqlCondition();
	}
}
