package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

import eu.uneltelibere.scarpa.dao.TranslationDao;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcTranslationDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private TranslationDao translationDao;

	@Test
	@DatabaseSetup("/db/in/translation/one_translation_2_languages.xml")
	public void testRetrieveTwoLanguages() {
		Optional<Translation> maybeTranslation = translationDao.retrieve(1);

		assertTrue(maybeTranslation.isPresent());
		assertEquals("neutral text", maybeTranslation.get().getNeutralText());
		assertEquals(2, maybeTranslation.get().getTranslationTexts().size());
		assertEquals("en", maybeTranslation.get().getTranslationTexts().get(0).getLanguage().getCode());
		assertEquals("english text", maybeTranslation.get().getTranslationTexts().get(0).getTranslatedText());
		assertEquals("ro", maybeTranslation.get().getTranslationTexts().get(1).getLanguage().getCode());
		assertEquals("romanian text", maybeTranslation.get().getTranslationTexts().get(1).getTranslatedText());
	}

	@Test
	@DatabaseSetup("/db/in/translation/one_translation_0_languages.xml")
	public void testRetrieveNoLanguages() {
		Optional<Translation> maybeTranslation = translationDao.retrieve(1);

		assertTrue(maybeTranslation.isPresent());
		assertEquals("neutral text", maybeTranslation.get().getNeutralText());
		assertEquals(0, maybeTranslation.get().getTranslationTexts().size());
	}
}
