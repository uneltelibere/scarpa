package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import eu.uneltelibere.scarpa.dao.StaticPageDao;
import eu.uneltelibere.scarpa.model.StaticPage;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcStaticPageDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private StaticPageDao staticPageDao;

	@Test
	@DatabaseSetup("/db/in/staticPage/static_page.xml")
	public void testGetLatestVersionOfPageWithNoVersions() {
		Optional<StaticPage.Version> entityInDb = staticPageDao.getLatestVersionOfPage(1);
		assertFalse(entityInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/staticPage/static_page.xml")
	public void testGetLatestVersionOfPage() {
		Optional<StaticPage.Version> entityInDb = staticPageDao.getLatestVersionOfPage(2);
		assertEquals(2, entityInDb.get().getId());
		assertEquals(2, entityInDb.get().getStaticPage().getId());
		assertEquals(2, entityInDb.get().getVersionNumber());
		assertEquals("some content", entityInDb.get().getContent());
		assertEquals(1, entityInDb.get().getAuthor().getId());
		assertEquals("updated", entityInDb.get().getComment());
	}

	@Test
	@DatabaseSetup("/db/in/staticPage/static_page.xml")
	public void testGetVersionOfPage() {
		Optional<StaticPage.Version> entityInDb = staticPageDao.getVersionOfPage(2, 1);
		assertEquals(1, entityInDb.get().getId());
		assertEquals(2, entityInDb.get().getStaticPage().getId());
		assertEquals(1, entityInDb.get().getVersionNumber());
		assertNull(entityInDb.get().getContent());
		assertNull(entityInDb.get().getAuthor().dbRef());
		assertNull(entityInDb.get().getComment());
	}

	@Test
	public void testGetLatestVersionOfPageNonexistent() {
		Optional<StaticPage.Version> entityInDb = staticPageDao.getLatestVersionOfPage(-1);
		assertFalse(entityInDb.isPresent());
	}

	@Test
	@DatabaseSetup("/db/in/staticPage/single_empty_static_page.xml")
	@ExpectedDatabase(value = "/db/out/staticPage/created_static_page.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testCreateVersion() {
		StaticPage staticPage = new StaticPage();
		staticPage.setId(1);

		User author = new User();
		author.setId(1);

		StaticPage.Version pageVersion = new StaticPage.Version();
		pageVersion.setContent("page content");
		pageVersion.setVersionNumber(1);
		pageVersion.setStaticPage(staticPage);
		pageVersion.setAuthor(author);
		pageVersion.setCreationDate(LocalDateTime.of(2018, 6, 23, 16, 47));
		pageVersion.setComment("created");

		long id = staticPageDao.create(pageVersion);
		assertTrue(id > 0);
	}
}
