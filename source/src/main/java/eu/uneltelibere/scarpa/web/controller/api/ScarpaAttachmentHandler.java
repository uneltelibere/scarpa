package eu.uneltelibere.scarpa.web.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.service.attach.AttachmentService;

@Component
public class ScarpaAttachmentHandler implements AttachmentHandler {

	@Autowired
	private AttachmentService attachmentService;

	@Override
	public Attachment saveAttachment(MultipartFile incomingFile) {
		Attachment attach = new Attachment();
		attach.setOriginalFilename(incomingFile.getOriginalFilename());
		attach.setSize(incomingFile.getSize());
		attach.setContentType(incomingFile.getContentType());
		attach.setIncomingFile(incomingFile);
		return attachmentService.save(attach);
	}
}
