package eu.uneltelibere.scarpa.web.controller.data.column;

import javax.servlet.http.HttpServletRequest;

public interface UserEntityColumnService {

	void save(String shownList, HttpServletRequest request);

	String getHiddenList(HttpServletRequest request);

	String getShownList(HttpServletRequest request);
}
