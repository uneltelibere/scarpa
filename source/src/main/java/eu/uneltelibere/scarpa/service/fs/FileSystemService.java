package eu.uneltelibere.scarpa.service.fs;

import java.io.IOException;
import java.net.URISyntaxException;

public interface FileSystemService {

	void ensureDirExists(String dir) throws URISyntaxException, IOException;
}
