package eu.uneltelibere.scarpa.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.service.EntityDeleteService;
import eu.uneltelibere.scarpa.web.controller.api.EntityDeleteHandler;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;

@Component
public class RoleGroupDeleteHandler extends EntityDeleteHandler<RoleGroup> {

	private static final String MESSAGE_DELETE_FAILED = "message.delete.role.group.failed";
	private static final String MESSAGE_NOT_FOUND = "message.delete.role.group.notfound";
	private static final String MESSAGE_STILL_USED = "message.delete.role.group.still.used";
	private static final String MESSAGE_DELETED = "message.delete.role.group.success";

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Autowired
	@Qualifier("defaultRoleGroupService")
	private EntityDeleteService<RoleGroup> entityService;

	@Override
	protected EntityDeleteService<RoleGroup> entityService() {
		return entityService;
	}

	@Override
	protected String successMessage(RoleGroup entity, HttpServletRequest request) {
		return localizedMessageService.getMessage(MESSAGE_DELETED, new Object[] { entity.getName() }, request);
	}

	@Override
	protected String notFoundMessage(HttpServletRequest request) {
		return localizedMessageService.getMessage(MESSAGE_NOT_FOUND, request);
	}

	@Override
	protected String stillUsedMessage(HttpServletRequest request) {
		return localizedMessageService.getMessage(MESSAGE_STILL_USED, request);
	}

	@Override
	protected String deleteFailedMessage(Throwable e, HttpServletRequest request) {
		return localizedMessageService.getMessage(MESSAGE_DELETE_FAILED, new Object[] { e.getLocalizedMessage() },
				request);
	}

	@Override
	protected String entityUrl() {
		return "/role-group";
	}
}
