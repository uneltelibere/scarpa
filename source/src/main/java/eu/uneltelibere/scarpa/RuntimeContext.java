package eu.uneltelibere.scarpa;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import eu.uneltelibere.scarpa.service.MailContentProvider;
import eu.uneltelibere.scarpa.service.SettingService;

public class RuntimeContext {

	private static final String APP_CONTEXT_SETTING = "scarpa.config.app.context";

	private String appContext = "";

	private List<String> customCss = Collections.emptyList();

	private Map<String, String> dependenciesVersions = Collections.emptyMap();

	private String defaultLocale = "en";

	@Autowired
	private SettingService settingSevice;

	@Autowired
	@Qualifier("scarpaMailContentProvider")
	private MailContentProvider scarpaMailContentProvider;

	private MailContentProvider mailContentProvider;

	public String getAppContext() {
		if (this.appContext.isEmpty()) {
			Optional<String> maybeSetting = settingSevice.get(APP_CONTEXT_SETTING);
			if (maybeSetting.isPresent()) {
				appContext = maybeSetting.get();
			}
		}

		return appContext;
	}

	public void setAppContext(String appContextUrl) {
		if (this.appContext.isEmpty()) {
			settingSevice.set(APP_CONTEXT_SETTING, appContextUrl);
		}

		this.appContext = appContextUrl;
	}

	public List<String> getCustomCss() {
		return customCss;
	}

	public void setCustomCss(List<String> customCss) {
		this.customCss = customCss;
	}

	public Map<String, String> getDependenciesVersions() {
		return dependenciesVersions;
	}

	public void setDependenciesVersions(Map<String, String> dependenciesVersions) {
		this.dependenciesVersions = dependenciesVersions;
	}

	public String getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(String defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public MailContentProvider getMailContentProvider() {
		if (mailContentProvider == null) {
			setMailContentProvider(scarpaMailContentProvider);
		}
		return mailContentProvider;
	}

	public void setMailContentProvider(MailContentProvider mailContentProvider) {
		this.mailContentProvider = mailContentProvider;
	}
}
