package eu.uneltelibere.scarpa.api;

public class Page {

	private final int offset;

	private final int limit;

	private Page(int offset, int limit) {
		this.offset = offset;
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public static Page of(int offset, int limit) {
		return new Page(offset, limit);
	}
}
