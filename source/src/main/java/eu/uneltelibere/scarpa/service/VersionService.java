package eu.uneltelibere.scarpa.service;

public interface VersionService {

	String version();

	String buildDate();
}
