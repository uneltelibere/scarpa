ALTER TABLE user_authority
	DROP CONSTRAINT fk_user_authority_users;

ALTER TABLE user_authority
	ADD CONSTRAINT fk_user_authority_users FOREIGN KEY(user_id)
		REFERENCES users(id)
		ON DELETE CASCADE;
