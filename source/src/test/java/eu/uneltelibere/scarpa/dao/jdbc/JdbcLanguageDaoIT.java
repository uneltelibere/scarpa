package eu.uneltelibere.scarpa.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Optional;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

import eu.uneltelibere.scarpa.dao.LanguageDao;
import eu.uneltelibere.scarpa.model.Language;
import eu.uneltelibere.scarpa.spring.TestConfig;
import eu.uneltelibere.scarpa.test.MyDatabaseOperationLookup;
import eu.uneltelibere.scarpa.test.PostgresContainerRule;

/**
 * Hint: start the container manually to speed up the tests:
 *
 * <pre>
 * docker run --publish-all --rm --name scarpatest ul/scarpatest:latest
 * </pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@DbUnitConfiguration(databaseConnection = {
		"dbUnitDatabaseConnection" }, databaseOperationLookup = MyDatabaseOperationLookup.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@Transactional(transactionManager = "scarpaTransactionManager")
public class JdbcLanguageDaoIT {

	private static final String IMAGE = "ul/scarpatest:latest";

	@ClassRule
	public static PostgresContainerRule dockerRule = new PostgresContainerRule(IMAGE);

	@Autowired
	private LanguageDao languageDao;

	@Test
	@DatabaseSetup(value = "/db/in/language/language_test.xml", type = DatabaseOperation.INSERT)
	public void testGetById() {
		Optional<Language> languageInDb = languageDao.getByCode("test");
		assertEquals(1024, languageInDb.get().getId());
		assertEquals("Test", languageInDb.get().getName());
	}

	@Test
	public void testGetByIdNonexistent() {
		Optional<Language> languageInDb = languageDao.getByCode("foo");
		assertFalse(languageInDb.isPresent());
	}

	@Test
	public void testGetByIdNull() {
		Optional<Language> languageInDb = languageDao.getByCode(null);
		assertFalse(languageInDb.isPresent());
	}
}
