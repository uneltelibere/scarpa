package eu.uneltelibere.scarpa.web.controller.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.uneltelibere.scarpa.model.Entity;

public class SearchResult<T extends Entity> {

	@JsonProperty("search_results")
	private List<T> result;

	private String message;

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
