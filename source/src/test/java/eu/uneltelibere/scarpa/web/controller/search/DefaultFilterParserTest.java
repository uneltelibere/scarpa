package eu.uneltelibere.scarpa.web.controller.search;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.uneltelibere.scarpa.web.controller.search.DefaultFilterParser;
import eu.uneltelibere.scarpa.web.controller.search.FilterParser;
import eu.uneltelibere.scarpa.web.controller.search.TextCondition;

public class DefaultFilterParserTest {

	private FilterParser parser = new DefaultFilterParser();

	@Test
	public void testParseFilterBinaryOperator() {
		TextCondition condition = parser.parseFilter("name like john doe");

		assertEquals("name", condition.getField());
		assertEquals("like", condition.getOperator());
		assertEquals("john doe", condition.getValue());
	}

	@Test
	public void testParseFilterUnaryOperator() {
		TextCondition condition = parser.parseFilter("name any");

		assertEquals("name", condition.getField());
		assertEquals("any", condition.getOperator());
		assertEquals("", condition.getValue());
	}

	@Test
	public void testParseFilterNoOperator() {
		TextCondition condition = parser.parseFilter("name");

		assertEquals("", condition.getField());
		assertEquals("", condition.getOperator());
		assertEquals("", condition.getValue());
	}

	@Test
	public void testParseFilterCompoundFieldName() {
		TextCondition condition = parser.parseFilter("cost-center eq 4");

		assertEquals("cost-center", condition.getField());
		assertEquals("eq", condition.getOperator());
		assertEquals("4", condition.getValue());
	}
}
