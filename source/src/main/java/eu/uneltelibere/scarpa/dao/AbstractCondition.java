package eu.uneltelibere.scarpa.dao;

import eu.uneltelibere.scarpa.filter.Condition;

public class AbstractCondition<T> implements Condition<T> {

	private final Field field;

	private final Operator operator;

	private final Object value;

	public AbstractCondition(Field field, Operator operator, Object value) {
		this.field = field;
		this.operator = operator;
		this.value = value;
	}

	public AbstractCondition(Field field, Operator operator) {
		this(field, operator, null);
	}

	@Override
	public Field getField() {
		return field;
	}

	@Override
	public Operator getOperator() {
		return operator;
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public String toString() {
		return field + " " + operator + " " + value;
	}
}
