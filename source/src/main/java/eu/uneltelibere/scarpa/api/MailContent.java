package eu.uneltelibere.scarpa.api;

import java.util.LinkedList;
import java.util.List;

import org.springframework.core.io.InputStreamSource;

public class MailContent {

	public static class InlineImage {

		private InputStreamSource source;

		private String resourceName = "";

		private String imageContentType = "";

		public InputStreamSource getSource() {
			return source;
		}

		public void setSource(InputStreamSource source) {
			this.source = source;
		}

		public String getResourceName() {
			return resourceName;
		}

		public void setResourceName(String resourceName) {
			this.resourceName = resourceName;
		}

		public String getImageContentType() {
			return imageContentType;
		}

		public void setImageContentType(String imageContentType) {
			this.imageContentType = imageContentType;
		}
	}

	private String htmlBody = "";

	private List<InlineImage> images = new LinkedList<>();

	public String getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public List<InlineImage> getImages() {
		return images;
	}

	public void addImage(InlineImage image) {
		this.images.add(image);
	}
}
