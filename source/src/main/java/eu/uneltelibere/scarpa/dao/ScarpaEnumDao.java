package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.ScarpaEnum;

public interface ScarpaEnumDao<T extends ScarpaEnum> {

	Optional<T> getById(long id);

	Optional<T> getByCode(String code);

	List<T> getAll();
}
