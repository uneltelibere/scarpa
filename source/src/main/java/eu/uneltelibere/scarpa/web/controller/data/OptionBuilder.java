package eu.uneltelibere.scarpa.web.controller.data;

public class OptionBuilder<T> {

	private Option<T> option = new Option<>();

	public OptionBuilder<T> withValue(T value) {
		option.setValue(value);
		return this;
	}

	public OptionBuilder<T> withLabel(String label) {
		option.setLabel(label);
		return this;
	}

	public OptionBuilder<T> withData(String key, Object value) {
		option.getData().put(key, value);
		return this;
	}

	public OptionBuilder<T> withSelected(T selectedValue) {
		option.setSelected((selectedValue.equals(option.getValue())));
		return this;
	}

	public Option<T> build() {
		return option;
	}
}
