package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.UserAdminRole;

@Component
public class UsersMenu extends AbstractMenu{

	public UsersMenu() {
		super("menu.users", "/users", new UserAdminRole());
	}
}
