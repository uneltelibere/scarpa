package eu.uneltelibere.scarpa.dao.util;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface IdDecoder {

	Long id(Object value, Class<? extends Entity> type);

	List<Long> listOfIds(Object value);
}
