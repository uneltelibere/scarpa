(function() {
	$(".scarpa-hover-button").mouseout(function() {
		$(this).children(".scarpa-hover-label").hide();
		var onClass = $(this).data("scarpa-hover-button-on");
		var offClass = $(this).data("scarpa-hover-button-off");
		if (onClass && offClass) {
			$(this).removeClass(onClass);
			$(this).addClass(offClass);
		}
	});
	$(".scarpa-hover-button").mousemove(function() {
		$(this).children(".scarpa-hover-label").show();
		var onClass = $(this).data("scarpa-hover-button-on");
		var offClass = $(this).data("scarpa-hover-button-off");
		if (onClass && offClass) {
			$(this).removeClass(offClass);
			$(this).addClass(onClass);
		}
	});
})();
