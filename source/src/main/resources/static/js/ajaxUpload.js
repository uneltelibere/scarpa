(function() {

	var uploadButton = $('#startButton');
	var uploadProgress = $("#uploadProgress");
	var uploadStatus = $('#loadProgressText');

	var restartButton = $('#restartButton');
	var restartProgress = $('#restartProgress');
	var restartStatus = $('#restartProgressText');

	var pingIntervalId;

	function updateProgressBar(percentComplete) {
		var percentVal = percentComplete + '%';
		$("#loadProgressBar").css('width', percentVal);
		$("#loadProgressBar").attr("aria-valuenow", percentComplete);
		$("#loadProgressBarLabel").html(percentVal);
	}

	$('#installForm').ajaxForm({
		// url : contextPath + 'import',
		dataType : "json",
		beforeSend : function() {
			var percentVal = '0%';
			uploadStatus.empty();
			uploadProgress.show();
			updateProgressBar(0);
			uploadButton.attr("disabled", "disabled");
		},
		uploadProgress : function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			uploadStatus.html("Uploaded " + percentVal);
			updateProgressBar(percentComplete);
		},
		success : function() {
			var percentVal = '100%';
			uploadStatus.html("Uploaded " + percentVal);
			updateProgressBar(100);
			uploadProgress.hide();
		},
		complete : function(jqXHR) {
			var json = jqXHR.responseJSON;
			if (!json.success) {
				uploadStatus.html(json.message);
				uploadButton.removeAttr("disabled");
			} else {
				uploadStatus.html('Done.')

				restartButton.show();
			}
		},
		error : function(jqXHR, errorType, exception) {
			console.log('ajax upload error: ' + errorType);
			uploadStatus.html("Upload failed.");
			uploadProgress.hide();
		}
	});

	$('#restartForm').ajaxForm({
		// url : contextPath + 'import',
		dataType : "json",
		beforeSend : function() {
			restartButton.attr("disabled", "disabled");
			restartProgress.show();
		},
		success : function() {
			restartStatus.html("Restarting...");
		},
		complete : function(jqXHR) {
			var json = jqXHR.responseJSON;
			if (!json.success) {
				restartStatus.html(json.message);
			} else {
				restartStatus.html('Restart requested.')
				pingIntervalId = setInterval(ping, 2000);
			}
		},
		error : function(jqXHR, errorType, exception) {
			console.log('ajax restart error: ' + errorType);
			restartStatus.html("Restart failed.");
			restartProgress.hide();
		}
	});

	var pingFailureCount = 0;
	var maxRetries = 60;

	function stopPinging() {
		clearInterval(pingIntervalId);
		pingFailureCount = 0;
	}

	var ping = function() {
		$.ajax({
			url : contextPath + 'ping',
			type : 'GET',
			dataType : 'text',
			success : function(data) {
				console.log('success');
			},
			complete : function(jqXHR) {
				var pong = jqXHR.responseText;
				console.log('complete, response was [' + pong + '], ping failed ' + pingFailureCount + ' times.')
				if (pong.match("^pong") && pingFailureCount > 0) {
					console.log('back online!');
					stopPinging();
					restartProgress.hide();
					restartStatus.html("Application was successfully restarted.");
				}
			},
			error : function(jqXHR, errorType, exception) {
				console.log('ping failed');
				pingFailureCount++;
				if (pingFailureCount === 1) {
					restartStatus.html("Application was shut down.");
				} else if (pingFailureCount <= maxRetries) {
					restartStatus.html("Waiting for application to come back online...");
				} else  if (pingFailureCount > maxRetries) {
					console.log('Giving up.');
					stopPinging();
					restartStatus.html("Application was shut down, but failed to restart.");
					restartProgress.hide();
				}
			}
		});
	}

})();
