package eu.uneltelibere.scarpa.web.controller;

import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.error;
import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.success;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.api.page.UserPageConfig;
import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.NotFoundException;
import eu.uneltelibere.scarpa.service.TokenService;
import eu.uneltelibere.scarpa.service.UserService;
import eu.uneltelibere.scarpa.service.ValidationException;
import eu.uneltelibere.scarpa.service.user.UserValidationService;
import eu.uneltelibere.scarpa.web.controller.api.IdProvider;
import eu.uneltelibere.scarpa.web.controller.data.AjaxResult;
import eu.uneltelibere.scarpa.web.controller.data.SessionObjectHolder;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityFactory;
import eu.uneltelibere.scarpa.web.controller.data.WebEntityWriter;
import eu.uneltelibere.scarpa.web.controller.data.group.WebUser;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;
import eu.uneltelibere.scarpa.web.menu.UsersMenu;

@Controller
@RequestMapping("/users")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	private static final String MESSAGE_QUESTION_EDIT_NOT_FOUND = "page.user.edit.notfound";

	private static final String MESSAGE_SAVED = "generic.alert.saved";

	private static final String MESSAGE_SAVE_FAILED = "message.user.save.failed";

	@Autowired
	private UserService userService;

	@Autowired
	private UserValidationService userValidationService;

	@Autowired
	private SessionObjectHolder<User, Long> sessionUserHolder;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private MenuHolder menu;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Autowired
	private UserPageConfig userPageConfig;

	@Autowired
	private WebEntityFactory<User, WebUser> webUserFactory;

	@Autowired
	private WebEntityWriter<WebUser> webUserWriter;

	@Autowired
	private IdProvider<User> idProvider;

	@GetMapping(value = { "/new", "/{id}" })
	public String showEditUserPage(@PathVariable("id") Optional<Long> userId, Model model, HttpServletRequest request) {
		long id = userId.orElse(User.NEW_ID);
		LOG.info("Showing details page for user {}", id);
		menu.set(UsersMenu.class, request, model);
		User user = getOrCreateUser(id, request);
		WebUser webUser = webUserFactory.webEntity(user);
		webUser = webUserWriter.write(webUser, request);

		model.addAttribute("user", webUser);
		model.addAttribute("canResetPassword", id != User.NEW_ID);
		return userPageConfig.getEditUserPage(id, model, request);
	}

	@PostMapping(value = "/{id}")
	public String postUser(@PathVariable("id") long id,
			@RequestParam(name = "username", defaultValue = "") String username, @RequestParam("email") String email,
			@RequestParam(name = "groups[]", defaultValue = "") List<Long> groupIDs, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		LOG.info("Received data for user {}", id);

		HttpSession session = request.getSession();
		User user = new User();
		try {
			user = getOrCreateUser(id, request);
			user.setId(id);
			user.setUsername(username);
			user.setEmail(email);

			userValidationService.validate(user);
			user.setId(userService.saveWithoutPassword(user));
			userService.setRoleGroupsForUser(user, groupIDs);
			LOG.debug("Saved groups {} for user {}", groupIDs, user.getId());
			userPageConfig.postSaveUser(user, request);
			success(redirectAttributes, localizedMessageService.getMessage(MESSAGE_SAVED, request));

			sessionUserHolder.clearCurrentObject(session);
		} catch (ValidationException e) {
			LOG.warn("User is not valid: {}", e.getLocalizedMessage());
			error(redirectAttributes, localizedMessageService.getMessage(e.getMessage(), request));

			sessionUserHolder.setCurrentObject(user, id, session);
		} catch (NotFoundException e) {
			LOG.warn("User was not found");
			error(redirectAttributes, localizedMessageService.getMessage(MESSAGE_QUESTION_EDIT_NOT_FOUND, request));
			sessionUserHolder.clearCurrentObject(session);
		} catch (Exception e) {
			LOG.warn("Failed to save user.", e);
			error(redirectAttributes, localizedMessageService.getMessage(MESSAGE_SAVE_FAILED,
					new Object[] { e.getLocalizedMessage() }, request));

			sessionUserHolder.setCurrentObject(user, id, session);
		}

		return "redirect:/users/" + idProvider.id(user);
	}

	private User getOrCreateUser(long id, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Optional<User> maybeCurrentEntity = sessionUserHolder.currentObjectFromSession(id, session);
		if (!maybeCurrentEntity.isPresent() && id != User.NEW_ID) {
			Locale locale = localeResolver.resolveLocale(request);
			maybeCurrentEntity = userService.getById(id, locale);
		}
		if (!maybeCurrentEntity.isPresent() && id != User.NEW_ID) {
			throw new NotFoundException();
		} else {
			return maybeCurrentEntity.orElseGet(User::new);
		}
	}

	@PostMapping(value = "/{id}/resetPassword")
	public @ResponseBody AjaxResult resetPassword(@PathVariable("id") long id, HttpServletRequest request,
			HttpSession session) {
		LOG.info("Password reset was requested for user {}", id);

		Optional<User> maybeUser = userService.getById(id);
		if (maybeUser.isPresent()) {
			User user = maybeUser.get();
			Future<Boolean> resetPasswordSuccess = userService.resetPassword(user);
			session.setAttribute(resetPasswordAttribute(id), resetPasswordSuccess);
			return AjaxResult.OK;
		} else {
			LOG.warn("User {} not found.", id);
			return AjaxResult.FAILURE;
		}
	}

	@GetMapping(value = "/{id}/resetPasswordStatus")
	public @ResponseBody AjaxResult resetPasswordStatus(@PathVariable("id") long id, HttpSession session) {
		LOG.info("Ajax request for reset password status for user {}", id);
		AjaxResult result = new AjaxResult();

		Object sessionValue = session.getAttribute(resetPasswordAttribute(id));
		if (sessionValue instanceof Future) {
			@SuppressWarnings("unchecked")
			Future<Boolean> resetPasswordSuccess = (Future<Boolean>) sessionValue;

			if (resetPasswordSuccess.isDone()) {
				Boolean sent;
				try {
					sent = resetPasswordSuccess.get();
					if (sent) {
						result.setMessage("Email was sent.");
						result.setProgress(AjaxResult.PROGRESS_DONE);
					} else {
						result.setMessage("Email was not sent.");
						result.setSuccess(false);
					}
				} catch (InterruptedException e) {
					String message = "Failed to wait for reset password result";
					LOG.warn(message, e);
					result.setMessage(message);
					result.setSuccess(false);
					// Restore interrupted state
					Thread.currentThread().interrupt();
				} catch (ExecutionException e) {
					String message = "Failed to wait for reset password result";
					LOG.warn(message, e);
					result.setMessage(message);
					result.setSuccess(false);
				}
			} else {
				result.setMessage("Waiting...");
			}
		} else {
			result.setSuccess(false);
			result.setMessage("Password reset was not recently requested for this user.");
		}
		return result;
	}

	@PostMapping(path = "/delete")
	public String delete(@RequestParam("id") long id) {
		LOG.info("Deleting user {}", id);
		tokenService.deleteTokensForUser(id);
		userService.delete(id);
		return "redirect:/users";
	}

	private String resetPasswordAttribute(long userId) {
		return "reset_password_" + userId;
	}
}
