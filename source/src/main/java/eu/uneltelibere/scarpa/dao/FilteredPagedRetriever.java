package eu.uneltelibere.scarpa.dao;

import java.util.Collection;
import java.util.List;

import eu.uneltelibere.scarpa.api.Page;
import eu.uneltelibere.scarpa.filter.ConditionGroup;

public interface FilteredPagedRetriever<T> {

	List<T> retrieveFiltered(ConditionGroup<T> conditions, Page page);

	List<T> retrieveSorted(Collection<SortCriteria<T>> sortCriteria, Page page);

	List<T> retrieveFilteredAndSorted(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria,
			Page page);

	int count(ConditionGroup<T> conditions);
}
