package eu.uneltelibere.scarpa.web.controller.data.group;

import eu.uneltelibere.scarpa.model.RoleGroup;

public class WebRoleGroup extends RoleGroup {

	private static final long serialVersionUID = 1L;

	private String displayName = "";

	private boolean assigned;

	@Override
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isAssigned() {
		return assigned;
	}

	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
}
