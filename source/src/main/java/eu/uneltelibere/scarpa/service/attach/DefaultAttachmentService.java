package eu.uneltelibere.scarpa.service.attach;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.dao.AttachmentDao;
import eu.uneltelibere.scarpa.model.Attachment;
import eu.uneltelibere.scarpa.service.UploadConfigurationService;
import eu.uneltelibere.scarpa.service.fs.FileSystemService;

@Service
public class DefaultAttachmentService implements AttachmentService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAttachmentService.class);

	@Autowired
	private AttachmentDao attachmentDao;

	@Autowired
	private UploadConfigurationService uploadConfigService;

	@Autowired
	private FileSystemService fileSystemService;

	@Override
	public Attachment save(Attachment entity) {
		long id = attachmentDao.create(entity);
		entity.setId(id);

		upload(entity);

		return entity;
	}

	private void upload(Attachment attach) {
		if (attach.getIncomingFile() != null && !attach.getIncomingFile().isEmpty()) {
			try {
				File destination = attachmentPath(attach).toFile();
				if (destination.exists()) {
					LOG.error("{} already exists.", destination);
				} else {
					attach.getIncomingFile().transferTo(destination);

					LOG.info("Uploaded {} bytes for attachment {} to {}.", attach.getIncomingFile().getSize(),
							attach.getId(), destination);
				}
			} catch (Exception e) {
				LOG.error("Upload failed!", e);
			}
		} else {
			LOG.warn("No incoming file in attachment!");
		}
	}

	@Override
	public Optional<Attachment> getById(long id) {
		return attachmentDao.getById(id);
	}

	@Override
	public List<Attachment> retrieve(List<Long> ids) {
		return attachmentDao.retrieve(ids);
	}

	@Override
	public void delete(Attachment entity) {
		attachmentDao.delete(entity.getId());
		removeFile(entity);
	}

	private void removeFile(Attachment attachment) {
		try {
			Path pathToFile = attachmentPath(attachment);
			Files.delete(pathToFile);
		} catch (URISyntaxException | IOException e) {
			LOG.error("Failed to remove file.", e);
		}
	}

	public File file(Attachment attachment) {
		try {
			return attachmentPath(attachment).toFile();
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Path attachmentPath(Attachment attachment) throws URISyntaxException, IOException {
		String uploadDir = uploadConfigService.attachmentUploadDirectory();
		String fileName = attachment.getInternalFilename();
		fileSystemService.ensureDirExists(uploadDir);

		return Paths.get(uploadDir, fileName);
	}
}
