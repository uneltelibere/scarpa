package eu.uneltelibere.scarpa.service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.RuntimeContext;
import eu.uneltelibere.scarpa.dao.TokenDao;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

@Service
public class PasswordResetTokenService implements TokenService {

	private static final Logger LOG = LoggerFactory.getLogger(PasswordResetTokenService.class);

	private static final long VALID_DAYS = 14;

	@Autowired
	private TokenDao tokenDao;

	@Autowired
	private RuntimeContext runtimeContext;

	@Override
	public Token createToken(User user) {
		Token resetToken = new Token();
		resetToken.setUser(user);
		resetToken.setToken(UUID.randomUUID().toString());
		resetToken.setExpiryDate(LocalDateTime.now().plusDays(VALID_DAYS));

		long id = tokenDao.create(resetToken);

		resetToken.setId(id);
		return resetToken;
	}

	@Override
	public Token getToken(String token) {
		Optional<Token> tokenInDb = tokenDao.read(token);
		return tokenInDb.orElseThrow(NotFoundException::new);
	}

	@Override
	public boolean isValid(Token token) {
		return LocalDateTime.now().isBefore(token.getExpiryDate().plusDays(VALID_DAYS));
	}

	@Override
	public void deleteToken(Token token) {
		LOG.info("Deleting token {}", token.getToken());
		tokenDao.deleteToken(token);
	}

	@Override
	public void deleteTokensForUser(long userId) {
		LOG.info("Deleting tokens for {}", userId);
		tokenDao.deleteTokensForUser(userId);
	}

	@Override
	public String tokenUrl(Token token) {
		return runtimeContext.getAppContext() + "/password/" + token.getToken();
	}
}
