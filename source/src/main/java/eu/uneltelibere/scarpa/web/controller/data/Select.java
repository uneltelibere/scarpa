package eu.uneltelibere.scarpa.web.controller.data;

import java.util.LinkedList;
import java.util.List;

public class Select<T> implements Input {

	private List<OptGroup<T>> optGroups = new LinkedList<>();

	private List<Option<T>> options = new LinkedList<>();

	public List<OptGroup<T>> getOptGroups() {
		return optGroups;
	}

	public void setOptGroups(List<OptGroup<T>> optGroups) {
		this.optGroups = optGroups;
	}

	public List<Option<T>> getOptions() {
		return options;
	}

	public void setOptions(List<Option<T>> options) {
		this.options = options;
	}
}
