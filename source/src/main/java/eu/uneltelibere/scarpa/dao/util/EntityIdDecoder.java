package eu.uneltelibere.scarpa.dao.util;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.model.Entity;

@Component
public class EntityIdDecoder implements IdDecoder {

	private static final Logger LOG = LoggerFactory.getLogger(EntityIdDecoder.class);

	private static final Long NO_ID = Entity.NEW_ID;

	@Override
	public Long id(Object value, Class<? extends Entity> type) {
		Long id = NO_ID;
		if (type.isAssignableFrom(value.getClass())) {
			id = type.cast(value).dbRef();
		} else if (value instanceof Long) {
			id = (Long) value;
		} else if (value instanceof Integer) {
			id = Long.valueOf((Integer) value);
		} else {
			LOG.warn("Value {} was not of an expected type: {}.", value, value.getClass().getName());
		}
		return id;
	}

	@Override
	public List<Long> listOfIds(Object value) {
		List<Long> ids = Collections.singletonList(NO_ID);

		if (value instanceof List) {
			List<?> list = (List<?>) value;
			if (!list.isEmpty()) {
				if (Entity.class.isAssignableFrom(list.get(0).getClass())) {
					List<? extends Entity> entityList = (List<? extends Entity>) list;
					ids = entityList.stream().map(Entity::getId).collect(toList());
				} else if (Long.class.isAssignableFrom(list.get(0).getClass())) {
					ids = (List<Long>) list;
				}
			}
		} else if (Entity.class.isAssignableFrom(value.getClass())) {
			Entity singleEntity = (Entity) value;
			ids = Collections.singletonList(singleEntity.dbRef());
		} else if (value instanceof Long) {
			ids = Collections.singletonList((Long) value);
		} else if (value instanceof Integer) {
			ids = Collections.singletonList(Long.valueOf((Integer) value));
		} else {
			LOG.warn("Value {} was not of an expected type: {}.", value, value.getClass().getName());
		}
		return ids;
	}
}
