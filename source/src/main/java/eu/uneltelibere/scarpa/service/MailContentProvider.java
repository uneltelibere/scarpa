package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.api.MailContent;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

public interface MailContentProvider {

	/**
	 * The content for the welcome mail to be sent when the account is created by an
	 * administrator. Includes a token for setting the initial password.
	 * 
	 * @param token the token for setting the password
	 * @return the welcome mail content, with token
	 */
	MailContent welcomeMailContent(Token token);

	/**
	 * The content for the welcome mail to be sent when the account is created
	 * (registered) by the user themselves. Does not need to include a token for
	 * setting the initial password.
	 * 
	 * @param user the user to welcome
	 * @return the welcome mail content, without token
	 */
	MailContent welcomeMailContent(User user);

	MailContent recoveryMailContent(Token token);
}
