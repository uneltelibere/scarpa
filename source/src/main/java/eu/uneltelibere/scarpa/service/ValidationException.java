package eu.uneltelibere.scarpa.service;

@SuppressWarnings("serial")
public class ValidationException extends RuntimeException {

	private Object[] messageArguments = new Object[] {};

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(String message, Object... messageArguments) {
		super(message);
		this.messageArguments = messageArguments;
	}

	public Object[] getMessageArguments() {
		return messageArguments;
	}
}
