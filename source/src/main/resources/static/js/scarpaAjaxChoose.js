jQuery(document).ready(
		function() {

			var labelToId = {};

			var addTypeAhead = function(typeAheadElement) {
				console.log('Configuring typeahead for '
						+ typeAheadElement.attr('id'));
				typeAheadElement.typeahead({
					source : function(query, process) {
						url = typeAheadElement.attr('data-source') + query;
						$.get(url, function(json) {
							labels = [];
							$.each(json.search_results, function(i, element) {
								labelToId[element.label] = element.id;
								labels.push(element.label);
							});
							process(labels);
							if (labels.length === 0) {
								markWarning(typeAheadElement, json.message);
							}
						});
					}
				});
			}

			$('[data-role=typeahead-input]').change(function(event) {
				var chosenLabel = $(this).val();
				var selectedId = $(this).siblings('[data-role=selectedId]');
				var defaultValue = selectedId.attr('data-default-value');
				if (chosenLabel) {
					var chosenId = labelToId[chosenLabel];
					if (chosenId === undefined) {
						selectedId.val(defaultValue).trigger('change');
						markError($(this));
					} else {
						selectedId.val(chosenId).trigger('change');
						markSuccess($(this));
					}
				} else {
					selectedId.val(defaultValue).trigger('change');
					markSuccess($(this));
				}
			});

			var markError = function(input) {
				formGroup = input.closest('.form-group');
				if (formGroup.hasClass("has-warning")) {
					formGroup.removeClass("has-warning");
				}
				if (!formGroup.hasClass("has-error")) {
					formGroup.addClass("has-error");
				}
				if (!formGroup.hasClass("has-feedback")) {
					formGroup.addClass("has-feedback");
				}
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="error"]')
						.show();
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="warning"]')
						.hide();
				input.parent().find('.help-block').show();

				var submit = input.closest('form').find(':submit');
				submit.attr("disabled", true);
			}

			var markWarning = function(input, message) {
				formGroup = input.closest('.form-group');
				if (!formGroup.hasClass("has-warning")) {
					formGroup.addClass("has-warning");
				}
				if (formGroup.hasClass("has-error")) {
					formGroup.removeClass("has-error");
				}
				if (!formGroup.hasClass("has-feedback")) {
					formGroup.addClass("has-feedback");
				}
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="warning"]')
						.show();
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="error"]')
						.hide();
				var helpBlock = input.parent().find('.help-block');
				if (message !== undefined) {
					helpBlock.html(message)
					helpBlock.show();
				}

				var submit = input.closest('form').find(':submit');
				submit.attr("disabled", false);
			}

			var markSuccess = function(input) {
				formGroup = input.closest('.form-group');
				if (formGroup.hasClass("has-warning")) {
					formGroup.removeClass("has-warning");
				}
				if (formGroup.hasClass("has-error")) {
					formGroup.removeClass("has-error");
				}
				if (formGroup.hasClass("has-feedback")) {
					formGroup.removeClass("has-feedback");
				}
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="warning"]')
						.hide();
				input.parent().find(
						'.form-control-feedback[data-showOnStatus="error"]')
						.hide();
				input.parent().find('.help-block').hide();

				var submit = input.closest('form').find(':submit');
				submit.attr("disabled", false);
			}

			$('[data-role=typeahead-input]').each(function(index, element) {
				addTypeAhead($(element));
				markSuccess($(element));
			});
		});
