ALTER TABLE translation_text
	DROP CONSTRAINT translation_text_translation_id_fkey,
	ADD CONSTRAINT translation_text_translation_id_fkey
		FOREIGN KEY(translation_id)
		REFERENCES translation(id)
		ON DELETE CASCADE;
