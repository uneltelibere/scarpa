package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.web.controller.data.Input;
import eu.uneltelibere.scarpa.web.controller.data.Text;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;

public abstract class AbstractTableColumnFactory<T extends Entity> implements TableColumnFactory<T> {

	private static final String EMPTY_STRING = "";

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Override
	public boolean canHandle(String code) {
		return getCode().equals(code);
	}

	protected abstract String getCode();

	protected String getTitle() {
		return EMPTY_STRING;
	}

	protected abstract String getLabel();

	@Override
	public TableColumn<T> column(TableColumnSource<T> tableColumnSource) {
		List<? extends T> entities = tableColumnSource.getEntities();
		HttpServletRequest request = tableColumnSource.getRequest();
		boolean noSearch = tableColumnSource.isNoSearchAtAll() || tableColumnSource.isNoSearch(getCode());
		TableColumn.Heading heading = columnHeading(request);
		return new TableColumnBuilder<T>().heading(heading).valueText(columnContent(request))
				.detailedValueTexts(columnDetailedContent(request)).valueEntities(entities).filterType(filterType())
				.filterInput(filterInput(heading, request)).noSearch(noSearch).build();
	}

	@Override
	public TableColumn.Heading columnHeading(HttpServletRequest request) {
		TableColumn.Heading heading = new TableColumn.Heading();

		heading.setCode(getCode());

		String label = localizedMessageService.getMessage(getLabel(), request);
		heading.setText(label);

		String titleCode = getTitle();
		if (titleCode.length() > 0) {
			String title = localizedMessageService.getMessage(getTitle(), request);
			heading.setAbbreviationTitle(title);
		}

		return heading;
	}

	protected abstract Function<T, String> columnContent(HttpServletRequest request);

	protected Function<T, List<String>> columnDetailedContent(HttpServletRequest request) {
		return entity -> Collections.singletonList(columnContent(request).apply(entity));
	}

	protected TableColumnFilterType filterType() {
		return TableColumnFilterType.TEXT;
	}

	protected Input filterInput(TableColumn.Heading heading, HttpServletRequest request) {
		Text input = new Text();
		input.setPlaceholder(heading.getText());
		return input;
	}
}
