package eu.uneltelibere.scarpa.web.controller.search;

import java.util.Collection;
import java.util.List;

import eu.uneltelibere.scarpa.filter.ConditionGroup;

public interface ConditionFactory<T> {

	ConditionGroup<T> conditions(List<String> filters);

	ConditionGroup<T> conditions(Collection<TextCondition> textConditions);
}
