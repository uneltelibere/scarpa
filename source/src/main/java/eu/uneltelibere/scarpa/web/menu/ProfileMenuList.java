package eu.uneltelibere.scarpa.web.menu;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.api.OrderedMenu;

@Component
public class ProfileMenuList implements OrderedMenu {

	@Autowired
	private Menu myAccountMenu;

	@Autowired(required = false)
	private ProfileMenuListExtender profileMenuListExtender;

	public List<Menu> menus() {
		List<Menu> profileMenus = new LinkedList<>();
		if (profileMenuListExtender != null) {
			profileMenus.addAll(profileMenuListExtender.menusBefore());
		}
		profileMenus.add(myAccountMenu);
		if (profileMenuListExtender != null) {
			profileMenus.addAll(profileMenuListExtender.menusAfter());
		}
		return profileMenus;
	}
}
