package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.dao.TranslationDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.TranslationText;

@Service
public class DefaultRoleService implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private TranslationDao translationDao;

	@Autowired
	private TranslationService translationService;

	@Override
	public List<Role> retrieve(Locale locale) {
		List<Role> allRoles = roleDao.getRoles();
		for (Role role : allRoles) {
			role = roleWithTranslatedDescription(role, locale);
		}
		return allRoles;
	}

	@Override
	public List<Role> retrieve(RoleGroup group, Locale locale) {
		List<Role> allRoles = roleDao.getRolesInGroup(group);
		for (Role role : allRoles) {
			role = roleWithTranslatedDescription(role, locale);
		}
		return allRoles;
	}

	@Override
	public Optional<Role> retrieve(long id, Locale locale) {
		return roleDao.getRole(id).map(role -> roleWithTranslatedDescription(role, locale));
	}

	private Role roleWithTranslatedDescription(Role role, Locale locale) {
		Translation translatedDescription = role.getTranslatedDescription();
		translationService.retrieve(translatedDescription.getId()).ifPresent(t -> {
			role.setDescription(translationService.translatedText(t, locale));
		});
		return role;
	}

	@Override
	@Transactional(transactionManager = "masterTransactionManager")
	public Role save(Role role, Locale locale) {
		Translation descriptionTranslation = role.getTranslatedDescription();
		descriptionTranslation = translationService.retrieve(descriptionTranslation.getId())
				.orElse(descriptionTranslation);
		translationService.set(descriptionTranslation, role.getDescription(), locale);
		role.setTranslatedDescription(descriptionTranslation);

		String langCode = locale.getLanguage();
		roleDao.update(role);
		for (TranslationText translationText : role.getTranslatedDescription().getTranslationTexts()) {
			if (translationText.getLanguage().getCode().equals(langCode)) {
				translationText.setTranslatedText(role.getDescription());
				translationDao.update(translationText);
			}
		}

		return role;
	}
}
