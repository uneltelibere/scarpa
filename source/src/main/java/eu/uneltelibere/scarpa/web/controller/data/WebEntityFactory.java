package eu.uneltelibere.scarpa.web.controller.data;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface WebEntityFactory<T extends Entity, W extends T> {

	W webEntity(T entity);

	List<W> webEntities(List<T> entities);
}
