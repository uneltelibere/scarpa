package eu.uneltelibere.scarpa.dao;

import java.util.Optional;

import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.TranslationText;

public interface TranslationDao {

	long create(Translation translation);

	Optional<Translation> retrieve(long id);

	void update(Translation translation);

	long create(TranslationText translationText);

	void update(TranslationText translationText);
}
