package eu.uneltelibere.scarpa.dao;

public interface SortCriteria<T> {

	public interface Field {
	}

	public enum Order {
		ASC, DESC
	}

	Field getField();

	Order getOrder();
}
