package eu.uneltelibere.scarpa.filter;

import eu.uneltelibere.scarpa.dao.sql.SqlCondition;

public interface SqlConditionFactory {

	SqlCondition sqlCondition(ConditionGroup<?> conditionGroup);
}
