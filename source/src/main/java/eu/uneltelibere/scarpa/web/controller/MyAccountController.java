package eu.uneltelibere.scarpa.web.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.PasswordService;
import eu.uneltelibere.scarpa.service.UserService;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;
import eu.uneltelibere.scarpa.web.menu.MyAccountMenu;

@Controller
@RequestMapping("/me")
public class MyAccountController {

	private static final Logger LOG = LoggerFactory.getLogger(MyAccountController.class);

	@Autowired
	@Qualifier("profileMenuHolder")
	private MenuHolder menu;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordService passwordService;

	@Autowired
	private LocaleResolver localeResolver;

	@GetMapping
	public String listUser(Model model, HttpServletRequest request) {
		LOG.info("Showing the MyAccount page.");
		menu.set(MyAccountMenu.class, request, model);
		model.addAttribute("user", currentUser(request));
		model.addAttribute("passwordMinLen", passwordService.minPasswordLength());
		return "myAccount";
	}

	@PostMapping
	public String postUser(@RequestParam("email") String email,
			@RequestParam(value = "password", defaultValue = "") String password,
			@RequestParam(value = "passwordAgain", defaultValue = "") String passwordAgain, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		User user = currentUser(request);

		user.setEmail(email);

		try {
			changePassword(user, password, passwordAgain);
			userService.save(user);
			redirectAttributes.addFlashAttribute("alert", "success");
			redirectAttributes.addFlashAttribute("alertMessage", "Saved.");
		} catch (FormException e) {
			LOG.warn("Failed to change password for user " + user, e);
			redirectAttributes.addFlashAttribute("alert", "warning");
			redirectAttributes.addFlashAttribute("alertMessage", e.getMessage());
		}

		return "redirect:/me";
	}

	private void changePassword(User user, String password, String passwordAgain) {
		if (!password.isEmpty()) {
			if (!passwordService.isValidPassword(password)) {
				throw new FormException("The password is not valid.");
			}
			if (!password.equals(passwordAgain)) {
				throw new FormException("Passwords did not match.");
			}
			LOG.info("Changing password for {}", user.getUsername());
			user.setPassword(passwordService.encodePassword(password));
		} else {
			LOG.info("Not changing password for {}", user);
		}
	}

	private User currentUser(HttpServletRequest request) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
		Locale locale = localeResolver.resolveLocale(request);
		return userService.getUser(name, locale).orElseGet(User::new);
	}
}
