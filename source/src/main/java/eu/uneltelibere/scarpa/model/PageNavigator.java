package eu.uneltelibere.scarpa.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PageNavigator {

	public static class Button {

		private boolean enabled = true;

		private boolean active;

		private String label = "1";

		private int targetPage = 1;

		public Button() {
		}

		public Button(Button other) {
			setEnabled(other.isEnabled());
			setActive(other.isActive());
			setLabel(other.getLabel());
			setTargetPage(other.getTargetPage());
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public int getTargetPage() {
			return targetPage;
		}

		public void setTargetPage(int targetPage) {
			this.targetPage = targetPage;
		}

		public String toString() {
			if (isActive()) {
				return "[" + getLabel() + "]";
			} else {
				return getLabel();
			}
		}
	}

	private List<Button> buttons = new ArrayList<>();

	private Button first = new Button();

	private Button last = new Button();

	private String basePageUrl = "/";

	public void addButton(Button button) {
		buttons.add(button);
		if (buttons.size() == 1) {
			first = new Button(button);
		}
		last = new Button(button);
	}

	public List<Button> getButtons() {
		return Collections.unmodifiableList(buttons);
	}

	public Button getFirst() {
		return first;
	}

	public Button getLast() {
		return last;
	}

	public String getBasePageUrl() {
		return basePageUrl;
	}

	public void setBasePageUrl(final String basePageUrl) {
		/*
		 * If the provided URL starts with a slash, remove it and leave it to the view
		 * to build the absolute URL.
		 */
		String relativeUrl = basePageUrl;
		if (basePageUrl.startsWith("/")) {
			relativeUrl = basePageUrl.substring(1);
		}
		this.basePageUrl = relativeUrl;
	}

	@Override
	public String toString() {
		return buttons.toString();
	}
}
