package eu.uneltelibere.scarpa.dao;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface AggregateDao<T extends Entity> {

	void removePartsFromWhole(final T whole, List<Long> idsOfPartsToRemove);

	void addPartsToWhole(final T whole, List<Long> idsOfPartsToAdd);

	List<Long> readPartIds(final T whole);
}
