package eu.uneltelibere.scarpa.service.search;

import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.AbstractQuickSearchService;

@Service
public class UserQuickSearchService extends AbstractQuickSearchService<User> {

	@Override
	public String label(User user) {
		return String.format("%s", user.getUsername());
	}
}
