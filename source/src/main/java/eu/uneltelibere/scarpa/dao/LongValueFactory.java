package eu.uneltelibere.scarpa.dao;

public class LongValueFactory implements ValueFactory {

	@Override
	public Object createValue(String valueAsString) {
		return Long.parseLong(valueAsString);
	}
}
