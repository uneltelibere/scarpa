package eu.uneltelibere.scarpa.service.poi;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

public class CellValueHelper {

	public static long longValue(Row row, int column) {
		Cell cell = row.getCell(column, MissingCellPolicy.CREATE_NULL_AS_BLANK);
		switch (cell.getCellTypeEnum()) {
		case NUMERIC:
			double numericValue = cell.getNumericCellValue();
			return (long) numericValue;
		case STRING:
			String stringValue = cell.getStringCellValue();
			try {
				Number number = NumberFormat.getNumberInstance(Locale.US).parse(stringValue);
				return number.longValue();
			} catch (ParseException pe) {
				throw new CellException(stringValue + " doesn't look like a number.");
			}
		case BLANK:
			throw new CellException("Blank cell at " + cellTitle(row, column) + ".");
		default:
			String message = MessageFormat.format("Can''t read cell {0} on row {1}. Type is {2}, I was expecting {3}.",
					column, row.getRowNum(), cellTypeName(cell), "numeric");
			throw new CellException(message);
		}
	}

	public static String stringValue(Row row, int column) {
		Cell cell = row.getCell(column, MissingCellPolicy.CREATE_NULL_AS_BLANK);
		switch (cell.getCellTypeEnum()) {
		case STRING:
		case BLANK:
			return cell.getStringCellValue();
		case NUMERIC:
			double numericValue = cell.getNumericCellValue();
			return String.valueOf((long) numericValue);
		default:
			String message = MessageFormat.format("Can''t read cell {0} on row {1}. Type is {2}, I was expecting {3}.",
					column, row.getRowNum(), cellTypeName(cell), "string");
			throw new CellException(message);
		}
	}

	private static String cellTypeName(Cell cell) {
		switch (cell.getCellTypeEnum()) {
		case NUMERIC:
			return "numeric";
		case STRING:
			return "string";
		case FORMULA:
			return "formula";
		case BLANK:
			return "blank";
		case BOOLEAN:
			return "boolean";
		case ERROR:
			return "error";
		default:
			return "unknown";
		}
	}

	public static String cellTitle(Row row, int column) {
		return cellTitle(row.getRowNum(), column);
	}

	public static String cellTitle(int row, int column) {
		if (column > 0 && column < (int) 'Z' - (int) 'A') {
			char columnTitle = (char) ('A' + column);
			return Character.toString(columnTitle) + (row + 1);
		}
		return "(" + row + ", " + column + ")";
	}
}
