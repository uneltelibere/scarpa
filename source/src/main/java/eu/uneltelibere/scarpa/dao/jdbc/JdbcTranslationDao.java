package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.TranslationDao;
import eu.uneltelibere.scarpa.model.Language;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.TranslationText;

@Repository
public class JdbcTranslationDao extends JdbcMasterDao implements TranslationDao {

	// @formatter:off
	private static final String CREATE_TRANSLATION = "INSERT INTO translation ("
			+ "neutral_text"
			+ ") VALUES ("
			+ ":neutralText)";

	private static final String GET_TRANSLATION = "SELECT "
			+ "t.id, t.neutral_text, "
			+ "tt.id AS translation_text_id, tt.translated_text,"
			+ "l.id AS lang_id, l.code AS lang_code, l.name AS lang_name "
			+ "FROM translation t "
			+ "LEFT JOIN translation_text tt ON tt.translation_id = t.id "
			+ "LEFT JOIN language l ON l.id = tt.language_id "
			+ "WHERE t.id = :id";

	private static final String UPDATE_TRANSLATION = "UPDATE translation SET "
			+ "neutral_text = :neutralText";

	private static final String CREATE_TEXT = "INSERT INTO translation_text ("
			+ "translation_id, language_id, translated_text"
			+ ") VALUES ("
			+ ":translationId, :languageId, :translatedText)";

	private static final String UPDATE_TEXT = "UPDATE translation_text SET "
			+ "translated_text = :translatedText "
			+ "WHERE id = :id";
	// @formatter:on

	@Override
	public long create(Translation translation) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("neutralText", translation.getNeutralText());
		jdbcTemplate.update(CREATE_TRANSLATION, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public Optional<Translation> retrieve(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		return jdbcTemplate.query(GET_TRANSLATION, params, new ResultSetExtractor<Optional<Translation>>() {

			@Override
			public Optional<Translation> extractData(ResultSet rs) throws SQLException, DataAccessException {
				Translation translation = null;
				List<TranslationText> translationTexts = new LinkedList<>();
				while (rs.next()) {
					if (rs.isFirst()) {
						translation = new Translation();
						translation.setId(rs.getInt("id"));
						translation.setNeutralText(rs.getString("neutral_text"));
						translation.setTranslationTexts(translationTexts);
					}
					Integer translationTextId = rs.getObject("translation_text_id", Integer.class);
					if (translationTextId != null) {
						Language language = new Language();
						language.setId(rs.getInt("lang_id"));
						language.setCode(rs.getString("lang_code"));
						language.setName(rs.getString("lang_name"));

						TranslationText translationText = new TranslationText();
						translationText.setId(translationTextId);
						translationText.setTranslation(translation);
						translationText.setTranslatedText(rs.getString("translated_text"));
						translationText.setLanguage(language);
						translationTexts.add(translationText);
					}
				}
				return Optional.ofNullable(translation);
			}
		});
	}

	@Override
	public void update(Translation translation) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", translation.getId());
		params.addValue("neutralText", translation.getNeutralText());
		jdbcTemplate.update(UPDATE_TRANSLATION, params);
	}

	@Override
	public long create(TranslationText text) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("translationId", text.getTranslation().dbRef());
		params.addValue("languageId", text.getLanguage().dbRef());
		params.addValue("translatedText", text.getTranslatedText());
		jdbcTemplate.update(CREATE_TEXT, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public void update(TranslationText text) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", text.getId());
		params.addValue("translatedText", text.getTranslatedText());
		jdbcTemplate.update(UPDATE_TEXT, params);
	}
}
