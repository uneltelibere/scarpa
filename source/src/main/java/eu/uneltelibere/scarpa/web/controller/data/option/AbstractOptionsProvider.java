package eu.uneltelibere.scarpa.web.controller.data.option;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.ScarpaEnumDao;
import eu.uneltelibere.scarpa.model.ScarpaEnum;
import eu.uneltelibere.scarpa.web.controller.data.Option;
import eu.uneltelibere.scarpa.web.controller.data.OptionBuilder;
import eu.uneltelibere.scarpa.web.controller.data.Select;
import eu.uneltelibere.scarpa.web.controller.i18n.LocalizedMessageService;

public abstract class AbstractOptionsProvider<T extends ScarpaEnum> implements EnumOptionsProvider<T> {

	private static final int VALUE_ANY = -1;

	@Autowired
	private ScarpaEnumDao<T> enumDao;

	@Autowired
	private LocalizedMessageService localizedMessageService;

	@Override
	public Select<Long> enumOptions(HttpServletRequest request, long selectedEnumId) {
		return enumOptions(request, selectedEnumId, new DefaultOptionsLabelProvider());
	}

	@Override
	public Select<Long> enumOptions(HttpServletRequest request, long selectedEnumId,
			OptionsLabelProvider optionsLabelProvider) {
		Select<Long> select = new Select<>();

		String labelAny = localizedMessageService.getMessage(optionsLabelProvider.labelAny(), request);
		Option<Long> any = new OptionBuilder<Long>().withLabel(labelAny).withValue(Long.valueOf(VALUE_ANY))
				.withSelected(selectedEnumId).build();
		select.getOptions().add(any);

		List<T> items = enumDao.getAll();
		for (T item : items) {
			String label = optionsLabelProvider.labelFormat(item.getName(), request);
			Option<Long> option = new OptionBuilder<Long>().withLabel(label).withValue(Long.valueOf(item.getId()))
					.withSelected(selectedEnumId).build();
			select.getOptions().add(option);
		}

		return select;
	}
}
