package eu.uneltelibere.scarpa.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class AbstractMasterPagedRetriever<T> extends AbstractPagedRetriever<T> {

	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("masterDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	protected NamedParameterJdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
}
