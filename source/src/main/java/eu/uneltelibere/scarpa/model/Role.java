package eu.uneltelibere.scarpa.model;

public class Role extends Entity {

	private static final long serialVersionUID = 1L;

	private String code = "";

	private String name = "";

	private String description = "";

	private Translation translatedDescription = new Translation();

	private boolean assigned;

	public static class Builder {

		private Role role = new Role();

		public Builder id(long id) {
			role.setId(id);
			return this;
		}

		public Builder code(String code) {
			role.setCode(code);
			return this;
		}

		public Role build() {
			return role;
		}
	}

	@Override
	public String toString() {
		return "Role [name=" + getName() + "]";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Translation getTranslatedDescription() {
		return translatedDescription;
	}

	public void setTranslatedDescription(Translation translatedDescription) {
		this.translatedDescription = translatedDescription;
	}

	public boolean isAssigned() {
		return assigned;
	}

	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
}
