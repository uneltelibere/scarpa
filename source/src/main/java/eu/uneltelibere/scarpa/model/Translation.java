package eu.uneltelibere.scarpa.model;

import java.util.LinkedList;
import java.util.List;

public class Translation extends Entity {

	private static final long serialVersionUID = 1L;

	private String neutralText = "";

	private List<TranslationText> translationTexts = new LinkedList<>();

	public String getNeutralText() {
		return neutralText;
	}

	public void setNeutralText(String neutralText) {
		this.neutralText = neutralText;
	}

	public List<TranslationText> getTranslationTexts() {
		return translationTexts;
	}

	public void setTranslationTexts(List<TranslationText> translationTexts) {
		this.translationTexts = translationTexts;
	}
}
