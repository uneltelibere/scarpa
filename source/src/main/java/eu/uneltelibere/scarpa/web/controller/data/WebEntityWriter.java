package eu.uneltelibere.scarpa.web.controller.data;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.model.Entity;

public interface WebEntityWriter<T extends Entity> {

	T write(T entity);

	List<T> write(List<T> entities);

	T write(T entity, HttpServletRequest request);

	List<T> write(List<T> entities, HttpServletRequest request);
}
