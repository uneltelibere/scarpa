package eu.uneltelibere.scarpa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.dao.SettingDao;
import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;

@Service
public class DefaultSettingService implements SettingService {

	@Autowired
	private SettingDao settingDao;

	@Override
	public void set(String key, String value) {
		Optional<Setting> maybeSettingInDb = settingDao.retrieve(key);
		if (maybeSettingInDb.isPresent()) {
			Setting settingInDb = maybeSettingInDb.get();
			settingInDb.setValue(value);
			settingDao.update(settingInDb);
		} else {
			Setting setting = new Setting.Builder().key(key).value(value).build();
			settingDao.create(setting);
		}
	}

	@Override
	public void set(String key, String value, User user) {
		Optional<Setting> maybeSettingInDb = settingDao.retrieve(key, user);
		if (maybeSettingInDb.isPresent()) {
			Setting settingInDb = maybeSettingInDb.get();
			settingInDb.setValue(value);
			settingDao.update(settingInDb, user);
		} else {
			Setting setting = new Setting.Builder().key(key).value(value).build();
			settingDao.create(setting, user);
		}
	}

	@Override
	public Optional<String> get(String key) {
		Optional<Setting> setting = settingDao.retrieve(key);
		if (setting.isPresent()) {
			String value = setting.get().getValue();
			return Optional.ofNullable(value);
		}
		return Optional.empty();
	}

	@Override
	public List<Setting> getFromGroup(String keyPrefix) {
		return settingDao.retrieveFromGroup(keyPrefix);
	}

	@Override
	public Optional<String> get(String key, User user) {
		Optional<Setting> setting = settingDao.retrieve(key, user);
		if (setting.isPresent()) {
			String value = setting.get().getValue();
			return Optional.ofNullable(value);
		}
		return Optional.empty();
	}
}
