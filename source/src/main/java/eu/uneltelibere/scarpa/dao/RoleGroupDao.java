package eu.uneltelibere.scarpa.dao;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.User;

public interface RoleGroupDao {

	long create(RoleGroup roleGroup);

	List<RoleGroup> retrieve();

	Optional<RoleGroup> retrieve(long id);

	Optional<RoleGroup> retrieveByCode(String code);

	List<RoleGroup> getRoleGroupsOfUser(User user);

	void update(RoleGroup roleGroup);

	void delete(long id);

	void removeRolesFromGroup(RoleGroup group, List<Long> roleIDsToRemove);

	void addRolesToGroup(RoleGroup group, List<Long> roleIDsToAdd);
}
