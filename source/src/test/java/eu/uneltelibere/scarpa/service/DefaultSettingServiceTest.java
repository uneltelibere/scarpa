package eu.uneltelibere.scarpa.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.dao.SettingDao;
import eu.uneltelibere.scarpa.model.Setting;
import eu.uneltelibere.scarpa.model.User;

public class DefaultSettingServiceTest {

	private static final long ID = 123;
	private static final String KEY_PREFIX = "test.setting";
	private static final String KEY = "test.setting.key";
	private static final String VALUE = "42";
	private static final long USER_ID = 321;

	@Captor
	private ArgumentCaptor<Setting> captor;

	@InjectMocks
	private DefaultSettingService settingService;

	@Mock
	private SettingDao settingDao;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateGeneralSetting() {
		when(settingDao.retrieve(KEY)).thenReturn(Optional.empty());

		settingService.set(KEY, VALUE);

		verify(settingDao).retrieve(KEY);
		verify(settingDao).create(captor.capture());
		assertEquals(KEY, captor.getValue().getKey());
		assertEquals(VALUE, captor.getValue().getValue());
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testUpdateGeneralSetting() {
		Setting settingInDb = new Setting.Builder().id(ID).key(KEY).value(VALUE).build();
		when(settingDao.retrieve(KEY)).thenReturn(Optional.of(settingInDb));

		settingService.set(KEY, VALUE);

		verify(settingDao).retrieve(KEY);
		verify(settingDao).update(settingInDb);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testCreateUserSetting() {
		User user = new User.Builder().id(USER_ID).build();
		when(settingDao.retrieve(KEY, user)).thenReturn(Optional.empty());

		settingService.set(KEY, VALUE, user);

		verify(settingDao).retrieve(KEY, user);
		verify(settingDao).create(captor.capture(), eq(user));
		assertEquals(KEY, captor.getValue().getKey());
		assertEquals(VALUE, captor.getValue().getValue());
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testUpdateUserSetting() {
		User user = new User.Builder().id(USER_ID).build();
		Setting settingInDb = new Setting.Builder().id(ID).key(KEY).value(VALUE).build();
		when(settingDao.retrieve(KEY, user)).thenReturn(Optional.of(settingInDb));

		settingService.set(KEY, VALUE, user);

		verify(settingDao).retrieve(KEY, user);
		verify(settingDao).update(settingInDb, user);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testGetNonExistingGeneralSetting() {
		when(settingDao.retrieve(KEY)).thenReturn(Optional.empty());

		Optional<String> maybeSettingValue = settingService.get(KEY);

		assertFalse(maybeSettingValue.isPresent());
		verify(settingDao).retrieve(KEY);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testGetExistingGeneralSetting() {
		Setting settingInDb = new Setting.Builder().key(KEY).value(VALUE).build();
		when(settingDao.retrieve(KEY)).thenReturn(Optional.of(settingInDb));

		Optional<String> maybeSettingValue = settingService.get(KEY);

		assertTrue(maybeSettingValue.isPresent());
		assertEquals(VALUE, maybeSettingValue.get());
		verify(settingDao).retrieve(KEY);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testGetExistingGroupSettings() {
		Setting settingInDb = new Setting.Builder().key(KEY).value(VALUE).build();
		List<Setting> settingsInDb = Collections.singletonList(settingInDb);
		when(settingDao.retrieveFromGroup(KEY_PREFIX)).thenReturn(settingsInDb);

		List<Setting> settings = settingService.getFromGroup(KEY_PREFIX);

		assertThat(settings, is(settingsInDb));
		verify(settingDao).retrieveFromGroup(KEY_PREFIX);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testGetNonExistingUserSetting() {
		User user = new User.Builder().id(USER_ID).build();
		when(settingDao.retrieve(KEY, user)).thenReturn(Optional.empty());

		Optional<String> maybeSettingValue = settingService.get(KEY, user);

		assertFalse(maybeSettingValue.isPresent());
		verify(settingDao).retrieve(KEY, user);
		verifyNoMoreInteractions(settingDao);
	}

	@Test
	public void testGetExistingUserSetting() {
		User user = new User.Builder().id(USER_ID).build();
		Setting settingInDb = new Setting.Builder().key(KEY).value(VALUE).build();
		when(settingDao.retrieve(KEY, user)).thenReturn(Optional.of(settingInDb));

		Optional<String> maybeSettingValue = settingService.get(KEY, user);

		assertTrue(maybeSettingValue.isPresent());
		assertEquals(VALUE, maybeSettingValue.get());
		verify(settingDao).retrieve(KEY, user);
		verifyNoMoreInteractions(settingDao);
	}
}
