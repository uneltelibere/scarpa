CREATE TABLE IF NOT EXISTS static_page (
	id SERIAL PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS static_page_version (
	id SERIAL PRIMARY KEY NOT NULL,
	static_page_id INT NOT NULL REFERENCES static_page(id) ON DELETE CASCADE,
	content TEXT,
	version_number INT NOT NULL CONSTRAINT positive_version_number CHECK (version_number > 0),
	author_user_id INT REFERENCES users(id) ON DELETE SET NULL,
	comment_text TEXT,
	creation_date TIMESTAMP NOT NULL DEFAULT now(),
	CONSTRAINT version_number_unique_per_page UNIQUE(static_page_id, version_number)
);
