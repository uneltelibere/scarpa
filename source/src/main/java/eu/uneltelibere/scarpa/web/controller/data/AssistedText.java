package eu.uneltelibere.scarpa.web.controller.data;

public class AssistedText extends Text {

	private String dataSource = "";

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
}
