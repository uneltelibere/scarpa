package eu.uneltelibere.scarpa;

import eu.uneltelibere.scarpa.api.AbstractRole;

public class UserRole extends AbstractRole {

	public UserRole() {
		super("USER");
	}
}
