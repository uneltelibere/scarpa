package eu.uneltelibere.scarpa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.dao.TranslationDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.Translation;

public class RoleServiceTest {

	@InjectMocks
	private DefaultRoleService roleService;

	@Mock
	private RoleDao roleDao;

	@Mock
	private TranslationDao translationDao;

	@Mock
	private TranslationService translationService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRetrieveByIdNotFound() {
		long roleId = 100;
		when(roleDao.getRole(roleId)).thenReturn(Optional.empty());

		Optional<Role> maybeRole = roleService.retrieve(roleId, Locale.ENGLISH);

		assertFalse(maybeRole.isPresent());
	}

	@Test
	public void testRetrieveByIdWithTranslation() {
		long roleId = 100;
		long translationId = 200;
		Role role = roleWithIdAndTranslationId(roleId, translationId);
		Translation translation = translationWithId(translationId);

		when(roleDao.getRole(roleId)).thenReturn(Optional.of(role));
		when(translationService.retrieve(translationId)).thenReturn(Optional.of(translation));
		when(translationService.translatedText(translation, Locale.ENGLISH)).thenReturn("description");

		Optional<Role> maybeRole = roleService.retrieve(roleId, Locale.ENGLISH);

		assertTrue(maybeRole.isPresent());
		assertEquals("description", maybeRole.get().getDescription());
	}

	@Test
	public void testRetrieveAllNoneFound() {
		when(roleDao.getRoles()).thenReturn(Collections.emptyList());

		List<Role> roles = roleService.retrieve(Locale.ENGLISH);

		assertTrue(roles.isEmpty());
	}

	@Test
	public void testRetrieveAllWithTranslations() {
		long role1Id = 101;
		long role2Id = 102;
		long translation1Id = 201;
		long translation2Id = 202;
		Role role1 = roleWithIdAndTranslationId(role1Id, translation1Id);
		Role role2 = roleWithIdAndTranslationId(role2Id, translation2Id);
		Translation translation1 = translationWithId(translation1Id);
		Translation translation2 = translationWithId(translation2Id);

		when(roleDao.getRoles()).thenReturn(Arrays.asList(role1, role2));
		when(translationService.retrieve(translation1Id)).thenReturn(Optional.of(translation1));
		when(translationService.retrieve(translation2Id)).thenReturn(Optional.of(translation2));
		when(translationService.translatedText(translation1, Locale.ENGLISH)).thenReturn("description 1");
		when(translationService.translatedText(translation2, Locale.ENGLISH)).thenReturn("description 2");

		List<Role> roles = roleService.retrieve(Locale.ENGLISH);

		assertEquals(2, roles.size());
		assertEquals("description 1", roles.get(0).getDescription());
		assertEquals("description 2", roles.get(1).getDescription());
	}

	private Role roleWithIdAndTranslationId(long roleId, long translationId) {
		Role role = new Role();
		role.setId(roleId);
		role.getTranslatedDescription().setId(translationId);
		return role;
	}

	private Translation translationWithId(long translationId) {
		Translation translatedDescription = new Translation();
		translatedDescription.setId(translationId);
		return translatedDescription;
	}
}
