package eu.uneltelibere.scarpa.web.interceptor;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.UserService;

@Component
public class DefaultCurrentUserService implements CurrentUserService {

	private static final String SESSION_KEY_CURRENT_USER = "session.key.current.user";

	@Autowired
	private UserService userService;

	@Override
	public Optional<User> getCurrentUser(HttpSession session) {
		Object currentUserValue = session.getAttribute(SESSION_KEY_CURRENT_USER);
		if (currentUserValue instanceof User) {
			return Optional.of((User) currentUserValue);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public User getCurrentUserOrFail(HttpSession session) {
		Optional<User> maybeCurrentUser = getCurrentUser(session);
		return maybeCurrentUser.orElseThrow(() -> new IllegalStateException("No user logged in."));
	}

	@Override
	public User getCurrentUserOrFail(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return getCurrentUserOrFail(session);
	}

	@Override
	public void setCurrentUser(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute(SESSION_KEY_CURRENT_USER) == null) {
			User currentUser = userService.currentUser(request).orElse(null);
			session.setAttribute(SESSION_KEY_CURRENT_USER, currentUser);
		}
	}
}
