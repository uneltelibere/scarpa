package eu.uneltelibere.scarpa.web.controller.search;

import java.util.List;

public interface FilterParser {

	List<TextCondition> parseFilter(List<String> filters);

	TextCondition parseFilter(String filter);
}
