package eu.uneltelibere.scarpa.web.controller.data.column;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.web.controller.data.Input;
import eu.uneltelibere.scarpa.web.controller.data.Text;

public class TableColumn<T extends Entity> {

	public static class Heading {

		private String code = "";

		private String text = "";

		private String abbreviationTitle = "";

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getAbbreviationTitle() {
			return abbreviationTitle;
		}

		public void setAbbreviationTitle(String abbreviationTitle) {
			this.abbreviationTitle = abbreviationTitle;
		}
	}

	public static class Value {

		private String text = "";

		private String title = "";

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
	}

	private Heading heading = new Heading();

	private Map<T, Value> values = new HashMap<>();

	private Map<T, List<Value>> detailedValues = new HashMap<>();

	private TableColumnFilterType filterType = TableColumnFilterType.TEXT;

	private Input filterInput = new Text();

	private boolean noSearch;

	public Heading getHeading() {
		return heading;
	}

	public void setHeading(Heading heading) {
		this.heading = heading;
	}

	public Map<T, Value> getValues() {
		return values;
	}

	public void setValues(Map<T, Value> values) {
		this.values = values;
	}

	public Map<T, List<Value>> getDetailedValues() {
		return detailedValues;
	}

	public void setDetailedValues(Map<T, List<Value>> detailedValues) {
		this.detailedValues = detailedValues;
	}

	public TableColumnFilterType getFilterType() {
		return filterType;
	}

	public void setFilterType(TableColumnFilterType filterType) {
		this.filterType = filterType;
	}

	public Input getFilterInput() {
		return filterInput;
	}

	public void setFilterInput(Input filterInput) {
		this.filterInput = filterInput;
	}

	public boolean isNoSearch() {
		return noSearch;
	}

	public void setNoSearch(boolean noSearch) {
		this.noSearch = noSearch;
	}
}
