package eu.uneltelibere.scarpa.web.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.api.OrderedMenu;

@Service
@Primary
public class DefaultMenuHolder extends AbstractMenuHolder {

	@Autowired
	private OrderedMenu orderedMenu;

	@Override
	protected OrderedMenu getOrderedMenu() {
		return orderedMenu;
	}
}
