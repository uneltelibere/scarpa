package eu.uneltelibere.scarpa.dao.sql;

public class AndSqlCondition extends SqlCondition {

	@Override
	protected String conjunction() {
		return " AND ";
	}
}
