package eu.uneltelibere.scarpa.service.user;

import eu.uneltelibere.scarpa.model.User;

public interface UserValidationService {

	void validate(User user);
}
