package eu.uneltelibere.scarpa.web.menu;

import java.util.List;

public interface ProfileMenuListExtender {

	List<Menu> menusBefore();

	List<Menu> menusAfter();
}
