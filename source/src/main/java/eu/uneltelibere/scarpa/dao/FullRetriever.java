package eu.uneltelibere.scarpa.dao;

import java.util.List;

public interface FullRetriever<T> {

	List<T> retrieveAll();
}
