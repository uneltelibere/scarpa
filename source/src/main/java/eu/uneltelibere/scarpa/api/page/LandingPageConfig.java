package eu.uneltelibere.scarpa.api.page;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.api.Role;

public interface LandingPageConfig {

	Optional<String> applicationLandingPage();

	Map<Role, String> roleToWelcomePage();

	void preprocess(Model model, HttpServletRequest request);
}
