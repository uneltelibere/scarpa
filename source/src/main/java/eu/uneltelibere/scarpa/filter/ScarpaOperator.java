package eu.uneltelibere.scarpa.filter;

import eu.uneltelibere.scarpa.filter.Condition.Operator;

public enum ScarpaOperator implements Operator {

	// @formatter:off
	ANY("any", Type.UNARY),
	EQUALS("eq", Type.BINARY),
	LIKE("like", Type.BINARY),
	IN("in", Type.BINARY),
	LESS_THAN_OR_EQUAL("le", Type.BINARY),
	GREATER_THAN("gt", Type.BINARY);
	// @formatter:on

	private final String name;
	private final Type type;

	private ScarpaOperator(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	public static ScarpaOperator from(String name) {
		for (ScarpaOperator value : ScarpaOperator.values()) {
			if (value.name.equals(name)) {
				return value;
			}
		}
		throw new IllegalArgumentException("No enum value with name " + name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Type getType() {
		return type;
	}
}
