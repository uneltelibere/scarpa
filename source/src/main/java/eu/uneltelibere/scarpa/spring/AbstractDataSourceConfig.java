package eu.uneltelibere.scarpa.spring;

import org.flywaydb.core.Flyway;

public abstract class AbstractDataSourceConfig {

	protected void migrateFlyway(FlywayProperties flywayProperties, JdbcProperties jdbc) {
		final Flyway flyway = new Flyway();
		flyway.setDataSource(jdbc.getUrl(), jdbc.getUsername(), jdbc.getPassword());
		flyway.setSchemas(flywayProperties.getSchemas());
		flyway.setLocations(flywayProperties.getLocations().split(","));
		flyway.setTable(flywayProperties.getTable());
		flyway.migrate();
	}
}
