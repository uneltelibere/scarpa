package eu.uneltelibere.scarpa.spring;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class RegExPasswordRemover implements PasswordRemover {

	/**
	 * Characters that may not appear unescaped in an URI:
	 * 
	 * <pre>
	 * ! * ' ( ) ; : @ & = + $ , / ? # [ ]
	 * </pre>
	 * 
	 * See <a href="https://tools.ietf.org/html/rfc3986#section-2.2">RFC 3986
	 * section 2.2 Reserved Characters</a>.
	 */
	private static final String URI_RESERVED_CHARACTERS = "!*'();:@&=+$,/?#[]";
	private static final Pattern PATTERN = Pattern
			.compile("(password|passwordAgain)=([^" + Pattern.quote(URI_RESERVED_CHARACTERS) + "]*)");

	private static final String REPLACEMENT = "***";

	@Override
	public String removePassword(String uri) {
		/*
		 * In an URI, when encountering a parameter named "password" or "passwordAgain",
		 * replace its value with three stars (*).
		 */
		return PATTERN.matcher(uri).replaceAll("$1=" + REPLACEMENT);
	}
}
