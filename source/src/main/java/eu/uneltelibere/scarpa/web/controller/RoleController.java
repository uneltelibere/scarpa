package eu.uneltelibere.scarpa.web.controller;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;

import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.service.RoleService;
import eu.uneltelibere.scarpa.web.menu.MenuHolder;
import eu.uneltelibere.scarpa.web.menu.UsersMenu;

@Controller
@RequestMapping("/role")
public class RoleController {

	private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);

	@Autowired
	private MenuHolder menu;

	@Autowired
	private RoleService roleService;

	@Autowired
	private LocaleResolver localeResolver;

	@GetMapping
	public String listRoles(Model model, HttpServletRequest request) {
		LOG.info("Showing role list page");
		menu.set(UsersMenu.class, request, model);
		Locale locale = localeResolver.resolveLocale(request);
		model.addAttribute("roles", roleService.retrieve(locale));
		return "roleList";
	}

	@GetMapping(value = "/{id}")
	public String editRolePage(@PathVariable("id") long id, Model model, HttpServletRequest request) {
		LOG.info("Showing details page for role {}", id);
		menu.set(UsersMenu.class, request, model);
		Locale locale = localeResolver.resolveLocale(request);
		Optional<Role> maybeRole = roleService.retrieve(id, locale);
		if (maybeRole.isPresent()) {
			model.addAttribute("role", maybeRole.get());
		} else {
			model.addAttribute("alert", "warning");
			model.addAttribute("alertMessage", "page.role.edit.notfound");
			model.addAttribute("mayEditRole", false);
		}
		return "roleEdit";
	}

	@PostMapping(value = "/{id}")
	public String postRole(@PathVariable("id") long id, @RequestParam(name = "roleName", defaultValue = "") String name,
			@RequestParam(name = "roleDescription", defaultValue = "") String description, HttpServletRequest request) {
		LOG.info("Received data for role {}", id);

		Locale locale = localeResolver.resolveLocale(request);
		Optional<Role> maybeRole = roleService.retrieve(id, locale);
		Role role = maybeRole.orElseThrow(() -> new IllegalStateException("Role not found."));
		role.setId(id);
		role.setName(name);
		role.setDescription(description);

		roleService.save(role, locale);

		return "redirect:/role/" + role.getId();
	}
}
