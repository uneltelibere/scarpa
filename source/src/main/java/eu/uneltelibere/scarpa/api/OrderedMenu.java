package eu.uneltelibere.scarpa.api;

import java.util.List;

import eu.uneltelibere.scarpa.web.menu.Menu;

public interface OrderedMenu {

	List<Menu> menus();
}
