package eu.uneltelibere.scarpa.api.page;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.model.User;

public interface UserPageConfig {

	String getEditUserPage(long id, Model model, HttpServletRequest request);

	void postSaveUser(User user, HttpServletRequest request);
}
