package eu.uneltelibere.scarpa.spring;

import javax.sql.DataSource;

public interface MasterDataSource extends DataSource {
}
