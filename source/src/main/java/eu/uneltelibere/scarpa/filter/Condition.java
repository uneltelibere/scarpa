package eu.uneltelibere.scarpa.filter;

public interface Condition<T> {

	public interface Field {

		String getName();

		Class<?> getType();
	}

	public interface Operator {

		public enum Type {
			UNARY, BINARY;
		}

		String getName();

		Type getType();
	}

	Field getField();

	Operator getOperator();

	Object getValue();
}
