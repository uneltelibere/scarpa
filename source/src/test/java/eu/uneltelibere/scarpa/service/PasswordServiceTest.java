package eu.uneltelibere.scarpa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import eu.uneltelibere.scarpa.service.DefaultPasswordService;

public class PasswordServiceTest {

	private DefaultPasswordService passwordService;
	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Before
	public void setup() {
		passwordService = new DefaultPasswordService();
		passwordService.setPasswordEncoder(passwordEncoder);
	}

	@Test
	public void testGeneratePassword() {
		String password = passwordService.generatePassword();
		assertEquals(12, password.length());
	}

	@Test
	public void testEncodePassword() {
		String plain = "abc";
		String encoded = passwordService.encodePassword(plain);
		assertNotEquals(plain, encoded);
	}

	@Test
	public void testEncodedPasswordMatches() {
		String plain = "abc";
		String encoded = passwordService.encodePassword(plain);
		assertTrue(passwordEncoder.matches(plain, encoded));
	}
}
