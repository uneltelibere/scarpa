package eu.uneltelibere.scarpa.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.uneltelibere.scarpa.api.AbstractPagedRetrievingService;
import eu.uneltelibere.scarpa.api.FilteredPagedRetrievingService;
import eu.uneltelibere.scarpa.model.User;

@Configuration
public class ScarpaPagedRetrievingServices {

	@Bean
	public FilteredPagedRetrievingService<User> userRetrievingService() {
		return new AbstractPagedRetrievingService<User>() {
		};
	}
}
