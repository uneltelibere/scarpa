package eu.uneltelibere.scarpa;

import eu.uneltelibere.scarpa.api.AbstractRole;

/**
 * Special role that does not exist in the database, and is used for matching
 * any role.
 */
public final class AnyRole extends AbstractRole {

	public AnyRole() {
		super("ANY");
	}
}
