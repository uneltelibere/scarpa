package eu.uneltelibere.scarpa.service.lister;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.api.FilteredPagedRetrievingService;
import eu.uneltelibere.scarpa.dao.SortCriteria;
import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.model.PageNavigator;
import eu.uneltelibere.scarpa.service.PageNavigationBuilder;

public abstract class FilteredPagedLister<T extends Entity> implements FilteredLister<T> {

	private static final int OBJECTS_PER_PAGE = 25;

	@Autowired
	private FilteredPagedRetrievingService<T> filteredPagedRetrievingService;

	@Override
	public List<T> getList(ConditionGroup<T> conditions, int pageNumber) {
		int offset = (pageNumber - 1) * OBJECTS_PER_PAGE;
		return filteredPagedRetrievingService.retrieveFiltered(conditions, offset, OBJECTS_PER_PAGE);
	}

	@Override
	public List<T> getList(ConditionGroup<T> conditions, Collection<SortCriteria<T>> sortCriteria, int pageNumber) {
		int offset = (pageNumber - 1) * OBJECTS_PER_PAGE;
		return filteredPagedRetrievingService.retrieveFilteredAndSorted(conditions, sortCriteria, offset,
				OBJECTS_PER_PAGE);
	}

	@Override
	public Optional<PageNavigator> getNavigator(ConditionGroup<T> conditions, String basePageUrl, int pageNumber) {
		PageNavigator navigator = null;
		int count = filteredPagedRetrievingService.count(conditions);
		int pageCount = (int) Math.ceil((double) count / OBJECTS_PER_PAGE);
		if (pageCount > 1) {
			navigator = new PageNavigationBuilder().currentPage(pageNumber).pageCount(pageCount)
					.basePageUrl(basePageUrl).build();
		}
		return Optional.ofNullable(navigator);
	}
}
