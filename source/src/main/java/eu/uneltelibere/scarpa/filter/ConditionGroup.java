package eu.uneltelibere.scarpa.filter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public abstract class ConditionGroup<T> {

	protected Collection<Condition<T>> conditions;

	@SafeVarargs
	protected ConditionGroup(Condition<T>... conditions) {
		this(new LinkedList<>(Arrays.asList(conditions)));
	}

	protected ConditionGroup(Collection<Condition<T>> conditions) {
		this.conditions = conditions;
	}

	public Collection<Condition<T>> getConditions() {
		return Collections.unmodifiableCollection(conditions);
	}

	public void add(Condition<T> condition) {
		this.conditions.add(condition);
	}

	public void add(Collection<Condition<T>> conditions) {
		this.conditions.addAll(conditions);
	}
}
