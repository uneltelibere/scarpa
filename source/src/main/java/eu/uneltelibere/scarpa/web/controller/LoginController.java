package eu.uneltelibere.scarpa.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.uneltelibere.scarpa.service.user.UserRegistrationService;

@Controller
@RequestMapping("/login")
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UserRegistrationService userRegistrationService;

	@GetMapping
	public String showLoginForm(Model model) {
		LOG.info("Showing the login page");
		model.addAttribute("isRegisterEnabled", userRegistrationService.isUserRegistrationEnabled());
		return "login";
	}
}
