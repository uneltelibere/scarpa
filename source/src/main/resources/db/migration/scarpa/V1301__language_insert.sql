INSERT INTO language(code, name)
SELECT 'en', 'English'
WHERE NOT EXISTS (SELECT code FROM language WHERE code = 'en');

INSERT INTO language(code, name)
SELECT 'ro', 'Română'
WHERE NOT EXISTS (SELECT code FROM language WHERE code = 'ro');
