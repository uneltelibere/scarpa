package eu.uneltelibere.scarpa.web.controller.data.column;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.model.Entity;

public interface TableColumnFactory<T extends Entity> {

	boolean canHandle(String code);

	TableColumn<T> column(TableColumnSource<T> tableColumnSource);

	TableColumn.Heading columnHeading(HttpServletRequest request);
}
