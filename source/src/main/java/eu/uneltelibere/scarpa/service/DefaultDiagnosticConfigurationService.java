package eu.uneltelibere.scarpa.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DefaultDiagnosticConfigurationService implements DiagnosticConfigurationService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultDiagnosticConfigurationService.class);

	private static final String DEFAULT_DIR = "";

	@Value("${app.diagnostic.log-dir:" + DEFAULT_DIR + "}")
	private String downloadDir;

	@Override
	public String logDirectory() {
		if (logDirNotConfigured()) {
			LOG.warn("Export directory is not configured. Please see app.diagnostic.log-dir.");
		}
		return downloadDir;
	}

	private boolean logDirNotConfigured() {
		return DEFAULT_DIR.equals(downloadDir);
	}
}
