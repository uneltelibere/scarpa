package eu.uneltelibere.scarpa.spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

public class PostRequestLoggingFilter extends AbstractRequestLoggingFilter {

	private static final String POST = RequestMethod.POST.toString();

	@Autowired
	private PasswordRemover passwordRemover;

	@Override
	protected boolean shouldLog(HttpServletRequest request) {
		return logger.isDebugEnabled() && POST.equals(request.getMethod());
	}

	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		// We don't need to log both before and after the request.
		// Chose to log *after* because it includes the parameters.
	}

	/**
	 * Writes a log message after the request is processed.
	 */
	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		String censoredMessage = passwordRemover.removePassword(message);
		logger.debug(censoredMessage);
	}
}
