(function() {
	$('.date-button').datepicker({
		weekStart : 1,
		todayBtn : "linked",
		todayHighlight : true,
		autoclose : true
	}).on('changeDate', function(ev) {
		targetId = $(this).data('target');

		targetFormat = $(this).data('target-format');
		selectedDate = DateFormat.format.date(ev.date, targetFormat);

		$('#' + targetId).val(selectedDate);
	});
})();
