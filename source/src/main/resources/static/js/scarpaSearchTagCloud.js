(function() {

	$(document).on('click', '.cloud-tag', function removeTag() {
		var code = $(this).data('code');

		var form = $(this).closest("form");

		$(this).remove();

		$('.cloud-tag').each(function() {
			$(this).prop('disabled', true);
		});

		form.append('<input type="hidden" name="exclude" value="' + code + '" />');
		form.submit();
	});
})();
