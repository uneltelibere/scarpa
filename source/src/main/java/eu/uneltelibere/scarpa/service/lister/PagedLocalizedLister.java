package eu.uneltelibere.scarpa.service.lister;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.api.LocalizedPagedRetrievingService;
import eu.uneltelibere.scarpa.api.Page;
import eu.uneltelibere.scarpa.model.PageNavigator;
import eu.uneltelibere.scarpa.service.PageNavigationBuilder;

public abstract class PagedLocalizedLister<T> implements LocalizedLister<T> {

	private static final int OBJECTS_PER_PAGE = 25;

	@Autowired
	private LocalizedPagedRetrievingService<T> pagedRetrievingService;

	@Override
	public List<T> getList(int pageNumber, Locale locale) {
		int offset = (pageNumber - 1) * OBJECTS_PER_PAGE;
		return pagedRetrievingService.retrieve(Page.of(offset, OBJECTS_PER_PAGE), locale);
	}

	@Override
	public Optional<PageNavigator> getNavigator(String basePageUrl, int pageNumber) {
		PageNavigator navigator = null;
		int count = pagedRetrievingService.count();
		int pageCount = (int) Math.ceil((double) count / OBJECTS_PER_PAGE);
		if (pageCount > 1) {
			navigator = new PageNavigationBuilder().currentPage(pageNumber).pageCount(pageCount)
					.basePageUrl(basePageUrl).build();
		}
		return Optional.ofNullable(navigator);
	}
}
