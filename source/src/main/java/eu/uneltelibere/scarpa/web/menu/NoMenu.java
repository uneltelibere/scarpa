package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.web.menu.AbstractMenu;

@Component
public class NoMenu extends AbstractMenu {

	public NoMenu() {
		super("-", "-");
	}
}
