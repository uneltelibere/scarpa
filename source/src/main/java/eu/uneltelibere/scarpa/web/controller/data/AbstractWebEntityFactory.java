package eu.uneltelibere.scarpa.web.controller.data;

import java.util.ArrayList;
import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractWebEntityFactory<T extends Entity, W extends T> implements WebEntityFactory<T, W> {

	@Override
	public final List<W> webEntities(List<T> entities) {
		List<W> webEntities = new ArrayList<>(entities.size());
		for (T entity : entities) {
			webEntities.add(webEntity(entity));
		}
		return webEntities;
	}
}
