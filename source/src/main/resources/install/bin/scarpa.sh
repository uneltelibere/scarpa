#!/bin/bash

JAVA=java
APP_FINISHED=0
RESTART_CODE=64

SCRIPT_PATH=$(dirname $(readlink -f $0))
APP_HOME=$(dirname $SCRIPT_PATH)

function run {

    APP_JAR=$(most_recent_app_jar)

    APP_FINISHED=0

    JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom"
    JAVA_OPTS=$JAVA_OPTS" -Djava.awt.headless=true"
	JAVA_OPTS=$JAVA_OPTS" -Dspring.profiles.active=live"

    export SCARPA_HOME=$APP_HOME

    $JAVA $JAVA_OPTS -jar $APP_JAR --spring.config.location=classpath:/config/scarpa.yaml,$APP_HOME/etc/application.yaml &

    APP_PID=$!
    wait $APP_PID
    EXIT_CODE=$?
    APP_FINISHED=1

    echo "App exited with code "$EXIT_CODE
}

function cleanup {
	echo "Cleaning up..."
	if [ $APP_FINISHED -eq 0 ]; then
		kill -TERM $APP_PID; exit 0;
	else
		echo "App already finished."
	fi
}

function most_recent_app_jar {
    echo $(ls -1 --sort=time $APP_HOME/lib/*.jar | head -n 1)
}

function main {
    trap cleanup EXIT

    run
    while [ $EXIT_CODE -eq $RESTART_CODE ]; do
        echo "Restarting..."
        run
    done
}

main

