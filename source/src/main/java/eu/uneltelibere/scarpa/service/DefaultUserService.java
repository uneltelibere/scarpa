package eu.uneltelibere.scarpa.service;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.api.Page;
import eu.uneltelibere.scarpa.dao.RoleDao;
import eu.uneltelibere.scarpa.dao.RoleGroupDao;
import eu.uneltelibere.scarpa.dao.UserDao;
import eu.uneltelibere.scarpa.model.Role;
import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

@Service
public class DefaultUserService implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultUserService.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private RoleGroupDao roleGroupDao;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PasswordService passwordService;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private UserEmailService mailService;

	@Autowired
	private AuthorizedUserService authorizedUserService;

	@Override
	public int count() {
		return userDao.count();
	}

	@Override
	public List<User> retrieve(int offset, int limit) {
		return retrieve(Page.of(offset, limit), Locale.ENGLISH);
	}

	@Override
	public List<User> retrieve(Page page, Locale locale) {
		List<User> users = userDao.retrieve(page.getOffset(), page.getLimit());
		for (User user : users) {
			user = authorizedUserService.userWithRoles(user);
			user.setRoles(roleDao.getRolesOfUser(user));
		}
		return users;
	}

	@Override
	public User getOrMake(long id) {
		return getById(id).orElseGet(() -> new User.Builder().id(id).build());
	}

	@Override
	public Optional<User> getById(long id) {
		return getById(id, Locale.ENGLISH);
	}

	@Override
	public Optional<User> getById(long id, Locale locale) {
		return userDao.getUser(id).map(user -> putAllRoles(user, locale));
	}

	@Override
	public Optional<User> getUser(String username, Locale locale) {
		return userDao.getUser(username).map(user -> {
			user = authorizedUserService.userWithRoles(user);
			putAllRoles(user, locale);
			return user;
		});
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

	@Override
	public Optional<User> getUserDetails(String username) {
		return authorizedUserService.getUserDetails(username);
	}

	@Override
	public Optional<User> currentUser(HttpServletRequest httpServletRequest) {
		Principal currentUser = httpServletRequest.getUserPrincipal();
		if (currentUser != null) {
			String currentUsername = currentUser.getName();
			return getUserDetails(currentUsername);
		}
		return Optional.empty();
	}

	@Override
	public User newUser(Locale locale) {
		User user = new User();
		user.setId(User.NEW_ID);
		putAllRoles(user, locale);
		return user;
	}

	private void putDefaultPassword(User user) {
		String plainTextPassword = passwordService.generatePassword();
		LOG.info("Default password for {}: {}", user.getUsername(), plainTextPassword);
		user.setPassword(passwordService.encodePassword(plainTextPassword));
	}

	private User putAllRoles(User user, Locale locale) {
		List<Role> allRoles = roleService.retrieve(locale);
		for (Role generalRole : allRoles) {
			generalRole.setAssigned(false);
			for (Role userRole : user.getRoles()) {
				if (generalRole.getId() == userRole.getId()) {
					generalRole.setAssigned(true);
					break;
				}
			}
		}
		user.setRoles(allRoles);
		return user;
	}

	@Override
	public User save(User user) {
		if (user.getPassword().isEmpty()) {
			userDao.updateWithoutPassword(user);
		} else {
			userDao.update(user);
		}

		return user;
	}

	@Override
	public long saveWithoutPassword(final User user) {
		if (user.getId() == User.NEW_ID) {
			putDefaultPassword(user);
			user.setCredentialsNonExpired(false);
			long id = userDao.create(user);
			welcomeUserById(id);
			return id;
		} else {
			userDao.updateWithoutPassword(user);
			return user.getId();
		}
	}

	@Override
	public long register(final User user) {
		user.setId(User.NEW_ID);
		user.setPassword(passwordService.encodePassword(user.getPassword()));
		long id = userDao.create(user);
		Executors.newSingleThreadExecutor().execute(() -> mailService.sendWelcomeMail(user));
		return id;
	}

	private void welcomeUserById(long id) {
		final User userInDb = userDao.getUser(id).orElse(new User());
		Token token = tokenService.createToken(userInDb);
		Executors.newSingleThreadExecutor().execute(() -> mailService.sendWelcomeMail(token));
	}

	@Override
	public void setRoleGroupsForUser(final User user, List<Long> groupIDs) {
		List<Long> existingGroupIds = roleGroupDao.getRoleGroupsOfUser(user).stream().map(RoleGroup::getId)
				.collect(Collectors.toList());

		List<Long> groupsToRemove = existingGroupIds.stream().filter(group -> !groupIDs.contains(group))
				.collect(Collectors.toList());
		if (!groupsToRemove.isEmpty()) {
			userDao.removeGroupsFromUser(user, groupsToRemove);
		}

		List<Long> groupsToAdd = groupIDs.stream().filter(group -> !existingGroupIds.contains(group))
				.collect(Collectors.toList());
		if (!groupsToAdd.isEmpty()) {
			userDao.addGroupsToUser(user, groupsToAdd);
		}
	}

	@Async
	@Override
	public Future<Boolean> resetPassword(User user) {
		LOG.info("Sending password reset email to user {}, at {}.", user.getUsername(), user.getEmail());
		Token token = tokenService.createToken(user);
		boolean sent = mailService.sendRecoveryMail(token);
		// TODO change the password to a new random one
		return new AsyncResult<>(sent);
	}

	@Override
	public void setPassword(User user, String encodedPassword) {
		LOG.info("Changing password for user {}", user.getId());
		user.setPassword(encodedPassword);
		userDao.setPassword(user);
	}

	@Override
	public void delete(long userId) {
		userDao.delete(userId);
	}

	@Override
	public boolean userHasRole(User user, eu.uneltelibere.scarpa.api.Role roleToCheck) {
		List<Role> roles = user.getRoles();
		for (Role role : roles) {
			String roleFromDb = role.getCode();
			String roleCodeToCheck = "ROLE_" + roleToCheck.name();
			if (roleCodeToCheck.equals(roleFromDb)) {
				return true;
			}
		}
		return false;
	}
}
