package eu.uneltelibere.scarpa.dao.util;

import eu.uneltelibere.scarpa.filter.Condition;
import eu.uneltelibere.scarpa.model.Entity;

public interface ConditionSerializer {

	<T extends Entity> String serializeValue(Condition<T> condition);
}
