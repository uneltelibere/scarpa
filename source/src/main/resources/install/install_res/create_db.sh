#!/bin/bash

if [ $# -lt 3 ]
then
	echo "Usage: $0 create_db_user_file create_db_file db_name"
	exit 1
fi

create_db_user_file=$1
create_db_file=$2
db_name=$3

echo "Creating database user..."
psql -U postgres --file $create_db_user_file --quiet

echo "Creating database..."
if psql -U postgres --tuples-only --command="SELECT 1 FROM pg_database WHERE datname = '$db_name'" | grep -q 1
then
	echo "Database already exists."
else
	psql -U postgres --file $create_db_file --quiet
fi

