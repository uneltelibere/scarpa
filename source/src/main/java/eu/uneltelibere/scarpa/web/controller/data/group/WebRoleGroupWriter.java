package eu.uneltelibere.scarpa.web.controller.data.group;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.service.TranslationService;
import eu.uneltelibere.scarpa.web.controller.data.AbstractWebEntityWriter;

@Service
public class WebRoleGroupWriter extends AbstractWebEntityWriter<WebRoleGroup> {

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private TranslationService translationService;

	@Override
	public WebRoleGroup write(WebRoleGroup webRoleGroup, HttpServletRequest request) {
		Locale locale = localeResolver.resolveLocale(request);
		Translation name = translationService.retrieve(webRoleGroup.getName().getId()).orElse(webRoleGroup.getName());
		String localizedName = translationService.translatedText(name, locale);
		webRoleGroup.setDisplayName(localizedName);
		return webRoleGroup;
	}
}
