package eu.uneltelibere.scarpa.web.controller.data;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractWebEntityWriter<T extends Entity> implements WebEntityWriter<T> {

	@Override
	public T write(T entity) {
		return entity;
	}

	@Override
	public final List<T> write(List<T> entities) {
		return entities.stream().map(this::write).collect(toList());
	}

	@Override
	public T write(T entity, HttpServletRequest request) {
		return entity;
	}

	@Override
	public final List<T> write(List<T> entities, HttpServletRequest request) {
		return entities.stream().map(entity -> write(entity, request)).collect(toList());
	}
}
