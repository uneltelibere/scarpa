package eu.uneltelibere.scarpa.service.lister;

import java.util.List;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.PageNavigator;

public interface Lister<T> {

	List<T> getList(int pageNumber);

	Optional<PageNavigator> getNavigator(String basePageUrl, int pageNumber);
}
