package eu.uneltelibere.scarpa.web.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.api.OrderedMenu;
import eu.uneltelibere.scarpa.web.menu.AbstractMenuHolder;

@Service
public class ProfileMenuHolder extends AbstractMenuHolder {

	@Autowired
	@Qualifier("profileMenuList")
	private OrderedMenu orderedMenu;

	@Override
	protected OrderedMenu getOrderedMenu() {
		return orderedMenu;
	}
}
