package eu.uneltelibere.scarpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConfig {

	@Bean
	public RuntimeContext runtimeContext() {
		return new RuntimeContext();
	}
}
