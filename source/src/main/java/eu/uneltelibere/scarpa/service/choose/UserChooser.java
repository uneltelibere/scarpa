package eu.uneltelibere.scarpa.service.choose;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.choose.api.AbstractNonFilteredDependencyChooser;
import eu.uneltelibere.scarpa.service.choose.api.DependencyChooserParameters;

@Component
public class UserChooser extends AbstractNonFilteredDependencyChooser<User> {

	@Override
	public String choose(DependencyChooserParameters<User> params, Model model, HttpSession session) {
		params.setListAttributeName("users");
		prepareChoosePage(params, model, session);
		return "userChoose";
	}
}
