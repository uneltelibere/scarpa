CREATE TABLE IF NOT EXISTS setting (
	id SERIAL PRIMARY KEY NOT NULL,
	setting_key VARCHAR(128) NOT NULL,
	setting_value VARCHAR(128) NOT NULL
);
