package eu.uneltelibere.scarpa.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class AndSqlConditionTest {

	@Test
	void testEmpty() {
		SqlCondition condition = new AndSqlCondition();
		assertEquals("true", condition.query());
	}

	@Test
	void testOneCondition() {
		SqlCondition condition = new AndSqlCondition();
		condition.addSql("x = 1");
		assertEquals("x = 1", condition.query());
	}

	@Test
	void testTwoConditions() {
		SqlCondition condition = new AndSqlCondition();
		condition.addSql("x = 1");
		condition.addSql("y < 2");
		assertEquals("x = 1 AND y < 2", condition.query());
	}
}
