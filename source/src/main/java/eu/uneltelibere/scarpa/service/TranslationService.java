package eu.uneltelibere.scarpa.service;

import java.util.Locale;
import java.util.Optional;

import eu.uneltelibere.scarpa.model.Translation;

public interface TranslationService {

	Optional<Translation> retrieve(long id);

	String translatedText(Translation translation, Locale locale);

	Translation set(Translation translation, String text, Locale locale);

	void save(Translation translation);
}
