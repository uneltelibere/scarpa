package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import eu.uneltelibere.scarpa.dao.ScarpaEnumDao;
import eu.uneltelibere.scarpa.model.ScarpaEnum;

public abstract class JdbcScarpaEnumDao<T extends ScarpaEnum> extends JdbcApplicationDao implements ScarpaEnumDao<T> {

	// @formatter:off
	private static final String GET_ALL = "SELECT "
			+ "id, code, name "
			+ "FROM %s ";

	private static final String GET_BY_ID = GET_ALL + "WHERE id = :id";

	private static final String GET_BY_CODE = GET_ALL + "WHERE code = :code";
	// @formatter:off

	private ResultSetExtractor<Optional<T>> singleEntityExtractor = new ResultSetExtractor<Optional<T>>() {

		@Override
		public Optional<T> extractData(ResultSet rs) throws SQLException, DataAccessException {
			Optional<T> maybeEntity = Optional.empty();
			if (rs.next()) {
				T entity = entityFromResultSet(rs);
				maybeEntity = Optional.of(entity);
			}
			return maybeEntity;
		}
	};

	private T entityFromResultSet(ResultSet rs) throws SQLException {
		T entity = newEntity();
		entity.setId(rs.getLong("id"));
		entity.setCode(rs.getString("code"));
		entity.setName(rs.getString("name"));
		return entity;
	}

	protected abstract T newEntity();

	@Override
	public Optional<T> getById(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		return jdbcTemplate.query(queryGetById(), params, singleEntityExtractor);
	}

	@Override
	public Optional<T> getByCode(String code) {
		SqlParameterSource params = new MapSqlParameterSource("code", code);
		return jdbcTemplate.query(queryGetByCode(), params, singleEntityExtractor);
	}

	@Override
	public List<T> getAll() {
		return jdbcTemplate.query(queryGetAll(), new RowMapper<T>() {

			@Override
			public T mapRow(ResultSet rs, int rowNum) throws SQLException {
				return entityFromResultSet(rs);
			}
		});
	}

	private String queryGetAll() {
		return String.format(GET_ALL, tableName());
	}

	private String queryGetById() {
		return String.format(GET_BY_ID, tableName());
	}

	private String queryGetByCode() {
		return String.format(GET_BY_CODE, tableName());
	}

	protected abstract String tableName();
}
