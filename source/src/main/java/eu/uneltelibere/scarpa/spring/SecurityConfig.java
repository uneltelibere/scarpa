package eu.uneltelibere.scarpa.spring;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import eu.uneltelibere.scarpa.InstallAdminRole;
import eu.uneltelibere.scarpa.UserAdminRole;
import eu.uneltelibere.scarpa.api.AuthorizationConfig;
import eu.uneltelibere.scarpa.api.AuthorizedUrl;
import eu.uneltelibere.scarpa.api.Role;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserDetailsService loginService;

	@Autowired
	private AuthorizationConfig authorizationConfig;

	@Autowired
	private SavedRequestAwareAuthenticationSuccessHandler scarpaAuthenticationSuccessHandler;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth
			.userDetailsService(loginService)
			.passwordEncoder(passwordEncoder);
		// @formatter:on
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		configureApplication(http);
		// @formatter:off
		http
			.authorizeRequests()
				.antMatchers("/users/**").hasRole(new UserAdminRole().name())
				.antMatchers("/role/**").hasRole(new UserAdminRole().name())
				.antMatchers("/install/**").hasRole(new InstallAdminRole().name())
				.antMatchers("/css/**", "/js/**", "/webjars/**").permitAll()
				.antMatchers("/favicon.png", "/favicon.ico").permitAll()
				.antMatchers("/login", "/password/**", "/register", "/forgot").permitAll()
				.antMatchers("/ping").permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.successHandler(successHandler())
				.failureHandler(exceptionMappingAuthenticationFailureHandler())
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.logoutSuccessUrl("/login?logout")
				.permitAll()
			.and()
				.exceptionHandling().accessDeniedPage("/403.html");
		// @formatter:on
	}

	private void configureApplication(HttpSecurity http) throws Exception {
		for (AuthorizedUrl url : authorizationConfig.authorizedUrls()) {
			String[] roleNames = url.getRoles().stream().map(Role::name).toArray(String[]::new);
			if (url.hasHttpMethod()) {
				http.authorizeRequests().antMatchers(url.getHttpMethod(), url.getUrlPattern()).hasAnyRole(roleNames);
			} else {
				http.authorizeRequests().antMatchers(url.getUrlPattern()).hasAnyRole(roleNames);
			}
		}
		for (String url : authorizationConfig.permittedUrls()) {
			http.authorizeRequests().antMatchers(url).permitAll();
		}

		if (authorizationConfig.secureOnly()) {
			http.requiresChannel().anyRequest().requiresSecure();
		}
	}

	@Bean
	public AuthenticationSuccessHandler successHandler() {
		scarpaAuthenticationSuccessHandler.setDefaultTargetUrl("/welcome");
		return scarpaAuthenticationSuccessHandler;
	}

	@Bean
	public ExceptionMappingAuthenticationFailureHandler exceptionMappingAuthenticationFailureHandler() {
		ExceptionMappingAuthenticationFailureHandler ex = new ExceptionMappingAuthenticationFailureHandler();
		Map<String, String> mappings = new HashMap<>();
		mappings.put(CredentialsExpiredException.class.getName(), "/password/expired");
		mappings.put(LockedException.class.getName(), "/login?locked");
		mappings.put(UsernameNotFoundException.class.getName(), "/login?failed");
		mappings.put(BadCredentialsException.class.getName(), "/login?failed");

		ex.setExceptionMappings(mappings);
		return ex;
	}
}
