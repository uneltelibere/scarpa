package eu.uneltelibere.scarpa.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class User extends Entity {

	private static final long serialVersionUID = 1L;

	private String username = "";

	private String email = "";

	private String password = "";

	private List<Role> roles = new LinkedList<>();

	private List<RoleGroup> roleGroups = new LinkedList<>();

	private boolean enabled = true;

	private boolean credentialsNonExpired = true;

	public static class Builder {

		private User user = new User();

		public Builder id(long id) {
			user.setId(id);
			return this;
		}

		public Builder username(String username) {
			user.setUsername(username);
			return this;
		}

		public Builder password(String password) {
			user.setPassword(password);
			return this;
		}

		public Builder email(String email) {
			user.setEmail(email);
			return this;
		}

		public User build() {
			return user;
		}
	}

	public String getRoleNames() {
		if (!getRoles().isEmpty()) {
			return getRoles().stream().map(Role::getName).collect(Collectors.joining(", "));
		} else {
			return "-";
		}
	}

	public String getStatus() {
		if (!isCredentialsNonExpired()) {
			return "Credentials expired";
		} else {
			return "OK";
		}
	}

	@Override
	public String toString() {
		return "User [username=" + getUsername() + "]";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<RoleGroup> getRoleGroups() {
		return roleGroups;
	}

	public void setRoleGroups(List<RoleGroup> roleGroups) {
		this.roleGroups = roleGroups;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
}
