package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.AttachmentDao;
import eu.uneltelibere.scarpa.model.Attachment;

@Repository
public class JdbcAttachmentDao extends JdbcMasterDao implements AttachmentDao {

	// @formatter:off
	private static final String NEW_ATTACHMENT = "INSERT INTO attachment ("
			+ "original_filename, size, content_type) "
			+ "VALUES ("
			+ ":originalFilename, :size, :contentType)";

	private static final String GET_ATTACHMENT = "SELECT id, original_filename, size, content_type "
			+ "FROM attachment WHERE id = :id";

	private static final String GET_ATTACHMENTS = "SELECT id, original_filename, size, content_type "
			+ "FROM attachment WHERE id IN (:ids)";

	private static final String DELETE_ATTACHMENT = "DELETE FROM attachment WHERE id = :id";
	// @formatter:on

	@Override
	public long create(Attachment attachment) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("originalFilename", attachment.getOriginalFilename());
		params.addValue("size", attachment.getSize());
		params.addValue("contentType", attachment.getContentType());
		jdbcTemplate.update(NEW_ATTACHMENT, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}

	@Override
	public Optional<Attachment> getById(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		return jdbcTemplate.query(GET_ATTACHMENT, params, new ResultSetExtractor<Optional<Attachment>>() {

			@Override
			public Optional<Attachment> extractData(ResultSet rs) throws SQLException, DataAccessException {
				Optional<Attachment> maybeAttachment = Optional.empty();
				if (rs.next()) {
					Attachment attachment = new Attachment();
					attachment.setId(rs.getInt("id"));
					attachment.setOriginalFilename(rs.getString("original_filename"));
					attachment.setSize(rs.getLong("size"));
					attachment.setContentType(rs.getString("content_type"));
					maybeAttachment = Optional.of(attachment);
				}
				return maybeAttachment;
			}
		});
	}

	@Override
	public List<Attachment> retrieve(List<Long> ids) {
		if (ids == null || ids.isEmpty()) {
			return new LinkedList<>();
		}
		SqlParameterSource params = new MapSqlParameterSource("ids", ids);
		return jdbcTemplate.query(GET_ATTACHMENTS, params, new ResultSetExtractor<List<Attachment>>() {

			@Override
			public List<Attachment> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Attachment> attachments = new LinkedList<>();
				while (rs.next()) {
					Attachment attachment = new Attachment();
					attachment.setId(rs.getInt("id"));
					attachment.setOriginalFilename(rs.getString("original_filename"));
					attachment.setSize(rs.getLong("size"));
					attachment.setContentType(rs.getString("content_type"));
					attachments.add(attachment);
				}
				return attachments;
			}
		});
	}

	@Override
	public void delete(long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		jdbcTemplate.update(DELETE_ATTACHMENT, params);
	}
}
