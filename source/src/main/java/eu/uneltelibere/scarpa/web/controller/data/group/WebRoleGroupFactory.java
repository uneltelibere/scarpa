package eu.uneltelibere.scarpa.web.controller.data.group;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import eu.uneltelibere.scarpa.model.RoleGroup;
import eu.uneltelibere.scarpa.web.controller.data.AbstractWebEntityFactory;

@Service
public class WebRoleGroupFactory extends AbstractWebEntityFactory<RoleGroup, WebRoleGroup> {

	@Override
	public WebRoleGroup webEntity(RoleGroup roleGroup) {
		WebRoleGroup webRoleGroup = new WebRoleGroup();
		BeanUtils.copyProperties(roleGroup, webRoleGroup);
		return webRoleGroup;
	}
}
