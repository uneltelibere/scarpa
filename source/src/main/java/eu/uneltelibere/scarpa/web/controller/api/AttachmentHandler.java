package eu.uneltelibere.scarpa.web.controller.api;

import org.springframework.web.multipart.MultipartFile;

import eu.uneltelibere.scarpa.model.Attachment;

public interface AttachmentHandler {

	Attachment saveAttachment(MultipartFile incomingFile);
}
