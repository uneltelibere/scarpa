#!/bin/bash

if [ "$EUID" -ne 0 ]
then
	echo "Please run as root."
	exit 1
fi

APP_NAME=scarpa
USER_NAME=scarpad
USER_HOME=/opt/$USER_NAME
SERVICE_DESCRIPTION='Scarpa server'
SCRIPT_PATH=$(dirname $(readlink -f $0))
SCRIPT_PARENT_PATH="$(dirname "$SCRIPT_PATH")"

function create_db {
	create_db_user_temp_file=$(mktemp)
	cp $SCRIPT_PARENT_PATH/install_res/create_db_user.sql $create_db_user_temp_file
	sed -i "s/DB_USER/$DB_USER/g" $create_db_user_temp_file
	sed -i "s/DB_PASS/$DB_PASS/g" $create_db_user_temp_file

	create_db_temp_file=$(mktemp)
	cp $SCRIPT_PARENT_PATH/install_res/create_db.sql $create_db_temp_file
	sed -i "s/DB_USER/$DB_USER/" $create_db_temp_file
	sed -i "s/DB_NAME/$DB_NAME/" $create_db_temp_file

	script_temp_file=$(mktemp)
	cp $SCRIPT_PARENT_PATH/install_res/create_db.sh $script_temp_file

	chown postgres $create_db_user_temp_file $create_db_temp_file $script_temp_file
	chmod +x $script_temp_file

	su --login postgres --command "$script_temp_file $create_db_user_temp_file $create_db_temp_file $DB_NAME"

	rm $script_temp_file
	rm $create_db_temp_file
	rm $create_db_user_temp_file
}

function permit_db_login {
	my_pg_hba_conf=$SCRIPT_PARENT_PATH/install_res/pg_hba.conf
	cp $my_pg_hba_conf.template $my_pg_hba_conf
	sed -i "s/DB_NAME/$DB_NAME/g" $my_pg_hba_conf
	sed -i "s/DB_USER/$DB_USER/g" $my_pg_hba_conf

	pg_hba="/var/lib/pgsql/data/pg_hba.conf"
	bak_file=$pg_hba".bak_"$(date +"%Y%m%d%H%M%S")
	file_to_insert=$my_pg_hba_conf
	if [ -f "$pg_hba" ]
	then
		if [ ! -f "$bak_file" ]
		then
			cp $pg_hba $bak_file
		fi

		grep -q "Scarpa configuration for $DB_NAME" $pg_hba
		if [ $? -eq 0 ]
		then
			echo "$pg_hba was already patched."
		else
			sed -e "s:^#\ TYPE.*$:cat $file_to_insert:e" $bak_file > $pg_hba

			echo "Reloading configuration..."
			su --login postgres --command "pg_ctl reload"
		fi
	else
		echo "$pg_hba not found."
	fi
}

function create_user {
	if id $USER_NAME >/dev/null 2>&1; then
		echo "System user $USER_NAME exists."
	else
		echo "System user $USER_NAME does not exist. Creating..."
		adduser --system --create-home --home $USER_HOME $USER_NAME
	fi
}

function copy_files {
	cp -R $SCRIPT_PARENT_PATH/{bin,etc,lib} $USER_HOME
	mv $USER_HOME/bin/scarpa.sh $USER_HOME/bin/$APP_NAME.sh

	backup_config=$USER_HOME/etc/pg_backup.config
	mv $backup_config.template $backup_config
	sed -i "s/DB_USER/$DB_USER/" $backup_config
	sed -i "s/SYSTEM_USER/$USER_NAME/" $backup_config

	app_config=$USER_HOME/etc/application.yaml
	mv $app_config.template $app_config
	sed -i "s/DB_NAME/$DB_NAME/" $app_config
	sed -i "s/DB_USER/$DB_USER/" $app_config
	sed -i "s/DB_PASS/$DB_PASS/" $app_config

	GROUP_ID=$(id -g $USER_NAME)
	chown -R $USER_NAME:$GROUP_ID $USER_HOME/{bin,etc,lib}
}

function setup_certificate {
	read_keystore_password
	read_key_password
	keytool -genkey \
		-keyalg RSA \
		-dname "cn=$(uname -n), ou=IT, o=Unelte Libere, c=RO" \
		-alias storm \
		-keystore keystore.jks \
		-validity 360 \
		-keysize 2048 \
		-storepass $KS_PASS \
		-keypass $KEY_PASS

	keystore=$USER_HOME/etc/keystore.jks
	mv keystore.jks $keystore
	chown $USER_NAME:$USER_NAME $keystore
	app_config=$USER_HOME/etc/application.yaml
	sed -i "s/KS_PASS/$KS_PASS/" $app_config
	sed -i "s/KEY_PASS/$KEY_PASS/" $app_config
}

function install_service {
	service_file=$USER_HOME/etc/systemd/system/scarpa.service

	cp $service_file.template $service_file

	sed -i "s#SERVICE_DESCRIPTION#$SERVICE_DESCRIPTION#" $service_file
	sed -i "s#SERVICE_USER_NAME#$USER_NAME#" $service_file
	sed -i "s#SERVICE_USER_HOME#$USER_HOME#" $service_file
	sed -i "s#SERVICE_APP_NAME#$APP_NAME#" $service_file

	cp $service_file /etc/systemd/system/$APP_NAME.service

	systemctl enable $APP_NAME
}

function read_db_password {
	read -r -s -p "Database password: " DB_PASS; echo
	read -r -s -p "Database password (again): " DB_PASS_AGAIN; echo
	if [ "$DB_PASS" != "$DB_PASS_AGAIN" ]; then
		echo "Passwords didn't match. Please try again."
		read_db_password
	fi
}

function read_keystore_password {
	read -r -s -p "Keystore password: " KS_PASS; echo
	read -r -s -p "Keystore password (again): " KS_PASS_AGAIN; echo
	if [ "$KS_PASS" != "$KS_PASS_AGAIN" ]; then
		echo "Passwords didn't match. Please try again."
		read_keystore_password
	fi
}

function read_key_password {
	read -r -s -p "Key password: " KEY_PASS; echo
	read -r -s -p "Key password (again): " KEY_PASS_AGAIN; echo
	if [ "$KEY_PASS" != "$KEY_PASS_AGAIN" ]; then
		echo "Passwords didn't match. Please try again."
		read_key_password
	fi
}

function usage {
	echo \
	"Usage: $0 [-n <app_name>]" \
	"          [-s <system_user_name>]" \
	"          [-j <user_home>]" \
	"          [-d <service_description>]" 1>&2
}

function read_options {
	local OPTIND
	while getopts ":n:s:j:d:" option; do
        case "${option}" in
			n)
				APP_NAME=${OPTARG}
            	;;
			s)
				USER_NAME=${OPTARG}
            	;;
			j)
				USER_HOME=${OPTARG}
            	;;
			d)
				SERVICE_DESCRIPTION=${OPTARG}
            	;;
			*)
				usage
				exit 1
				;;
    	esac
	done
	shift $((OPTIND-1))
}

function main {
	read_options $@

	read -p "Database name: " DB_NAME
	read -p "Database user: " DB_USER
	read_db_password

	echo "Creating database and user..."
	create_db

	echo "Configuring PostgreSQL..."
	permit_db_login

	echo "Creating system user..."
	create_user

	echo "Copying files..."
	copy_files
	
	echo "Setting up TLS..."
	setup_certificate

	echo "Installing service..."
	install_service

	echo "Starting service..."
	systemctl start $APP_NAME

	echo "Install complete."
}

main $@
