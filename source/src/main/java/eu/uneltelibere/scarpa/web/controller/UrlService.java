package eu.uneltelibere.scarpa.web.controller;

import javax.servlet.http.HttpServletRequest;

public interface UrlService {

	String appUrl(HttpServletRequest request);
}
