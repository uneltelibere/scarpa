package eu.uneltelibere.scarpa.web.controller.search;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DefaultFilterParser implements FilterParser {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultFilterParser.class);

	private static final String REGEX = "(?<field>[\\w-]+)\\ (?<operator>\\w+)(\\ (?<value>.+))?";
	private static final Pattern FILTER_PATTERN = Pattern.compile(REGEX);

	@Override
	public List<TextCondition> parseFilter(List<String> filters) {
		List<TextCondition> conditions = new LinkedList<>();
		for (String filter : filters) {
			conditions.add(parseFilter(filter));
		}
		return conditions;
	}

	@Override
	public TextCondition parseFilter(String filter) {
		TextCondition textCondition = new TextCondition();
		Matcher matcher = FILTER_PATTERN.matcher(filter);
		if (matcher.matches()) {
			textCondition.setField(matcher.group("field"));
			textCondition.setOperator(matcher.group("operator"));
			String value = matcher.group("value");
			if (value != null) {
				textCondition.setValue(value);
			}
		} else {
			LOG.warn("Could not parse filter {}.", filter);
		}
		return textCondition;
	}
}
