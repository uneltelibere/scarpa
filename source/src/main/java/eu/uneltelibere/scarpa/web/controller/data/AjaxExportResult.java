package eu.uneltelibere.scarpa.web.controller.data;

public class AjaxExportResult extends AjaxResult {

	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
