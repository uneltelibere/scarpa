package eu.uneltelibere.scarpa.dao;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface QuickSearchDao<T extends Entity> {

	List<T> search(String substring);
}
