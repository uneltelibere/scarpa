package eu.uneltelibere.scarpa.dao;

import java.util.List;

import eu.uneltelibere.scarpa.model.Entity;

public interface EntityAttachmentsDao<T extends Entity> {

	void removeAttachments(final T entity, List<Long> idsOfAttachmentsToRemove);

	void addAttachments(final T entity, List<Long> idsOfAttachmentsToAdd);

	List<Long> getAttachments(final T entity);
}
