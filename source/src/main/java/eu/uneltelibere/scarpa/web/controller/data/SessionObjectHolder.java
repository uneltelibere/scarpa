package eu.uneltelibere.scarpa.web.controller.data;

import java.util.Optional;

import javax.servlet.http.HttpSession;

public interface SessionObjectHolder<OBJECT, KEY> {

	void setCurrentObject(OBJECT object, KEY id, HttpSession session);

	void clearCurrentObject(HttpSession session);

	/**
	 * TODO remove when clients have stopped using it
	 * 
	 * @deprecated The default implementation always looks into the database, and it
	 *             shouldn't. Please use
	 *             {@link #currentObjectFromSession(Object, HttpSession)} instead,
	 *             which really only looks on the session.
	 */
	@Deprecated
	Optional<OBJECT> currentObject(KEY id, HttpSession session);

	Optional<OBJECT> currentObjectFromSession(KEY id, HttpSession session);
}
