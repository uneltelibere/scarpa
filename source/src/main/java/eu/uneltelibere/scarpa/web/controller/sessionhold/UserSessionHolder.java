package eu.uneltelibere.scarpa.web.controller.sessionhold;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.model.User;
import eu.uneltelibere.scarpa.service.UserService;
import eu.uneltelibere.scarpa.web.controller.data.AbstractSessionObjectHolder;

@Component
public class UserSessionHolder extends AbstractSessionObjectHolder<User, Long> {

	private static final String CURRENT_ENTITY_ATTRIBUTE = "currentUser";

	private static final String CURRENT_ENTITY_ID_ATTRIBUTE = "currentUserId";

	@Autowired
	private UserService userService;

	public UserSessionHolder() {
		super(User.class, Long.class);
	}

	protected String currentObjectAttribute() {
		return CURRENT_ENTITY_ATTRIBUTE;
	}

	protected String currentObjectIdAttribute() {
		return CURRENT_ENTITY_ID_ATTRIBUTE;
	}

	protected Optional<User> persistentObject(Long id, HttpSession session) {
		if (id == User.NEW_ID) {
			return Optional.of(new User());
		} else {
			return userService.getById(id);
		}
	}
}
