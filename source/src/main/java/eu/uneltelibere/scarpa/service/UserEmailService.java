package eu.uneltelibere.scarpa.service;

import eu.uneltelibere.scarpa.model.Token;
import eu.uneltelibere.scarpa.model.User;

public interface UserEmailService {

	boolean sendRecoveryMail(Token token);

	boolean sendWelcomeMail(Token token);

	boolean sendWelcomeMail(User user);
}
