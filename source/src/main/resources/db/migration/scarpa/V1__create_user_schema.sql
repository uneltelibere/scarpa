-- Spring Security User Schema

CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY NOT NULL,
	username VARCHAR(64) UNIQUE NOT NULL,
	email VARCHAR(128) NOT NULL,
	password VARCHAR(128) NOT NULL,
	enabled BOOLEAN NOT NULL,
	credentials_non_expired BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS authorities (
	id SERIAL PRIMARY KEY NOT NULL,
	code VARCHAR(64) UNIQUE NOT NULL,
	label VARCHAR(64) NOT NULL
);

CREATE TABLE IF NOT EXISTS user_authority (
	id SERIAL PRIMARY KEY NOT NULL,
	user_id INT NOT NULL,
	authority_id INT NOT NULL,
	CONSTRAINT fk_user_authority_users FOREIGN KEY(user_id) REFERENCES users(id),
	CONSTRAINT fk_user_authority_authorities FOREIGN KEY(authority_id) REFERENCES authorities(id),
	CONSTRAINT unique_user_authority UNIQUE(user_id, authority_id)
);

CREATE TABLE IF NOT EXISTS password_reset_tokens (
	id SERIAL PRIMARY KEY NOT NULL,
	user_id INT NOT NULL,
	token VARCHAR(64) UNIQUE NOT NULL,
	expiry_date TIMESTAMP NOT NULL,
	CONSTRAINT fk_password_reset_tokens_users FOREIGN KEY(user_id) REFERENCES users(id)
);

-- Password equals the username, but it is expired
-- so the user must change the password after first login.
INSERT INTO users(username, email, password, enabled, credentials_non_expired)
VALUES
	('root', 'root@localhost', '$2a$10$kABATxVN2wUkBKPsIkZH0edw47Y/Z.R2E1PCWT4rRz0Bi8GdbFlRW', TRUE, FALSE),
	('user', 'user@localhost', '$2a$08$xHMn/O2IsUpGTjzMSDLdbuqqRqCgPGa/fRybSoA2ecaW5fuHrZdHW', TRUE, FALSE);

INSERT INTO authorities(code, label)
VALUES
	('ROLE_USER_ADMIN', 'User Admin'),
	('ROLE_INSTALL_ADMIN', 'Install Admin'),
	('ROLE_LOG_VIEWER', 'Log Viewer'),
	('ROLE_USER', 'HR');

INSERT INTO user_authority(user_id, authority_id)
VALUES(
	(SELECT id FROM users WHERE username = 'root'),
	(SELECT id FROM authorities WHERE code = 'ROLE_USER_ADMIN')
), (
	(SELECT id FROM users WHERE username = 'root'),
	(SELECT id FROM authorities WHERE code = 'ROLE_INSTALL_ADMIN')
), (
	(SELECT id FROM users WHERE username = 'root'),
	(SELECT id FROM authorities WHERE code = 'ROLE_LOG_VIEWER')
), (
	(SELECT id FROM users WHERE username = 'user'),
	(SELECT id FROM authorities WHERE code = 'ROLE_USER')
), (
	(SELECT id FROM users WHERE username = 'user'),
	(SELECT id FROM authorities WHERE code = 'ROLE_INSTALL_ADMIN')
), (
	(SELECT id FROM users WHERE username = 'user'),
	(SELECT id FROM authorities WHERE code = 'ROLE_LOG_VIEWER')
);
