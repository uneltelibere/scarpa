package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.UserAdminRole;

@Component
public class RoleGroupMenu extends AbstractMenu{

	public RoleGroupMenu() {
		super("menu.role.group", "/role-group", new UserAdminRole());
	}
}
