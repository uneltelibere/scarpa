package eu.uneltelibere.scarpa.tool;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.uneltelibere.scarpa.tool.DefaultStringListBuilder;

public class StringListBuilderTest {

	@Test
	public void testListWithNoElements() {
		List<String> labels = Collections.emptyList();
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("", list);
	}

	@Test
	public void testListWithOneElement() {
		List<String> labels = Arrays.asList("one");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("one", list);
	}

	@Test
	public void testListWithTwoElements() {
		List<String> labels = Arrays.asList("one", "two");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("one and two", list);
	}

	@Test
	public void testListWithThreeElements() {
		List<String> labels = Arrays.asList("one", "two", "three");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("one, two and three", list);
	}

	@Test
	public void testListWithFourElements() {
		List<String> labels = Arrays.asList("one", "two", "three", "four");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("one, two, three and 1 more", list);
	}

	@Test
	public void testListWithFiveElements() {
		List<String> labels = Arrays.asList("one", "two", "three", "four", "five");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator()).list();
		Assert.assertEquals("one, two, three and 2 more", list);
	}

	@Test
	public void testListWithFiveElementsShowTwo() {
		List<String> labels = Arrays.asList("one", "two", "three", "four", "five");
		String list = new DefaultStringListBuilder().withSourceSize(labels.size()).withSource(labels.iterator())
				.withElementsToShow(2).list();
		Assert.assertEquals("one, two and 3 more", list);
	}
}
