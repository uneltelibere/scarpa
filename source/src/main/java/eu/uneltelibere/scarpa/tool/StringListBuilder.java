package eu.uneltelibere.scarpa.tool;

import java.util.Iterator;

public interface StringListBuilder {

	StringListBuilder withSourceSize(int sourceSize);

	StringListBuilder withSource(Iterator<String> source);

	String list();
}
