package eu.uneltelibere.scarpa.service;

import java.util.Optional;

import eu.uneltelibere.scarpa.model.User;

public interface AuthorizedUserService {

	Optional<User> getUserDetails(String username);

	User userWithRoles(User user);
}
