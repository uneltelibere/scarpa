package eu.uneltelibere.scarpa.service.enums;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import eu.uneltelibere.scarpa.dao.ScarpaEnumDao;
import eu.uneltelibere.scarpa.model.ScarpaEnum;

public abstract class AbstractScarpaEnumService<T extends ScarpaEnum> implements ScarpaEnumService<T> {

	@Autowired
	private ScarpaEnumDao<T> scarpaEnumDao;

	@Override
	public Optional<T> getById(long id) {
		return scarpaEnumDao.getById(id);
	}

	@Override
	public List<T> getAll() {
		return scarpaEnumDao.getAll();
	}
}
