package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.AnyRole;

@Component
public class MyAccountMenu extends AbstractMenu {

	public MyAccountMenu() {
		super("menu.me", "/me", new AnyRole());
	}
}
