package eu.uneltelibere.scarpa.web.menu;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.api.Role;

public abstract class AbstractMenu implements Menu {

	private final String label;
	private final String link;
	private final List<Role> roles;

	protected AbstractMenu(String label, String link, Role... roles) {
		this.label = label;
		this.link = link;
		this.roles = Arrays.asList(roles);
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public List<Role> getRoles() {
		return Collections.unmodifiableList(roles);
	}

	@Override
	public int getBadge(HttpServletRequest request) {
		return 0;
	}
}
