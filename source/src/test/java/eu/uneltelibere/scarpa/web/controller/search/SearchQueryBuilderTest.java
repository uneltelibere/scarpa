package eu.uneltelibere.scarpa.web.controller.search;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SearchQueryBuilderTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testSearchOneUnary() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder().addParam("title", "any", "");
		assertEquals("filter=title+any", queryBuilder.query());
	}

	@Test
	public void testSearchOneBinary() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder().addParam("title", "eq", "x");
		assertEquals("filter=title+eq+x", queryBuilder.query());
	}

	@Test
	public void testSearchTwoUnary() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.addParam("title", "any", "");
		queryBuilder.addParam("state", "any", "");
		assertEquals("filter=title+any&filter=state+any", queryBuilder.query());
	}

	@Test
	public void testSearchTwoBinary() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.addParam("title", "like", "x");
		queryBuilder.addParam("state", "eq", "y");
		assertEquals("filter=title+like+x&filter=state+eq+y", queryBuilder.query());
	}

	@Test
	public void testSearchTwoUnaryExcludeOne() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.exclude("title");
		queryBuilder.addParam("title", "any", "");
		queryBuilder.addParam("state", "any", "");
		assertEquals("filter=state+any", queryBuilder.query());
	}

	@Test
	public void testSearchTwoUnaryExcludeWrongUsage() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.addParam("title", "any", "");
		queryBuilder.addParam("state", "any", "");
		expectedException.expect(IllegalStateException.class);
		queryBuilder.exclude("title");
	}

	@Test
	public void testMaybeAddParam() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddParam("date", "eq", "2018-07-05", "1970-01-01");
		assertEquals("filter=date+eq+2018-07-05", queryBuilder.query());
	}

	@Test
	public void testMaybeAddParamAnyOperator() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddParam("date", "any", "", "1970-01-01");
		assertEquals("", queryBuilder.query());
	}

	@Test
	public void testMaybeAddParamAnyValue() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddParam("date", "eq", "1970-01-01", "1970-01-01");
		assertEquals("", queryBuilder.query());
	}

	@Test
	public void testMaybeAddParamNoneOperator() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddParam("date", "none", "1970-01-01", "1970-01-01");
		assertEquals("filter=date+none", queryBuilder.query());
	}

	@Test
	public void testMaybeAddEqParam() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddEqParam("date", "2018-07-05", "1970-01-01");
		assertEquals("filter=date+eq+2018-07-05", queryBuilder.query());
	}

	@Test
	public void testMaybeAddEqParamAnyValue() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddEqParam("date", "1970-01-01", "1970-01-01");
		assertEquals("", queryBuilder.query());
	}

	@Test
	public void testMaybeAddLikeParam() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddLikeParam("title", "x");
		assertEquals("filter=title+like+x", queryBuilder.query());
	}

	@Test
	public void testMaybeAddLikeParamMissingValue() {
		SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
		queryBuilder.maybeAddLikeParam("title", "");
		assertEquals("", queryBuilder.query());
	}
}
