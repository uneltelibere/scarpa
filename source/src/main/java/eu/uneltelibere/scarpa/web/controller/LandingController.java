package eu.uneltelibere.scarpa.web.controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.uneltelibere.scarpa.InstallAdminRole;
import eu.uneltelibere.scarpa.UserAdminRole;
import eu.uneltelibere.scarpa.api.Role;
import eu.uneltelibere.scarpa.api.page.LandingPageConfig;

@Controller
public class LandingController {

	private static final Logger LOG = LoggerFactory.getLogger(LandingController.class);

	private static final Map<Role, String> ROLE_TO_WELCOME_PAGE = new LinkedHashMap<>();

	static {
		ROLE_TO_WELCOME_PAGE.put(new UserAdminRole(), "/users");
		ROLE_TO_WELCOME_PAGE.put(new InstallAdminRole(), "/install");
	}

	@Autowired
	private LandingPageConfig landingConfig;

	@RequestMapping({ "/", "/welcome" })
	public String landingPage(Model model, HttpServletRequest request) {
		landingConfig.preprocess(model, request);

		Optional<String> maybeApplicationLanding = landingConfig.applicationLandingPage();
		if (maybeApplicationLanding.isPresent()) {
			String applicationLandingPage = maybeApplicationLanding.get();
			LOG.info("Showing application welcome page {}", applicationLandingPage);
			return applicationLandingPage;
		}

		Map<Role, String> roleToWelcomePage = new LinkedHashMap<>();
		roleToWelcomePage.putAll(landingConfig.roleToWelcomePage());
		roleToWelcomePage.putAll(ROLE_TO_WELCOME_PAGE);

		for (Map.Entry<Role, String> roleToPage : roleToWelcomePage.entrySet()) {
			if (request.isUserInRole(roleToPage.getKey().name())) {
				LOG.info("Showing welcome page {} for user with role {}", roleToPage.getValue(), roleToPage.getKey());
				return "redirect:" + roleToPage.getValue();
			}
		}
		LOG.info("Showing empty welcome page for user with undefined role.");
		return "emptyWelcome";
	}
}
