package eu.uneltelibere.scarpa.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.uneltelibere.scarpa.dao.LanguageDao;
import eu.uneltelibere.scarpa.dao.TranslationDao;
import eu.uneltelibere.scarpa.model.Language;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.TranslationText;

@Service
public class DefaultTranslationService implements TranslationService {

	@Autowired
	private TranslationDao translationDao;

	@Autowired
	private LanguageDao languageDao;

	@Override
	public Optional<Translation> retrieve(long id) {
		return translationDao.retrieve(id);
	}

	@Override
	public String translatedText(Translation translation, Locale locale) {
		String languageCode = locale.getLanguage();
		for (TranslationText translationText : translation.getTranslationTexts()) {
			if (translationText.getLanguage().getCode().equals(languageCode)) {
				return translationText.getTranslatedText();
			}
		}
		return translation.getNeutralText();
	}

	@Override
	public Translation set(Translation translation, String text, Locale locale) {
		String langCode = locale.getLanguage();
		boolean updated = false;
		for (TranslationText translationText : translation.getTranslationTexts()) {
			if (translationText.getLanguage().getCode().equals(langCode)) {
				translationText.setTranslatedText(text);
				updated = true;
			}
		}
		if (!updated) {
			Language language = languageDao.getByCode(langCode)
					.orElseThrow(() -> new IllegalStateException("Language " + langCode + " not supported"));

			TranslationText translationText = new TranslationText();
			translationText.setTranslation(translation);
			translationText.setLanguage(language);
			translationText.setTranslatedText(text);

			List<TranslationText> texts = new LinkedList<>(translation.getTranslationTexts());
			texts.add(translationText);
			translation.setTranslationTexts(texts);
		}
		return translation;
	}

	@Override
	@Transactional(transactionManager = "masterTransactionManager")
	public void save(Translation translation) {
		createOrUpdate(translation);
		for (TranslationText translationText : translation.getTranslationTexts()) {
			createOrUpdate(translationText);
		}
	}

	private void createOrUpdate(Translation translation) {
		if (translation.getId() == Translation.NEW_ID) {
			long id = translationDao.create(translation);
			translation.setId(id);
		} else {
			translationDao.update(translation);
		}
	}

	private void createOrUpdate(TranslationText translationText) {
		if (translationText.getId() == TranslationText.NEW_ID) {
			long id = translationDao.create(translationText);
			translationText.setId(id);
		} else {
			translationDao.update(translationText);
		}
	}
}
