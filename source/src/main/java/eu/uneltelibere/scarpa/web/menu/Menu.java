package eu.uneltelibere.scarpa.web.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eu.uneltelibere.scarpa.api.Role;

public interface Menu {

	String getLabel();

	String getLink();

	List<Role> getRoles();

	int getBadge(HttpServletRequest request);
}
