package eu.uneltelibere.scarpa.service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import eu.uneltelibere.scarpa.model.Language;
import eu.uneltelibere.scarpa.model.Translation;
import eu.uneltelibere.scarpa.model.TranslationText;

public class TranslationServiceTest {

	private static final String NEUTRAL_DESCRIPTION = "neutral description";
	private static final String ENGLISH_DESCRIPTION = "english description";
	private static final String ROMANIAN_DESCRIPTION = "romanian description";

	@InjectMocks
	private DefaultTranslationService translationService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTranslatedText() {
		Translation translation = translationWithEnglish();
		assertEquals(ENGLISH_DESCRIPTION, translationService.translatedText(translation, Locale.ENGLISH));
	}

	@Test
	public void testTranslatedTextNoLanguageMatch() {
		Translation translation = translationWithRomanian();
		assertEquals(NEUTRAL_DESCRIPTION, translationService.translatedText(translation, Locale.ENGLISH));
	}

	@Test
	public void testTranslatedTextNoLanguageAtALl() {
		Translation translation = translationWithNoTexts();
		assertEquals(NEUTRAL_DESCRIPTION, translationService.translatedText(translation, Locale.ENGLISH));
	}

	private Translation translationWithEnglish() {
		TranslationText englishDescription = new TranslationText();
		englishDescription.setId(1);
		englishDescription.setTranslatedText(ENGLISH_DESCRIPTION);
		englishDescription.setLanguage(language("en", "English"));

		Translation translatedDescription = new Translation();
		translatedDescription.setId(1);
		translatedDescription.setNeutralText(NEUTRAL_DESCRIPTION);
		translatedDescription.setTranslationTexts(Arrays.asList(englishDescription));

		return translatedDescription;
	}

	private Translation translationWithRomanian() {
		TranslationText englishDescription = new TranslationText();
		englishDescription.setId(1);
		englishDescription.setTranslatedText(ROMANIAN_DESCRIPTION);
		englishDescription.setLanguage(language("ro", "Romanian"));

		Translation translatedDescription = new Translation();
		translatedDescription.setId(1);
		translatedDescription.setNeutralText(NEUTRAL_DESCRIPTION);
		translatedDescription.setTranslationTexts(Arrays.asList(englishDescription));

		return translatedDescription;
	}

	private Translation translationWithNoTexts() {
		Translation translatedDescription = new Translation();
		translatedDescription.setId(1);
		translatedDescription.setNeutralText(NEUTRAL_DESCRIPTION);

		return translatedDescription;
	}

	private Language language(String code, String name) {
		Language english = new Language();
		english.setId(1);
		english.setCode(code);
		english.setName(name);
		return english;
	}
}
