package eu.uneltelibere.scarpa.web.controller.api;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.model.Entity;

@Component
public class EntityIdProvider<T extends Entity> implements IdProvider<T> {

	private static final String NEW = "new";

	@Override
	public String id(T entity) {
		return id(entity.getId());
	}

	@Override
	public String id(long id) {
		if (id != Entity.NEW_ID) {
			return String.valueOf(id);
		} else {
			return NEW;
		}
	}

	@Override
	public long id(String idString) {
		if (NEW.equals(idString)) {
			return Entity.NEW_ID;
		} else {
			return Long.parseLong(idString);
		}
	}
}
