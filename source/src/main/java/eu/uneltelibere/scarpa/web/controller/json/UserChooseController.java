package eu.uneltelibere.scarpa.web.controller.json;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.uneltelibere.scarpa.model.User;

@Controller
@RequestMapping("/user")
public class UserChooseController extends QuickChooseController<User> {

	private static final String NONE_FOUND = "search.quick.user.found.none";

	@Override
	protected String messageNoneFound() {
		return NONE_FOUND;
	}
}
