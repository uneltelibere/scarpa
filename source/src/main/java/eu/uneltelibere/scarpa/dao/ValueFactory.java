package eu.uneltelibere.scarpa.dao;

public interface ValueFactory {

	public Object createValue(String valueAsString);
}
