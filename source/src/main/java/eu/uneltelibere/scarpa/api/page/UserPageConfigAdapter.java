package eu.uneltelibere.scarpa.api.page;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.model.User;

public abstract class UserPageConfigAdapter implements UserPageConfig {

	@Override
	public String getEditUserPage(long id, Model model, HttpServletRequest request) {
		return "userEdit";
	}

	@Override
	public void postSaveUser(User user, HttpServletRequest request) {
	}
}
