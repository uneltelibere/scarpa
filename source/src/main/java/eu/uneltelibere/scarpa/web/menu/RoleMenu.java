package eu.uneltelibere.scarpa.web.menu;

import org.springframework.stereotype.Component;

import eu.uneltelibere.scarpa.UserAdminRole;

@Component
public class RoleMenu extends AbstractMenu {

	public RoleMenu() {
		super("menu.role", "/role", new UserAdminRole());
	}
}
