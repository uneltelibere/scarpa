package eu.uneltelibere.scarpa.web.controller.api;

import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.success;
import static eu.uneltelibere.scarpa.web.controller.ScarpaAlerts.warn;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import eu.uneltelibere.scarpa.model.Entity;
import eu.uneltelibere.scarpa.service.EntityDeleteService;
import eu.uneltelibere.scarpa.service.EntityStillUsedException;

public abstract class EntityDeleteHandler<T extends Entity> {

	private static final Logger LOG = LoggerFactory.getLogger(EntityDeleteHandler.class);

	public String delete(long id, Supplier<Optional<T>> entitySupplier, List<String> filter, int page,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
		try {
			Optional<T> maybeEntity = entitySupplier.get();
			if (maybeEntity.isPresent()) {
				entityService().delete(id);
				success(redirectAttributes, successMessage(maybeEntity.get(), request));
			} else {
				warn(redirectAttributes, notFoundMessage(request));
			}
		} catch (EntityStillUsedException e) {
			LOG.warn("Entity " + id + " is still used, will not be deleted.");
			warn(redirectAttributes, stillUsedMessage(request));
		} catch (Exception e) {
			LOG.warn("Failed to delete.", e);
			warn(redirectAttributes, deleteFailedMessage(e, request));
		}

		return "redirect:" + entityPageUrl(page, filter);
	}

	protected abstract EntityDeleteService<T> entityService();

	protected abstract String successMessage(T entity, HttpServletRequest request);

	protected abstract String notFoundMessage(HttpServletRequest request);

	protected abstract String stillUsedMessage(HttpServletRequest request);

	protected abstract String deleteFailedMessage(Throwable e, HttpServletRequest request);

	private String entityPageUrl(int page, List<String> filter) {
		StringBuilder sb = new StringBuilder(entityUrl());
		if (page > 1) {
			sb.append("/page/").append(page);
		}
		for (int i = 0; i < filter.size(); i++) {
			if (i == 0) {
				sb.append("?");
			} else {
				sb.append("&");
			}
			String filterElement = filter.get(i);
			String encodedFilterElement = "";
			try {
				encodedFilterElement = URLEncoder.encode(filterElement, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				LOG.warn("Failed to encode filter URL parameter " + filterElement, e);
			}
			sb.append("filter=").append(encodedFilterElement);
		}
		return sb.toString();
	}

	protected abstract String entityUrl();
}
