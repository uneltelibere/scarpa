package eu.uneltelibere.scarpa.spring;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RegExPasswordRemoverTest {

	private PasswordRemover remover = new RegExPasswordRemover();

	@Test
	public void testRemoveNoPassword() {
		String input = "uri=/my-account;payload=email=someone%40example.com";
		assertEquals(input, remover.removePassword(input));
	}

	@Test
	public void testRemoveOnePassword() {
		String input = "uri=/my-account;payload=email=someone%40example.com&password=secret";
		String expected = "uri=/my-account;payload=email=someone%40example.com&password=***";
		assertEquals(expected, remover.removePassword(input));
	}

	@Test
	public void testRemoveTwoPasswords() {
		String input = "uri=/my-account;payload=email=someone%40example.com&password=secret&passwordAgain=secret";
		String expected = "uri=/my-account;payload=email=someone%40example.com&password=***&passwordAgain=***";
		assertEquals(expected, remover.removePassword(input));
	}

	@Test
	public void testRemovePasswordKeepBracket() {
		String input = "[uri=/my-account;payload=email=someone%40example.com&password=secret]";
		String expected = "[uri=/my-account;payload=email=someone%40example.com&password=***]";
		assertEquals(expected, remover.removePassword(input));
	}
}
