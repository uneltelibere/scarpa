package eu.uneltelibere.scarpa.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:/META-INF/scarpa-application.version")
public class PropertiesVersionService implements VersionService {

	private static final String DEFAULT_VERSION = "development";

	private static final String DEFAULT_DATE = "unknown";

	@Value("${scarpa.application.version:" + DEFAULT_VERSION + "}")
	private String version;

	@Value("${scarpa.application.build.date:" + DEFAULT_DATE + "}")
	private String date;

	@Override
	public String version() {
		if (versionNotConfigured()) {
			String artifactVersion = getClass().getPackage().getImplementationVersion();
			if (artifactVersion == null) {
				artifactVersion = DEFAULT_VERSION;
			}
			return artifactVersion;
		}
		return version;
	}

	@Override
	public String buildDate() {
		return date;
	}

	private boolean versionNotConfigured() {
		return DEFAULT_VERSION.equals(version);
	}
}
