package eu.uneltelibere.scarpa.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SqlSortTest {

	@Test
	public void testWithDefaultOnly() {
		SqlSort sqlSort = SqlSort.sortByDefault("i.id DESC");
		assertEquals("i.id DESC", sqlSort.query());
	}

	@Test
	public void testWithDefaultAndExplicit() {
		SqlSort sqlSort = SqlSort.sortByDefault("i.id DESC");
		sqlSort.addSort("i.name", SortCriteria.Order.ASC);
		assertEquals("i.name, i.id DESC", sqlSort.query());
	}

	@Test
	public void testWithDefaultAndExplicitDesc() {
		SqlSort sqlSort = SqlSort.sortByDefault("i.id DESC");
		sqlSort.addSort("i.name", SortCriteria.Order.DESC);
		assertEquals("i.name DESC, i.id DESC", sqlSort.query());
	}

	@Test
	public void testQueryIdempotent() {
		SqlSort sqlSort = SqlSort.sortByDefault("i.id DESC");
		sqlSort.addSort("i.name", SortCriteria.Order.DESC);
		sqlSort.query();
		assertEquals("i.name DESC, i.id DESC", sqlSort.query());
	}
}
