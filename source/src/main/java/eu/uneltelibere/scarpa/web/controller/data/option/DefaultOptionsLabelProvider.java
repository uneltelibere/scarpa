package eu.uneltelibere.scarpa.web.controller.data.option;

import javax.servlet.http.HttpServletRequest;

public class DefaultOptionsLabelProvider implements OptionsLabelProvider {

	private static final String OPTION_ANY = "generic.select.option.any";

	@Override
	public String labelAny() {
		return OPTION_ANY;
	}

	@Override
	public String labelFormat(String label, HttpServletRequest request) {
		return label;
	}
}
