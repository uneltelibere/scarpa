package eu.uneltelibere.scarpa.api;

import java.util.List;

public interface PagedRetrievingService<T> {

	List<T> retrieve(int offset, int limit);

	int count();
}
