package eu.uneltelibere.scarpa.web.controller.data;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractSessionObjectHolder<OBJECT, KEY> implements SessionObjectHolder<OBJECT, KEY> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractSessionObjectHolder.class);

	private final Class<OBJECT> objectClass;
	private final Class<KEY> keyClass;

	protected AbstractSessionObjectHolder(Class<OBJECT> objectClass, Class<KEY> keyClass) {
		this.objectClass = objectClass;
		this.keyClass = keyClass;
	}

	@Override
	public void setCurrentObject(OBJECT object, KEY id, HttpSession session) {
		LOG.debug("Set current object {}.", id);
		session.setAttribute(currentObjectAttribute(), object);
		session.setAttribute(currentObjectIdAttribute(), id);
	}

	@Override
	public void clearCurrentObject(HttpSession session) {
		LOG.debug("Clear current object.");
		session.removeAttribute(currentObjectAttribute());
		session.removeAttribute(currentObjectIdAttribute());
	}

	@Override
	public Optional<OBJECT> currentObject(KEY id, HttpSession session) {
		LOG.debug("Get current object {}.", id);
		Optional<KEY> maybeCurrentId = sessionValue(session, currentObjectIdAttribute(), keyClass);
		if (maybeCurrentId.isPresent() && maybeCurrentId.get().equals(id)) {
			Optional<OBJECT> maybeCurrentObject = sessionValue(session, currentObjectAttribute(), objectClass);
			if (maybeCurrentObject.isPresent()) {
				LOG.debug("Fetching object {} from session.", id);
				return maybeCurrentObject;
			} else {
				LOG.debug("There was no current object for {}.", id);
				return Optional.empty();
			}
		} else {
			LOG.debug("Current object of type {} was for another ID ({}). Reading from persistence.",
					objectClass.getSimpleName(), maybeCurrentId);
			return persistentObject(id, session);
		}
	}

	@Override
	public Optional<OBJECT> currentObjectFromSession(KEY id, HttpSession session) {
		LOG.debug("Get current object {}.", id);
		Optional<KEY> maybeCurrentId = sessionValue(session, currentObjectIdAttribute(), keyClass);
		if (maybeCurrentId.isPresent()) {
			if (maybeCurrentId.get().equals(id)) {
				Optional<OBJECT> maybeCurrentObject = sessionValue(session, currentObjectAttribute(), objectClass);
				if (maybeCurrentObject.isPresent()) {
					LOG.debug("Fetching object {} from session.", id);
					return maybeCurrentObject;
				} else {
					LOG.debug("There was no current object for {}.", id);
					return Optional.empty();
				}
			} else {
				LOG.debug("Current object of type {} was for another ID ({}). Reading from persistence.",
						objectClass.getSimpleName(), maybeCurrentId);
				return persistentObject(id, session);
			}
		} else {
			return Optional.empty();
		}
	}

	private <T> Optional<T> sessionValue(HttpSession session, String key, Class<T> type) {
		Object currentObjectId = session.getAttribute(key);
		if (currentObjectId != null) {
			if (type.isAssignableFrom(currentObjectId.getClass())) {
				@SuppressWarnings("unchecked")
				T currentObjId = (T) currentObjectId;
				return Optional.of(currentObjId);
			} else {
				LOG.warn("Tried to read a session value of type {} into a {}.",
						currentObjectId.getClass().getSimpleName(), type.getSimpleName());
			}
		}
		return Optional.empty();
	}

	protected abstract String currentObjectAttribute();

	protected abstract String currentObjectIdAttribute();

	protected abstract Optional<OBJECT> persistentObject(KEY id, HttpSession session);
}
