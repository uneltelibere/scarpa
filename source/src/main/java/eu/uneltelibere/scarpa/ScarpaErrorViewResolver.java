package eu.uneltelibere.scarpa;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

@Component
public class ScarpaErrorViewResolver implements ErrorViewResolver {

	@Override
	public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
		Map<String, Object> pageModel = new HashMap<>();
		pageModel.put("title", status.name());
		pageModel.put("code", status.value());
		if (status.is5xxServerError()) {
			pageModel.put("type", "5xx");
		} else if (status.is4xxClientError()) {
			pageModel.put("type", "4xx");
		}
		return new ModelAndView("error", pageModel);
	}
}
