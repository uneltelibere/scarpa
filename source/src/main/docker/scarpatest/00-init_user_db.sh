#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	CREATE ROLE scarpatest LOGIN PASSWORD 'scarpatest' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
	CREATE DATABASE scarpatest WITH ENCODING='UTF8' OWNER=scarpatest CONNECTION LIMIT=-1;
	\connect scarpatest;
	CREATE SCHEMA scarpa AUTHORIZATION scarpatest;
	GRANT ALL ON SCHEMA scarpa TO scarpatest;
	ALTER USER scarpatest SET search_path TO scarpa;
EOSQL

cd /docker-entrypoint-initdb.d/migration/scarpa
for f in $(ls -1v)
do
	psql -v ON_ERROR_STOP=1 --username scarpatest --dbname=scarpatest --file=$f
done
