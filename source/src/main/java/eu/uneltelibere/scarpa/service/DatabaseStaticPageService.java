package eu.uneltelibere.scarpa.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.uneltelibere.scarpa.dao.StaticPageDao;
import eu.uneltelibere.scarpa.model.StaticPage;
import eu.uneltelibere.scarpa.model.StaticPage.Version;

@Service
public class DatabaseStaticPageService implements StaticPageService {

	private static final int FIRST_VERSION = 1;

	private static final int LATEST_PAGE_VERSION = 0;

	@Autowired
	private StaticPageDao staticPageDao;

	@Override
	public Optional<StaticPage.Version> getLatestVersionOfPage(long pageId) {
		return getVersionOfPage(pageId, LATEST_PAGE_VERSION);
	}

	@Override
	public Optional<StaticPage.Version> getVersionOfPage(long pageId, int versionNumber) {
		if (versionNumber == LATEST_PAGE_VERSION) {
			return staticPageDao.getLatestVersionOfPage(pageId);
		} else {
			return staticPageDao.getVersionOfPage(pageId, versionNumber);
		}
	}

	@Override
	public List<Version> getVersionsOfPage(long pageId) {
		return staticPageDao.getVersionsOfPage(pageId);
	}

	@Override
	@Transactional(transactionManager = "applicationTransactionManager")
	public void save(StaticPage.Version pageVersion) {
		int versionNumber = getLatestVersionOfPage(pageVersion.getStaticPage().getId())
				.map(version -> version.getVersionNumber() + 1).orElse(FIRST_VERSION);
		pageVersion.setVersionNumber(versionNumber);
		pageVersion.setCreationDate(LocalDateTime.now());
		staticPageDao.create(pageVersion);
	}

	@Override
	public int currentVersionNumber(StaticPage staticPage) {
		return getLatestVersionOfPage(staticPage.getId()).map(version -> version.getVersionNumber())
				.orElse(LATEST_PAGE_VERSION);
	}
}
