(function() {
	$(document).ready(function() {
		showOrHidePlaceholder();
		enableOrDisableChooseButton();
	});

	$(document).on('click', '.cloud-tag', function removeTag() {
		var id = $(this).data('id');
		$(this).remove();
		$("input[id='cloud-tag-source_" + id + "']").attr('checked', false);
		requestToRemove(id);
	});

	$("input[id^='cloud-tag-source_']").change(function() {
		var id = $(this).val();
		var checked = $(this).prop("checked");
		var code = $(this).data('code');
		var label = $(this).data('label');
		if (checked) {
			addTag(id, code, label);
			requestToAdd(id);
		} else {
			removeTag(id);
			requestToRemove(id);
		}
	});

	function addTag(id, code, label) {
		var li = '<li class="cloud-tag" title="' + label + '" data-id="' + id
				+ '">' + code + '</li>';
		$(li).appendTo("#tag-cloud");
	}

	function removeTag(id) {
		$("li[data-id='" + id + "']").remove();
	}

	function requestToAdd(id) {
		console.log('Add ' + id);
		requestToAddOrRemove('add', id);
	}

	function requestToRemove(id) {
		console.log('Remove ' + id);
		requestToAddOrRemove('remove', id);
	}

	function requestToAddOrRemove(action, id) {
		showOrHidePlaceholder();
		enableOrDisableChooseButton();

		var whatAreWeDoing = 'request to ' + action + ' ' + id;

		$.ajax({
			url : contextPath + 'export/costCenters/' + action + '/' + id,
			method : 'POST',
			beforeSend : function(xhr) {
				xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
			},
			success : function(data) {
				console.log('Successfully requested to ' + whatAreWeDoing);
			},
			error : function(jqXHR, errorType, exception) {
				console.log('The request to ' + whatAreWeDoing + ' failed.');
			}
		});
	}

	function showOrHidePlaceholder() {
		if ($('ul#tag-cloud li').length == 0) {
			$("#nothing-selected-message").show();
		} else {
			$("#nothing-selected-message").hide();
		}
	}

	function enableOrDisableChooseButton() {
		if ($('ul#tag-cloud li').length == 0) {
			$("#chooseButton").attr("disabled", "disabled");
		} else {
			$("#chooseButton").removeAttr("disabled");
		}
	}
})();
