package eu.uneltelibere.scarpa.service;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import eu.uneltelibere.scarpa.dao.AggregateDao;
import eu.uneltelibere.scarpa.dao.BulkRetriever;
import eu.uneltelibere.scarpa.model.Entity;

public abstract class AbstractAggregateService<T extends Entity, U extends Entity> implements AggregateService<T, U> {

	private AggregateDao<T> aggregateDao;

	private BulkRetriever<U> partDao;

	@Override
	public void setPartsForWhole(final T wholeInDb, Stream<U> parts, List<Long> partIDs) {
		List<Long> existingParts = parts.map(Entity::getId).collect(toList());

		List<Long> partsToRemove = existingParts.stream().filter(part -> !partIDs.contains(part)).collect(toList());
		if (!partsToRemove.isEmpty()) {
			aggregateDao.removePartsFromWhole(wholeInDb, partsToRemove);
		}

		List<Long> partsToAdd = partIDs.stream().filter(part -> !existingParts.contains(part)).collect(toList());
		if (!partsToAdd.isEmpty()) {
			aggregateDao.addPartsToWhole(wholeInDb, partsToAdd);
		}
	}

	@Override
	public List<Long> readPartIds(final T whole) {
		return aggregateDao.readPartIds(whole);
	}

	@Override
	public List<U> retrieveParts(List<Long> partIDs) {
		if (partIDs.isEmpty()) {
			return Collections.emptyList();
		}
		return partDao.retrieveByIds(partIDs);
	}

	public void setAggregateDao(AggregateDao<T> aggregateDao) {
		this.aggregateDao = aggregateDao;
	}

	public void setPartDao(BulkRetriever<U> partDao) {
		this.partDao = partDao;
	}
}
