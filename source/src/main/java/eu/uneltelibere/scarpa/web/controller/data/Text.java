package eu.uneltelibere.scarpa.web.controller.data;

public class Text implements Input {

	private String placeholder;

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
}
