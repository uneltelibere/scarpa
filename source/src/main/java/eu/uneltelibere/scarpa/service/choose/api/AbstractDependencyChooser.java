package eu.uneltelibere.scarpa.service.choose.api;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import eu.uneltelibere.scarpa.filter.ConditionGroup;
import eu.uneltelibere.scarpa.model.PageNavigator;

public abstract class AbstractDependencyChooser<T> implements DependencyChooser<T> {

	public void prepareChoosePage(DependencyChooserParameters<T> params, Model model, HttpSession session) {
		ConditionGroup<T> conditionGroups = params.getConditions();
		List<T> elements = getElements(params);
		model.addAttribute(params.getListAttributeName(), translate(elements));
		getNavigator(params).ifPresent(nav -> model.addAttribute("pageNavigator", nav));
		model.addAttribute("basePageUrl", params.getChooserUrl());
		model.addAttribute("chooseNothingPage", params.getPageToReturn());
		model.addAttribute("postUrl", params.getPageToChoose());
		model.addAttribute("doNotShowSearchBar", elements.isEmpty() && conditionGroups.getConditions().isEmpty());
	}

	protected abstract List<T> getElements(DependencyChooserParameters<T> params);

	protected abstract Optional<PageNavigator> getNavigator(DependencyChooserParameters<T> params);

	protected List<? extends T> translate(List<T> elements) {
		return elements;
	}
}
