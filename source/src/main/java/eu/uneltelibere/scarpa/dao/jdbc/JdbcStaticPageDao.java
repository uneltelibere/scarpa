package eu.uneltelibere.scarpa.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import eu.uneltelibere.scarpa.dao.StaticPageDao;
import eu.uneltelibere.scarpa.model.StaticPage;
import eu.uneltelibere.scarpa.model.User;

@Repository
public class JdbcStaticPageDao extends JdbcMasterDao implements StaticPageDao {

	// @formatter:off
	private static final String GET = "SELECT "
			+ "p.id AS page_id, "
			+ "v.id, v.version_number, v.content, v.comment_text, v.creation_date, "
			+ "u.id AS user_id "
			+ "FROM static_page_version v "
			+ "INNER JOIN static_page p ON p.id = v.static_page_id "
			+ "LEFT JOIN users u ON u.id = v.author_user_id ";

	private static final String GET_LATEST_PAGE_VERSION = GET
			+ "WHERE p.id = :pageId "
			+ "ORDER BY v.version_number DESC "
			+ "OFFSET 0 LIMIT 1";

	private static final String GET_PAGE_VERSION = GET
			+ "WHERE p.id = :pageId "
			+ "AND v.version_number = :versionNumber ";

	private static final String GET_ALL = GET
			+ "WHERE p.id = :pageId "
			+ "ORDER BY v.version_number DESC ";

	private static final String NEW_PAGE_VERSION = "INSERT INTO static_page_version ("
			+ "static_page_id, content, version_number, author_user_id, comment_text, creation_date "
			+ ") VALUES ("
			+ ":staticPageId, :content, :versionNumber, :authorUserId, :commentText, :creationDate )";
	// @formatter:on

	private ResultSetExtractor<Optional<StaticPage.Version>> resultSetExtractor = new ResultSetExtractor<Optional<StaticPage.Version>>() {

		@Override
		public Optional<StaticPage.Version> extractData(ResultSet rs) throws SQLException, DataAccessException {
			Optional<StaticPage.Version> maybeEntity = Optional.empty();
			if (rs.next()) {
				StaticPage.Version entity = entityFromResultSet(rs);
				maybeEntity = Optional.of(entity);
			}
			return maybeEntity;
		}
	};

	private StaticPage.Version entityFromResultSet(ResultSet rs) throws SQLException {
		StaticPage page = new StaticPage();
		page.setId(rs.getInt("page_id"));

		User author = new User();
		author.setId(rs.getInt("user_id"));

		StaticPage.Version entity = new StaticPage.Version();
		entity.setId(rs.getInt("id"));
		entity.setStaticPage(page);
		entity.setVersionNumber(rs.getInt("version_number"));
		entity.setContent(rs.getString("content"));
		entity.setAuthor(author);
		entity.setComment(rs.getString("comment_text"));
		entity.setCreationDate(rs.getObject("creation_date", LocalDateTime.class));
		return entity;
	}

	@Override
	public Optional<StaticPage.Version> getLatestVersionOfPage(long pageId) {
		SqlParameterSource params = new MapSqlParameterSource("pageId", pageId);
		return jdbcTemplate.query(GET_LATEST_PAGE_VERSION, params, resultSetExtractor);
	}

	@Override
	public Optional<StaticPage.Version> getVersionOfPage(long pageId, long versionNumber) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("pageId", pageId);
		params.addValue("versionNumber", versionNumber);
		return jdbcTemplate.query(GET_PAGE_VERSION, params, resultSetExtractor);
	}

	@Override
	public List<StaticPage.Version> getVersionsOfPage(long pageId) {
		SqlParameterSource params = new MapSqlParameterSource("pageId", pageId);
		return jdbcTemplate.query(GET_ALL, params, new RowMapper<StaticPage.Version>() {

			@Override
			public StaticPage.Version mapRow(ResultSet rs, int rowNum) throws SQLException {
				return entityFromResultSet(rs);
			}
		});
	}

	@Override
	public long create(StaticPage.Version pageVersion) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("staticPageId", pageVersion.getStaticPage().dbRef());
		params.addValue("content", pageVersion.getContent());
		params.addValue("versionNumber", pageVersion.getVersionNumber());
		params.addValue("authorUserId", pageVersion.getAuthor().dbRef());
		params.addValue("commentText", pageVersion.getComment());
		params.addValue("creationDate", pageVersion.getCreationDate());
		jdbcTemplate.update(NEW_PAGE_VERSION, params, generatedKeyHolder, new String[] { "id" });
		return generatedKeyHolder.getKey().longValue();
	}
}
